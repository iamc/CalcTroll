CalcTroll is a framework for creating targeted atomic-scale simulation applications. 
Practically it is a series of utilities for setting up series time-demanding long-running calculations on a cluster, at current specifially focused on STM simulations with the SIESTA code. However, the idea is that any atomic-scale simulation workflow would be easily implemented.

The basic idea is to set up chains of calculations, linking the output of one run to the input of another. Some structures and calculations come predefined in the program, but in general a user will need to implement these, likely by subclassing some of the basic types defined in the code. Once created though, the plugins will live in a simple scripting interface.

Basic usecases would be:
1. Creating a workflow on-demand for someone else to use.
2. Capture and share your workflow for transparency with collaborators.

CalcTroll depends on the Atomic Simulation Environment (ASE) which must be installed to use CalcTroll.
To set up include the root directory in your PYTHONPATH environment.
To get started go to the Examples directory section and investigate the commented scripts.

**********************
Installation:
**********************
CalcTroll currently only operate on Linux.
If the 'ase' directory is empty run the commands:
git submodule init  
git submodule update  

Run setupfile to create utility executables and to create example scripts 
and the file CalcTroll/HOSTS.py which contains information where and how 
the task set up by CalcTroll will actually be run.  
python setup.py  

The CalcTroll/HOSTS.py file should be modified to suit your local and remote evironment, although the plugin loaded as default should work for running
small calculations on the local machine.

Add the relevant directories to the PATH/PYTHONPATH environment: 

`export PATH=<Path-to-CalcTroll>/bin:$PATH`

`export PATH=$PATH:<Path-to-CalcTroll>/ase/tools:$PATH`

`export PYTHONPATH=<Path-to-CalcTroll>:<Path-to-CalcTroll>/ase:$PYTHONPATH`

To install Siesta you will need to install it from launchpad.net/siesta
Build siesta and make sure the executable is in your path:

`export PATH=$PATH:<Path-to-Siesta>/Obj`

Also include the evironment variables:

`export SIESTA_COMMAND="siesta <.%s >.%s"`

`export SIESTA_PP_PATH=<Path-to-CalcTroll>/CalcTroll/Plugins/Programs/Siesta/pseudopotentials/UOR`

Go to ExampleScripts and run the examples. 

To generate the documentation in CalcTroll/doc requires the installation of sphinx. The generated documentation for the 'master' branch can be found at:
http://calctroll.readthedocs.io/en/latest/
