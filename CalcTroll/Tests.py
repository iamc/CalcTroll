# Written by Mads Engelund, 2017, http://espeem.com
import os
import unittest
from Core.Test.Suite import importWalk, findTestCases, ImportTests, TEST_SUITE

tests_path = os.path.dirname(os.path.abspath(__file__))
ImportTests.tests_path = tests_path

importWalk(method=findTestCases, tests_path=tests_path)
suite = unittest.TestSuite()
suite.addTest(unittest.makeSuite(ImportTests))
suite.addTest(TEST_SUITE)
runner = unittest.TextTestRunner()
runner.run(suite)
