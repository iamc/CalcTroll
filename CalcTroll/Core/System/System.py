# Written by Mads Engelund, 2017, http://espeem.com
# Authored by Mads Engelund.

# All physical systems under consideration must use 'System' as a base class.
# In the CalcTroll code a system should be defined such that it can create
# different calculations that will approximate the true physical system,
# for instance a surface might be approximated by slabs of different width
# super-cells or it could be defined using open boundary conditions.

import numpy  as np
import new
from math import sqrt,pi
from numpy import matrix
from ase.io import write
from ase.visualize import view

from CalcTrollASE.Atoms import Atoms
from CalcTrollASE.AtomUtilities import makeAtomsRelaxed
from CalcTrollASE.AtomUtilities import findCopies
from CalcTrollASE.AtomUtilities import findPeriodicallyEqualAtoms
from CalcTroll.Core.Flags import DEFAULT
from CalcTroll.Core.Plugin.Types import SystemProvider
from CalcTroll.Core.ParameterControl import Parameters
from CalcTroll.Core.Submission.Submitter import calculate
from BasisChanger import BasisChanger


class System(SystemProvider):
    COORDINATES=None

    def __init__(
            self,
            sub_systems=tuple(),
            initial_state=DEFAULT,
            name=DEFAULT,
            parameters=DEFAULT,
            ):
        self.__sub_systems = sub_systems
        self.__basis_changer = None

        if initial_state is DEFAULT:
            initial_state = tuple()
        self.__initial_state = initial_state

        if name is DEFAULT:
            name = super(System, self).name()
        self.__name = name

        if isinstance(parameters, dict):
            parameters = self.parameterClass()(**parameters)

        self.__parameters = self.defaultParameters(parameters=parameters)

    def __call__(self, *args, **kwargs):
        return self.copy(*args, **kwargs)

    @classmethod
    def parameterClass(cls):
        message = "%s lacks an the method parameterClass" % cls.className()
        raise NotImplementedError(message)

    def atoms(self,
              parameters=DEFAULT,
              constrained=True,
              initialized=True,
              relaxed=True,
              ):
        if parameters is DEFAULT:
            parameters = self.parameters()
        if isinstance(parameters, Parameters):
            parameters = self.parameters(parameters)
        elif isinstance(parameters, dict):
            parameters = self.parameterClass()(**parameters)

        # Only relax if it is possible.
        relaxed = relaxed and self.isRelaxed()

        # Only initialize if the system is not relaxed.
        initialized = initialized and (not relaxed)

        system = self.fullyUnrelaxed()
        atoms = system.atomsTemplate(parameters=parameters)
        constraints = atoms.constraints
        del atoms.constraints

        atoms = self.applyInitializationConstraintsAndRelaxation(
                atoms=atoms,
                constraints=constraints,
                initialized=initialized,
                relaxed=relaxed,
                )

        if not constrained:
            del atoms.constraints

        return atoms

    def applyInitializationConstraintsAndRelaxation(
            self,
            atoms,
            constraints=tuple(),
            initialized=True,
            relaxed=True,
            ):
        if initialized:
            diff_vector, initial_moments = self.makeAtomsInitialized(atoms)

        atoms = self.makeSubsystemsRelaxed(atoms)

        if constraints is not None:
            for constraint in constraints:
                atoms.set_constraint(constraint)

        if relaxed:
            atoms = self.makeAtomsRelaxed(atoms)

        if initialized:
            atoms.positions += diff_vector
            atoms.set_initial_magnetic_moments(initial_moments)

        return atoms

    def makeSubsystemsInitialized(self, atoms):
        diff_vector = np.zeros(atoms.positions.shape)
        init_moments = np.zeros(len(atoms))

        for sub_system in self.subSystems():
            v, i = sub_system.makeAtomsInitialized(atoms)
            diff_vector += v
            init_moments += i

        return diff_vector, init_moments

    def initialStateIdentifiers(self, identifiers):
        return identifiers

    def makeAtomsInitialized(self, atoms):
        identifiers = [init['identifier'] for init in self.initialState()]
        identifiers = self.initialStateIdentifiers(identifiers)
        diff_vector, init_moments = self.makeSubsystemsInitialized(atoms)

        if len(identifiers) == 0:
            return diff_vector, init_moments

        system = self.fullyUnrelaxed()
        init_atoms, coordinates = self._findAtoms(identifiers)
        init_atoms = system.change(
                init_atoms,
                fro=coordinates,
                to=self.COORDINATES,
                )
        move = np.array([init['move_vector'] for init in self.initialState()])
        moment = np.array([init['initial_moment'] for init in self.initialState()])
        init_atoms.cell = atoms.cell
        init_atoms.pbc = atoms.pbc
        init_atoms.wrap()
        take = findCopies(init_atoms.positions, init_atoms.cell)
        init_atoms = init_atoms[take]
        move = move[take]
        moment = moment[take]

        group = findPeriodicallyEqualAtoms(init_atoms, atoms)
        change = (group !=-1)
        group = group[change]
        init_moments = np.zeros(len(change))
        diff_vector[change] += move[group]
        init_moments[change] = moment[group]

        return diff_vector, init_moments

    def _findAtoms(self, identifiers):
        raise NotImplementedError("%s" % self)

    def findAtoms(self,
            identifiers,
            initialized=True,
            relaxed=True,
            ):
        atoms, base_coordinates = self._findAtoms(identifiers)
        atoms = self.fullyUnrelaxed().change(
                atoms,
                to=self.COORDINATES,
                fro=base_coordinates,
                )

        atoms = self.applyInitializationConstraintsAndRelaxation(
                atoms=atoms,
                initialized=initialized,
                relaxed=relaxed,
                )

        return atoms

    def systemClass(self):
        return self.__class__

    def relaxation(self):
        return None

    def name(self):
        return self.__name

    def parameters(self):
        return self.__parameters

    def isDone(self):
        return True

    def initialState(self):
        return self.__initial_state

    def updateBasisChanger(self):
        basis_changer = self.setUpBasisChanger()
        self.setBasisChanger(basis_changer)

    def setUpBasisChanger(self):
        raise NotImplementedError

    def setBasisChanger(self, basis_changer):
        self.__basis_changer = basis_changer

    def basisChanger(self):
        if self.__basis_changer is None:
            self.updateBasisChanger()
        return self.__basis_changer

    def subSystems(self):
        return self.__sub_systems

    def setSubSystems(self, sub_systems):
        assert len(sub_systems) == len(self.subSystems())
        for s1, s2 in zip(sub_systems, self.subSystems()):
            if not s1.systemClass() == s2.systemClass():
                raise ValueError("%s, %s"%(s1.__class__, s2.__class__))

        other = self.copy()
        other.__sub_systems = sub_systems
        other.__basis_changer = None

        return other

    def fullyUnrelaxed(self):
        unrelaxed_subsystems = []
        if not self.hasRelaxedSubsystems():
            return self

        for sub_system in self.subSystems():
            unrelaxed_subsystem = sub_system.fullyUnrelaxed()
            unrelaxed_subsystems.append(unrelaxed_subsystem)

        unrelaxed = self.setSubSystems(unrelaxed_subsystems)

        return unrelaxed

    def isRelaxed(self):
        return (not self.relaxation() is None)

    def path(self):
        return self.name()

    def change(self, thing, to, fro, difference=False):
        return self.basisChanger().transform(thing, to, fro, difference)

    def hasRelaxedSubsystems(self):
        if len(self.subSystems()) == 0:
            return False

        return all([sub_system.isRelaxed() for sub_system in self.subSystems()])

    def defaultParameters(self, parameters=DEFAULT):
        raise NotImplementedError(str(self.className()))

    def subParameters(self, parameters=DEFAULT):
        return tuple()

    def pbc(self):
        raise NotImplementedError(str(self.className()))

    def relaxCell(self):
        return False

    def makeAtomsRelaxed(
            self,
            atoms,
            tolerance=1.5,
            ):
        atoms = makeAtomsRelaxed(
                atoms,
                relaxation=self.relaxation(),
                tolerance=tolerance,
                )

        return atoms

    def makeSubsystemsRelaxed(self, atoms, tolerance=1.0):
        for sub_system in self.subSystems():
            atoms = sub_system.makeSubsystemsRelaxed(
                    atoms=atoms,
                    tolerance=tolerance,
                    )
            atoms = sub_system.makeAtomsRelaxed(
                    atoms,
                    tolerance=tolerance,
                    )

        return atoms

    def read(self):
        calculate(self)

        if not self.isDone():
            return None

        return self.atoms()

    def frame(self, data=None, border=0.3, direction='-z'):
        raise NotImplementedError

    def customConstraints(self, atoms):
        return []

    def referenceEnergyMatchIndices(self, parameters):
        raise NotImplementedError

    def xyz_save(
            self,
            filename,
            data=None,
            ):
        if data is None:
            data = self.read()

        write(filename, data)
