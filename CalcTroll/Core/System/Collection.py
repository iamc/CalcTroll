# Written by Mads Engelund, 2017, http://espeem.com
# Authored by Mads Engelund.

# All physical systems under consideration must use 'System' as a base class.
# In the CalcTroll code a system should be defined such that it can create
# different calculations that will approximate the true physical system,
# for instance a surface might be approximated by slabs of different width
# super-cells or it could be defined using open boundary conditions.

import numpy  as np
import new
from math import sqrt,pi
from numpy import matrix
from ase.io import write
from ase.visualize import view

from CalcTrollASE.Atoms import Atoms
from CalcTrollASE.AtomUtilities import makeAtomsRelaxed
from CalcTrollASE.AtomUtilities import findCopies
from CalcTrollASE.AtomUtilities import findPeriodicallyEqualAtoms
from CalcTroll.Core.Flags import DEFAULT
from CalcTroll.Core.Plugin.Types import SystemProvider
from CalcTroll.Core.ParameterControl import Parameters
from CalcTroll.Core.Submission.Submitter import calculate
from BasisChanger import BasisChanger


class Collection(SystemProvider):
    def __init__(
            self,
            name=DEFAULT,
            parameters=DEFAULT,
            ):
        if name is DEFAULT:
            name = super(Collection, self).name()
        self.__name = name

        if isinstance(parameters, dict):
            parameters = self.parameterClass()(**parameters)

        self.__parameters = self.defaultParameters(parameters=parameters)

    def __call__(self, *args, **kwargs):
        return self.copy(*args, **kwargs)

    def entry(self,
              parameters=DEFAULT,
              constrained=True,
              initialized=True,
              relaxed=True,
              ):
        raise NotImplementedError

    def systemClass(self):
        return self.__class__

    def name(self):
        return self.__name

    def parameters(self):
        return self.__parameters

    def isDone(self):
        return True

    def isRelaxed(self):
        return (not self.relaxation() is None)

    def path(self):
        return self.name()

    def read(self):
        calculate(self)

        if not self.isDone():
            return None

        return self.entry()

    def nc_save(
            self,
            filename,
            data=None,
            ):
        raise NotImplementedError
