# Written by Mads Engelund, 2017, http://espeem.com
import os
from ase.visualize import view as AseView
from ase.io import read as AseRead
from ase.atoms import Atoms as AseAtoms
from ase.data import chemical_symbols

from CalcTroll.Core.System import System
from CalcTroll.Core.Submission.Submitter import submit, calculate
from CalcTroll.Core.Submission.Estimation import estimate
from Flags import CalcTrollException


def read(filename):
    if filename[-4:] == '.mol':
        return read_mol(filename)
    else:
        return AseRead(filename)

def read_mol(filename):
    positions = []
    symbols = []
    with open(filename, 'r') as f:
        lines = f.readlines()
        del(lines[:4])
        for line in lines:
            print line
            line = line.split()
            if not str(line[3]) in chemical_symbols:
                break
            x, y, z, symbol = line[:4]
            symbols.append(symbol)
            positions.append([float(x), float(y), float(z)])
    return AseAtoms(symbols=symbols, positions=positions)

def plot(calctroll_object, ax=None, show=False, **kwargs):
    data = calctroll_object.read()
    if data is None:
        return

    from matplotlib import pyplot as plt
    if ax is None:
        show = True
        figure = plt.figure()
        ax = figure.gca()

    if data is not None:
        calctroll_object.plot(data=data, ax=ax, **kwargs)

    if show:
        plt.show()

def save(calctroll_object, filename, **kwargs):
    from matplotlib import pylab as plt

    try:
        data = calctroll_object.read()
    except CalcTrollException:
        return None

    if not calctroll_object.isDone():
        return None

    return calctroll_object.save(filename, data, **kwargs)

def view(some_object):
    if isinstance(some_object, AseAtoms):
        AseView(some_object)
    elif isinstance(some_object, System):
        return AseView(some_object.atoms())
