# Written by Mads Engelund, 2017, http://espeem.com
from collections import OrderedDict
from CalcTroll.Core.ParameterControl import ParameterControl
from Mount import Mount

class Type(ParameterControl): pass

class ProgramProvider(Type):
    __metaclass__ = Mount

class SystemProvider(Type):
    __metaclass__ = Mount

class AnalysisProvider(Type):
    __metaclass__ = Mount

class HostProvider(Type):
    __metaclass__ = Mount

class Parameters(Type):
    __metaclass__ = Mount

loc = dict(locals())
ALL = OrderedDict([(key, value) for key, value in loc.iteritems() if \
        isinstance(value, type) and \
        issubclass(value, Type) and \
        value != Type
        ])
