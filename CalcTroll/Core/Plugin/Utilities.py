# Written by Mads Engelund, 2017, http://espeem.com
# Adapted from Web Dev Zone Frederico Thomazetti
# In the public domain.

import sys
import pkgutil

def explorePackage(
        module_name,
        modules=None,
        ):
    if modules is None:
        modules = []
        failed_modules = []
    loader = pkgutil.get_loader(module_name)

    for sub_module in pkgutil.walk_packages([loader.filename]):
        _, sub_module_name, _ = sub_module
        qname = module_name + "." + sub_module_name
        modules.append(qname)

        explorePackage(
                qname,
                modules=modules,
                )

    return modules
