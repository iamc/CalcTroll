# Written by Mads Engelund, 2017, http://espeem.com
import os
import numpy as np
import string
from os.path import join
from ase.visualize import view
from ase.data import chemical_symbols
import ase.data as data
from ase.atoms import Atoms
from ase.units import eV, Ang
from ase import io

from CalcTroll.Core.ParameterControl import ParameterControl
from CalcTroll.Core.Flags import DEFAULT, DONE, NOT_STARTED, INCOMPLETE
from CalcTroll.Core.Flags import TAIL, LOCAL, REMOTE

def makeIdentifierString(item):
    if isinstance(item, (tuple, list, np.ndarray)):
        identifiers = map(makeIdentifierString, item)
        identifiers = [identifier for identifier in identifiers if identifier !='']
        return '_'.join(identifiers)

    elif item is None:
        return ''
    elif isinstance(item, float):
        return '%.3f'%item
    else:
        return str(item)


class HasInputs(ParameterControl):
    def __init__(self,
            inputs=tuple(),
            ):
        self.setInputs(inputs)

    def setInputs(self, inputs):
        self.__inputs = list(inputs)

    def addInput(self, inp):
        self.__inputs.append(inp)

    def inputs(self):
        return self.__inputs

    def hasInputs(self):
        return (len(self.inputs()) > 0)

    def isReady(self):
        all_inputs = all([inp.isDone() for inp in self.inputs()])

        return all_inputs

    def isDone(self):
        if not self.isReady():
            return False

        return all([task.isDone() for task in self.tasks()])

    def isRunning(self):
        return self.host().isRunning(self)

    def isQueued(self):
        return self.host().isQueued(self)

class Task(HasInputs):
    def __init__(self,
                 path=DEFAULT,
                 shorthand=DEFAULT,
                 identifier=None,
                 method=DEFAULT,
                 host = DEFAULT,
                 inputs=tuple(),
                 submit_type=DEFAULT,
                 ):
        self.__status = None
        self.setMethod(method)
        self.setHost(host)
        if shorthand is DEFAULT:
            shorthand = self.className()
        self.__shorthand = shorthand
        self.__path = path
        self.__inputs = inputs
        self.__identifier = identifier
        self.setSubmitType(submit_type)
        self.__count = 0

        super(Task, self).__init__(inputs=inputs)

    def explain(self):
        raise NotImplementedError(self.name())

    def isDone(self):
        if not self.isReady():
            return False

        return self.status() == DONE

    def setMethod(self, method=DEFAULT):
        if method is DEFAULT:
            from CalcTroll.HOSTS import DEFAULT_METHOD
        self.__method = method

    def setHost(self, host=DEFAULT):
        if host is DEFAULT:
            from CalcTroll.Core.Submission.Workflow import WORKFLOW
            host = WORKFLOW.host()
        self.__host = host

    def setSubmitType(self, submit_type):
        if submit_type is DEFAULT:
            submit_type = self.host().submitType()
        self.__submit_type = submit_type

    def host(self):
        return self.__host

    def submitType(self):
        return self.__submit_type

    def identifier(self):
        return self.__identifier

    def path(self):
        return self.__path

    def jobName(self):
        return self.runFile()

    def method(self):
        return self.__method

    def shorthand(self):
        return self.__shorthand

    def dataPath(self, path=LOCAL):
        if self.host() is None:
            raise ValueError("Method dataPath on a Task instance cannot be" +
                    " called before a host is set")
        return self.host().dataPath(path=path)

    @classmethod
    def specificNode(cls):
        return None

    def setupCommand(self, utilities):
        pass

    def postCommand(self):
        return None

    def ncpus(cls):
        return None

    def _getLocalStatus(self):
        if not self.isReady():
            return NOT_STARTED

        outfile = self.outFile(path=LOCAL)
        runfile = self.runFile(path=LOCAL)
        status = self.method().checkIfFinished(outfile, runfile)

        return status

    def update(self):
        self.updateStatus()

    def updateStatus(self):
        for task in self.inputs():
            task.updateStatus()

        if self.isReady():
            status = self._getLocalStatus()
            if status is not DONE and self.host().isRemote():
                self.updateLocalFiles()
                status = self._getLocalStatus()
        else:
            status = NOT_STARTED

        assert status is not None
        self.__status = status

        return self.__status

    def updateLocalFiles(self):
        local = self.runDirectory(path=LOCAL)
        remote = self.runDirectory(path=REMOTE)
        if local == remote:
            return

        if not os.path.exists(local):
            os.makedirs(local)

        self.host().syncFiles(
                path=local,
                filters=self.neededFiles(),
                fro=LOCAL,
                to=REMOTE,
                )
        self.host().syncFiles(
                path=remote,
                filters=self.resultFiles(),
                fro=REMOTE,
                to=LOCAL,
                )


    def runDirectory(self, path=LOCAL):
        if path is TAIL:
            return ''

        return join(
                self.dataPath(path=path),
                self.path(),
                self.directoryBaseName(),
                )

    def status(self):
        if self.__status is None:
            self.__status = self.updateStatus()

        assert self.__status is not None

        return self.__status

    def neededFiles(self):
        return []

    def resultFiles(self):
        return []

    def isWritten(self):
        return os.path.exists(self.runFile())

    def write(self):
        raise NotImplementedError("The 'write' method is not implemented on the task: %s" % self)

    def copyFileNames(self):
        return list(np.unique(self.neededFiles() + self.resultFiles()))

    def hasFailed(self):
        return (not self.status() in (NOT_STARTED, DONE, INCOMPLETE))

    def directoryBaseName(self):
        return '%s%s'%(self.shorthand(), self.identifierString())

    def identifierString(self):
        identifier_string = makeIdentifierString(self.identifier())

        if identifier_string == '':
            return ''
        else:
            return '_%s' % identifier_string

    def runFileBasename(self):
        return 'RUN.' + self.method().runFileEnding()

    def outFileBasename(self):
        return 'RUN.' + self.method().outFileEnding()

    def runFile(self, path=LOCAL):
        filename = self.runFileBasename()

        return join(self.runDirectory(path=path), filename)

    def outFile(self, path=LOCAL):
        filename = self.outFileBasename()

        return join(self.runDirectory(path=path), filename)

    def cpuTime(self):
        raise Exception

    def initialTasks(self):
        return tuple()

    def inputDumpFile(self):
        if not self.isWritten():
            return None

        latest_input_dump = None
        max_value = -1
        for filename in os.listdir(self.runDirectory()):
            if filename[:3]=='fdf' and filename[-4:]=='.log':
                try:
                    value = int(filename[4:-4])
                except ValueError:
                    value = 0
                if value > max_value:
                    max_value = value
                    latest_input_dump = filename

        if latest_input_dump is None:
            return None
        return join(self.runDirectory(), latest_input_dump)

    def makeReadyToWrite(self):
        dirname = os.path.dirname(self.runFile(path=LOCAL))
        if not os.path.exists(dirname):
            os.makedirs(dirname)

    def executable(self):
        return self.method().executable()

    def estimate(self):
        raise NotImplementedError('"estimate" not implemented on ' + str(self.__class__))


class AbstractHasTasks(HasInputs):
    def tasks(self):
        raise NotImplementedError

    def tasksAndInputs(self):
        return self.tasks() + self.inputs()

    def hasTasks(self):
        return (len(self.tasks()) > 0)

    def update(self):
        self.updateStatus()
        self.updateTasks()

    def updateStatus(self):
        stati = []
        for entry in self.tasksAndInputs():
            stati.append(entry.updateStatus())


    def updateTasks(self):
        return

    def isDone(self):
        if not self.isReady():
            return False

        status = all([task.isDone() for task in self.tasks()])

        return status

class TaskCollection(AbstractHasTasks):
    def __init__(
            self,
            inputs=tuple(),
            tasks=tuple(),
            ):
        AbstractHasTasks.__init__(self, inputs)
        self.setTasks(tasks)

    def tasks(self):
        return self.__tasks

    def setTasks(self, tasks):
        self.__tasks = list(tasks)

    def addTask(self, task):
        self.__tasks.append(task)

    def addTasks(self, tasks):
        self.__tasks.extend(list(tasks))
