# Written by Mads Engelund, 2017, http://espeem.com
import os
from os.path import join
import numpy as np
from collections import OrderedDict
from CalcTroll.Core.Flags import DEFAULT, DONE, NOT_STARTED
from CalcTroll.Core.Task import Task, TaskCollection, AbstractHasTasks
from CalcTroll.Core.Plugin.Types import AnalysisProvider
from CalcTroll.Core.Relaxation import AbstractRelaxed, Relaxed
from CalcTroll.Core.InterfaceFunctions import calculate

class BaseAnalysis(AnalysisProvider, AbstractHasTasks):
    def __init__(self,
                 system,
                 method=DEFAULT,
                 parameters=DEFAULT,
                 inputs=tuple(),
                 ):
        if parameters is DEFAULT:
            parameters = system.parameters()

        if isinstance(system, AbstractRelaxed):
            method = system.method()

        if method is DEFAULT:
            from CalcTroll.HOSTS import DEFAULT_METHOD
            method = DEFAULT_METHOD

        if not isinstance(system, AbstractRelaxed):
            system = Relaxed(
                    system,
                    method=method,
                    parameters=parameters,
                    )

        self.__system = system

        if method is DEFAULT:
            method = system.method()

        self.__method = method

        self.__parameters = parameters

        inputs = list(inputs)
        if isinstance(system, AbstractHasTasks):
            inputs.extend(self.system().tasks())

        super(AbstractHasTasks, self).__init__(inputs=inputs)

    def method(self):
        return self.__method

    def identifier(self):
        return self.__identifier

    def system(self):
        return self.__system

    def parameters(self):
        return self.__parameters

    def read(self, **kwargs):
        raise NotImplementedError

    def frame(self, **kwargs):
        raise NotImplementedError

    def plot(self, ax=None, **kwargs):
        print self.__module__
        raise NotImplementedError('"plot" on %s' % self.className())


class Analysis(BaseAnalysis):
    def tasks(self):
        return []


class AnalysisCollator(AbstractHasTasks):
    def __eq__(self, other):
        if self.__class__ != other.__class__:
            return False

        for prop in self.mustMatch():
            if self.__properties[prop] != other.__properties[prop]:
                return False

        return True

    def __init__(
            self,
            items,
            ):
        self._setItems(items)

        super(AnalysisCollator, self).__init__([])

    def tasks(self):
        if self.__tasks is None:
            self.__tasks = self._getTasks()

        return self.__tasks

    def _getTasks(self):
        return self.method().makeTasks(items=self.items())

    def items(self):
        return self.__items

    def _setItems(self, items):
        properties = OrderedDict()
        for kw in self.mustMatch():
            attributes = [getattr(item, kw)() for item in items]
            assert len(np.unique(map(str, attributes))) == 1
            properties[kw] = attributes[0]

        self.__tasks = None
        self.__properties = properties
        self.__items = list(items)

    def system(self):
        return self.__properties['system']

    def method(self):
        return self.__properties['method']

    def parameters(self):
        return self.__properties['parameters']

    def mustMatch(self):
        return ['system',
                'method',
                'parameters',
                ]

    def read(self, item, **kwargs):
        task = self.method().makeTasks(items=[item])[-1]

        return task.readData(item, **kwargs)

class CollatableAnalysis(BaseAnalysis, AbstractHasTasks):
    collator_class = AnalysisCollator

    def collator(self, items=DEFAULT):
        if items is DEFAULT:
            items = [self]
        return self.collator_class(items=items)

    def read(self, **kwargs):
        calculate(self)

        if not self.isDone():
            return None

        return self.collator(items=[self]).read(self, **kwargs)

    def tasks(self):
        return self.collator(items=[self]).tasks()

    def fermiEnergy(self):
        return self.collator(items=[self]).tasks()[-1].fermiEnergy()
