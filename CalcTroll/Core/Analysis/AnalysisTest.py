# Written by Mads Engelund, 2017, http://espeem.com
import unittest
from CalcTroll.Core.Test.Case import CalcTrollTestCase
from CalcTroll.Core.Test.Dummies import TestSystem, TestMethod
from CalcTroll.Core.Analysis import CollatableAnalysis

class AnalysisTest(CalcTrollTestCase):
    def testConstruction(self):
        system = TestSystem()
        method = TestMethod()
        run = CollatableAnalysis(system=system, method=method)
        self.assertEqual(len(run.tasks()), 1)
        self.assertEqual(run.script(), 'CollatableAnalysis(system=TestSystem(), method=TestMethod())')

    def testInheritance(self):
        system = TestSystem()
        method = TestMethod()
        class Inheritor(CollatableAnalysis): pass
        run = Inheritor(system=system, method=method)
        self.assertEqual(len(run.tasks()), 1)
        self.assertEqual(run.script(), 'Inheritor(system=TestSystem(), method=TestMethod())')

if __name__=='__main__':
    unittest.main()
