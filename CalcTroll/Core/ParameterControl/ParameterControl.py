# Written by Mads Engelund, 2017, http://espeem.com
# This defines the base class of all plugins.
# The common functionality that this module defines is that the original call that sets up an instance is recorded.
# This means that the plugin can reproduce the script that created it and that it can be copied perfectly or while
# replacing one keyword.

import os
import numpy as np
import inspect
from collections import OrderedDict
import traceback

from CalcTroll.Core.Flags import ALL, MINIMAL, NO_DEFAULT, DEFAULT, CalcTrollFlag

def camelCase(string):
   words = string.split('_')
   first = words[0]
   return first + ''.join((w.capitalize() for w in words[1:]))


def copyInput(args, kwargs):
    nargs = []
    for arg in args:
        if isinstance(arg, ParameterControl):
            pass
        elif hasattr(arg, 'copy'):
            arg = arg.copy()

        nargs.append(arg)

    nkwargs = OrderedDict()
    for key, value in kwargs.iteritems():
        if isinstance(value, ParameterControl):
            pass
        elif hasattr(value, 'copy'):
            value = value.copy()

        nkwargs[key] = value

    return nargs, nkwargs

def makeDefaults(argspec):
    kwargs = OrderedDict()
    if argspec is None:
        return kwargs

    args, varagrs, keywords, defaults = argspec
    if args[0] != 'self':
        raise TypeError("ParameterControl initialized without 'self' as first argument")

    args = args[1:]
    if defaults is None:
        defaults = []
    for i in range(len(args) - len(defaults)):
        kwargs[args[i]] = NO_DEFAULT

    i =  len(args) - len(defaults)
    for j, default in enumerate(defaults):
        kwargs[args[i + j]] = defaults[j]

    args, kwargs = copyInput([], kwargs)

    return kwargs

class InspectParameters(type):
    def __new__(cls, clsname, bases, dct):
        result = type.__new__(cls, clsname, bases, dct)

        oldinit = None
        defaults = None
        if '__init__' in result.__dict__.keys():
            defaults = inspect.getargspec(result.__init__)
        else:
            for c in inspect.getmro(result)[1:]:
                if '__oldinit__' in c.__dict__.keys():
                    try:
                        defaults = inspect.getargspec(c.__oldinit__)
                    except TypeError:
                        defaults = None

                    oldinit = c.__oldinit__
                    break

        if oldinit is None:
            oldinit = result.__init__


        def initStub(self, *args, **kwargs):
            args, kwargs = copyInput(args, kwargs)
            self.__calls[result] = args, kwargs

            try:
                tmp = result.__oldinit__(self, *args, **kwargs)
            except TypeError, error:
                text = traceback.format_exc()
                raise TypeError("%s: " % self.__class__ +  error.message + text)

            return tmp

        result.__oldinit__ = oldinit
        result.__init__ = initStub
        result.__defaults = makeDefaults(defaults)

        return result

    def defaults(self):
        return self.__defaults.copy()

    def argumentNames(cls):
        return cls.defaults().keys()

    def callToDictionary(cls, call, detail_level=MINIMAL):
        args, kwargs = call
        args, kwargs = copyInput(args, kwargs)
        argument_names = cls.argumentNames()

        # Convert all to keywords.
        for init_arg, arg in zip(argument_names, args):
            kwargs[init_arg] = arg

        defaults = cls.defaults()
        if detail_level is ALL:
            nkwargs = defaults.copy()
        elif detail_level is MINIMAL:
            nkwargs = OrderedDict()

        for arg in argument_names:
            if arg in kwargs.keys():
                nkwargs[arg] = kwargs[arg]

        return nkwargs

class ParameterControl(object):
    __metaclass__ = InspectParameters
    def __hash__(self):
        return hash(self.script(detail_level=ALL))

    def __repr__(self):
        return self.script()

    def __eq__(self, other):
        s1 = self.script(detail_level=MINIMAL)
        s2 = other.script(detail_level=MINIMAL)

        result = s1 == s2

        return result

    def __ne__(self, other):
        return not self == other

    def __new__(self, *args, **kwargs):
        result = object.__new__(self, *args, **kwargs)
        result._InspectParameters__calls = OrderedDict()

        return result

    def __getitem__(self, key):
        return self.get(key)

    def __init__(self): pass

    def identifier(self):
        values = self.values()
        identifier = []
        for value in values:
            if isinstance(value, ParameterControl):
                identifier.append(value.identifier())
            else:
                identifier.append(value)

        return identifier

    def get(self, key, cls=DEFAULT):
        if cls is DEFAULT:
           cls = self.__class__

        call = self._InspectParameters__calls.get(cls)
        non_default_dict = cls.callToDictionary(call)

        if not key in non_default_dict.keys():
            try:
                result = cls.defaults()[key]
            except KeyError:
                raise KeyError("%s does not take the argument %s"%(cls, key))
            return result
        else:
            return non_default_dict[key]

    def iteritems(self):
        return self.toDictionary(detail_level=ALL).iteritems()

    def __calls(self):
        return self._InspectParameters__calls

    def toDictionary(self, detail_level=MINIMAL):
        call = self.__calls()[self.__class__]
        return self.__class__.callToDictionary(call, detail_level=detail_level)

    def keys(self):
        return self.__class__.argumentNames()

    def values(self):
        return self.toDictionary(detail_level=ALL).values()

    def copy(self, *args, **kwargs):
        init_args = self.keys()
        assert len(args) < init_args

        nkwargs = self.toDictionary(detail_level=ALL)
        minimal_arguments = set(\
                self.toDictionary(detail_level=MINIMAL).keys() +\
                kwargs.keys())

        # Convert all to keywords.
        for init_arg, arg in zip(init_args, args):
            nkwargs[init_arg] = arg

        nkwargs.update(kwargs)

        fkwargs = OrderedDict()
        for key, value in nkwargs.iteritems():
            if key in minimal_arguments:
                fkwargs[key] = value

        new = self.__class__(**fkwargs)

        return new

    def script(self, detail_level=MINIMAL, exclude=tuple()):
        name = self.className()
        dictionary = self.toDictionary(detail_level)

        script = makeScript(
                     name=name,
                     dictionary=dictionary,
                     detail_level=detail_level,
                     )

        return script

    def save(self, filename, data=None, **kwargs):
        if data is None:
            data = self.read()

        basename, ext = os.path.splitext(filename)

        func = ext[1:] + '_save'
        save_function = getattr(self, func)
        frame = save_function(filename, data, **kwargs)

        return frame

    def name(self):
        return self.className()

    @classmethod
    def className(cls):
        return str(cls)[:-2].split('.')[-1]


def makeScript(name, dictionary, detail_level):
    arguments = []
    script = '%s('%name
    for key, value in dictionary.iteritems():
        string = scriptValue(value, detail_level)

        arguments.append('%s=%s'%(key, string))
    script += ', '.join(arguments)
    script += ')'

    return script

def scriptValue(value, detail_level):
    if hasattr(value, 'script'):
        string = value.script(detail_level=detail_level)
    elif isinstance(value, np.ndarray):
        string = str(tuple(value))
    elif isinstance(value, tuple):
        string = str(value)
    elif isinstance(value, list):
        string = '[' + ', '.join([scriptValue(entry, detail_level) for entry in value]) + ']'
    elif isinstance(value, str):
        string = "'" + value + "'"
    elif inspect.isclass(value) and issubclass(value, CalcTrollFlag):
        string =  value.className()
    else:
        string = str(value)

    return string
