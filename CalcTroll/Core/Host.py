# Written by Mads Engelund, 2017, http://espeem.com
import os, sys, string
from os.path import join, relpath
import argparse
import getpass

from CalcTroll.Core.Plugin.Types import HostProvider
from CalcTroll.Core.Flags import LOCAL, DEFAULT, REMOTE
from CalcTroll.Core.InterfaceFunctions import submit, estimate

class Host(HostProvider):
    def __init__(self,
                 data_path='data',
                 local_path=None,
                 submit_type=None,
                 ):
        self.__data_path = data_path
        self.__local_path = local_path
        self.__submit_type = submit_type

    def submitType(self):
        return self.__submit_type

    def setSubmitType(self, submit_type):
        self.__submit_type = submit_type

    def localPath(self):
        return self.__local_path

    def dataPath(self, path=LOCAL):
        return join(self.topFolder(path=path), self.__data_path)

    def utilities(self):
        raise Exception

    def isRunning(self, run):
        raise NotImplementedError

    def isQueued(self, run):
        raise NotImplementedError

    def getSubmitted(self, identifier=None):
        raise NotImplementedError

    def setSubmitType(self, submit_type):
        self.__submit_type = submit_type

    def submitType(self):
        return self.__submit_type

    def scriptPath(self):
        raise NotImplementedError

    def isRemote(self):
        raise NotImplementedError

    def username(self):
        return self.__username

    def dataPath(self, path=LOCAL):
        return join(self.topFolder(path=path), self.__data_path)

    @classmethod
    def targetJobId(cls, job_in):
        raise NotImplementedError

    @classmethod
    def cancelJobId(cls, job_id):
        raise Exception

    def lsJob(self):
        raise NotImplementedError

    def allNodes(self, info):
        raise NotImplementedError

    def headNode(self, info):
        return self.allNodes(info)[0]

    def localCommandFile(self, info):
        raise NotImplementedError

    def localCommandDirectory(self, info):
        return os.path.dirname(self.localCommandFile(info))

    def runningJobsInFolder(self):
        raise NotImplementedError

    def jobId(self, info):
        raise NotImplementedError

    def copyJob(self, filename=None, info=None):
        raise NotImplementedError

    def cancelQueued(self):
        raise NotImplementedError

    def jobActive(self, info):
        raise NotImplementedError

    def whatIsRunning(self):
        raise NotImplementedError

    def cancelJobs(self):
        raise NotImplementedError

    def removeJobs(self):
        raise NotImplementedError

    def catJob(self, filename=None):
        raise NotImplementedError

    def tailJob(self, filename=None):
        raise NotImplementedError

    def findDefaultJob(self):
        raise NotImplementedError

    def outputFileCommand(self, cmd, filename=None):
        raise NotImplementedError

    def getJobInfo(self, jobid):
        raise NotImplementedError

    def utilities(self):
        return {}

    @classmethod
    def run(cls, *args):
        raise NotImplementedError

    def setup(self, task):
        task.setupCommand(utilities=self.utilities())
        submission_file = self.writeSubmissionFile(task)

        return (submission_file, )

    def writeSubmissionFile(self, task):
        raise NotImplementedError

    def writeSubmissionFile(self, run):
        script = self.header(run)
        script += self.setupScript(run)
        script += self.methodScript(run)

        cmd = run.postCommand()
        if cmd is not None:
            script += cmd

        script += self.tearDownScript(run)

        submission_file = '.'.join(run.runFile().split('.')[:-1]) + self.fileExtension()
        with open(submission_file, 'w') as f:
            f.write(script)

        return submission_file

    def header(self, task):
        return ''

    def setupScript(self, task):
        return ''

    def methodScript(self, task):
        raise NotImplementedError

    def tearDownScript(self, task):
        return ''

    @classmethod
    def fileExtension(cls):
        raise NotImplementedError

    def topFolder(self, path=LOCAL):
        raise NotImplementedError

    def submitParser(self):
        """ Return the parser of the input to the 'mysubmit'
            executable."""
        parser = argparse.ArgumentParser("ArgumentParser")
        parser.add_argument("filename")
        parser.add_argument("-d", "--dry_run",
                            action='store_true',
                            )
        parser.add_argument("-l", "--loop",
                            action='store_true', default=False,
                            )
        parser.add_argument("-s", "--skip_fail",
                            action='store_true', default=False,
                            )
        parser.add_argument("-c", "--cleanup",
                            action='store_true', default=False,
                            )
        parser.add_argument("-e", "--explain",
                            action='store_true', default=False,
                            )

        return parser

    def submit(
            self,
            filename,
            **kwargs
            ):
        ext = self.fileExtension()
        if filename[-len(ext):] == ext:
            self.run(filename)
        else:
            self.setSubmitType(kwargs)
            submit(
                   filename=filename,
                   **kwargs
                   )


    def estimateParser(self):
        parser = argparse.ArgumentParser("ArgumentParser")
        parser.add_argument("filename")

        return parser

    def estimate(self, filename):
        return estimate(filename, host=self)


class SubmissionServer(Host):
    def __init__(self,
                 server,
                 username=DEFAULT,
                 remote_path=None,
                 data_path='data',
                 script_path='scripts',
                 local_path=None,
                 submit_type=None,
                 ):
        if username is DEFAULT:
            username = getpass.getuser()
        self.__username = username
        self.__server = server
        self.__script_path = script_path
        self.__remote_path = remote_path
        self.__cache_job_info = {}

        Host.__init__(
                self,
                local_path=local_path,
                data_path=data_path,
                submit_type=submit_type,
                )

    def server(self):
        return self.__server

    def remotePath(self):
        return self.__remote_path

    def topFolder(self, path=LOCAL):
        if path is LOCAL:
            return self.localPath()
        elif path is REMOTE:
            remote_path = self.remotePath()
            if remote_path is None:
                remote_path = self.localPath()
            return remote_path

    def changePathType(self, path, to=REMOTE, fro=LOCAL):
        path = os.path.relpath(path, self.topFolder(fro))
        return join(self.topFolder(to), path)

    def scriptPath(self, path=LOCAL):
        return join(self.topFolder(path=path), self.__script_path)

    def isRemote(self):
        return self.__remote_path is not None

    def username(self):
        return self.__username

    def isRunning(self, run):
        raise NotImplementedError

    def isQueued(self, run):
        raise NotImplementedError

    @classmethod
    def targetJobId(cls, job_in):
        return job_in

    @classmethod
    def cancelJobId(cls, job_id):
        raise Exception

    def executeCommand(self, cmd, verbose=True, output=False):
        raise NotImplementedError

    def syncFiles(self, path, filters, fro, to):
        if not self.isRemote():
            return

        remote = self.changePathType(path, to=REMOTE, fro=fro)
        local = self.changePathType(path, to=LOCAL, fro=fro)
        full_remote = "%s@%s:%s" % (self.username(), self.server(), remote)

        cmd = 'rsync -vazr '
        if fro == LOCAL and to == REMOTE:
            original, target = local, full_remote
            cmd += """--rsync-path="mkdir -p %s && rsync" """ % remote
        elif fro == REMOTE and to == LOCAL:
            original, target = full_remote, local

        for filename in filters:
            cmd += '--include="%s" '%filename

        cmd += '--exclude="*" '
        cmd += '%s/ %s'%(original, target)

        os.system(cmd)

    def syncFile(self, filename, to, fro):
        filename = os.path.abspath(filename)
        dirname = os.path.dirname(filename)
        basename = os.path.basename(filename)
        self.syncFiles(
                path=dirname,
                filters=[basename],
                to=to,
                fro=fro,
                )

    def lsJob(self):
        info = self.findDefaultJob()
        cmd = 'ssh %s ls -lhtr %s'%(self.headNode(info), self.remoteTmpOutFolder(info))
        self.executeCommand(cmd, verbose=True)

    def allNodes(self, info):
        nodelist = info['NodeList']
        if '[' in nodelist:
            head, nodes = nodelist.split('[')
            nodes = nodes[:-1]
            nodes = nodes.split(',')
            nodelist = []
            for entry in nodes:
                if '-' in entry:
                    start, end = map(int, entry.split('-'))
                    nodelist.extend([head+'%d'%i for i in range(start, end+1)])
                else:
                    nodelist.append(head + entry)
        else:
            nodelist = [nodelist]

        return nodelist

    def headNode(self, info):
        return self.allNodes(info)[0]

    def localCommandFile(self, info):
        return info['Command']

    def localCommandDirectory(self, info):
        return os.path.dirname(self.localCommandFile(info))

    def remoteTmpOutFile(self, info):
        jobid = info['JobId']
        command = info['Command']
        slurm_out = join(os.path.dirname(command), 'slurm-%s.out'%jobid)

        outfile = None
        with open(slurm_out, 'r') as f:
            for line in f:
                if 'OUTFILE' in line:
                    outfile = line.strip().split('=')[-1]

        if outfile is None:
            outfile = join('/lscratch2/%s/job_%s'%(self.username(), jobid), 'TMP.out')

        return outfile

    def remoteTmpOutFolder(self, info):
        return os.path.dirname(self.remoteTmpOutFile(info))

    def runningJobsInFolder(self):
        cwd = os.getcwd()
        submitted = self.getSubmitted('id')
        submitted = [jobid for jobid in submitted if cwd in self.getJobInfo(jobid)['Command']]

        return submitted

    def jobId(self, info):
        return info['JobId']

    def copyJob(self, filename=None, info=None):
        if info is None:
            info = self.findDefaultJob()

        path = '/lscratch2/%s/*%s'%(self.username(), self.jobId(info))
        if filename is None:
            cmd = 'ssh %s cp -r %s/* %s'
        else:
            cmd = 'ssh %s cp -r %s/' + filename + ' %s'

        cmd = cmd % (self.headNode(info),
                     path,
                     self.localCommandDirectory(info)
                     )
        self.executeCommand(cmd, output=False, verbose=True)

    def cancelQueued(self):
        ids = self.getSubmitted()

        for job_id in ids:
            info = self.getJobInfo(job_id)
            if not self.jobActive(info):
                self.cancelJobId(job_id)

    def jobActive(self, info):
        return (info['JobState'] == 'RUNNING')

    def whatIsRunning(self):
        cwd = os.getcwd()
        if self.isRemote():
            cwd = self.changePathType(cwd, to=REMOTE, fro=LOCAL)

        jobids = self.runningJobsInFolder()
        for jobid in jobids:
            info = self.getJobInfo(jobid)
            path = relpath(info['Command'], cwd)
            print '%s (%s, %s)'%(path, jobid, info['RunTime'])

    def cancelJobs(self):
        for jobid in self.runningJobsInFolder():
            info = self.getJobInfo(jobid)
            self.copyJob(info=info)

        self.removeJobs()

    def removeJobs(self):
        for jobid in self.runningJobsInFolder():
            info = self.getJobInfo(jobid)
            path = '/lscratch2/%s/*%s'%(self.username(), jobid)
            cmd = 'scancel %s'%(jobid)
            self.executeCommand(cmd, output=False, verbose=True)

            for node in self.allNodes(info):
                cmd = 'ssh %s rm -rf %s'%(node, path)
                self.executeCommand(cmd, output=False, verbose=True)

    def catJob(self, filename=None):
        cmd = 'cat'
        self.outputFileCommand(cmd, filename=filename)

    def tailJob(self, filename=None):
        cmd = 'tail -f'
        self.outputFileCommand(cmd, filename)

    def findDefaultJob(self):
        topdir = os.getcwd()
        slurms = [f for f in os.listdir(topdir) if f.split('-')[0]=='slurm']

        all_jobs = self.getSubmitted()
        jobids = [slurm[6:-4] for slurm in slurms]
        get_jobs = [jobid for jobid in jobids if jobid in all_jobs]
        if len(get_jobs) == 0:
            raise Exception('No submitted jobs found')

        jobid = get_jobs[0]
        info = self.getJobInfo(jobid)

        return info

    def outputFileCommand(self, cmd, filename=None):
        topdir = os.getcwd()
        info = self.findDefaultJob()
        outfile = self.remoteTmpOutFile(info)
        if not filename is None:
            outfile = join(os.path.dirname(outfile), filename)
        full_cmd = 'ssh %s %s %s'%(self.headNode(info), cmd, outfile)
        self.executeCommand(full_cmd, output=False)

    def getJobInfo(self, jobid):
        if jobid in self.__cache_job_info.keys():
            return self.__cache_job_info[jobid]

        cmd = 'scontrol show job %s'%jobid
        lines = self.executeCommand(cmd, output=True)
        info = {}
        for line in lines:
            line = line.strip().split(' ')
            for entry in line:
                if '=' in entry:
                    key, value = entry.split('=')
                    info[key] = value

        self.__cache_job_info[jobid] = info

        return info

    def utilities(self):
        return {}

    @classmethod
    def getSubmitted(cls, identifier='name'):
        raise Exception

    @classmethod
    def writeSubmitted(cls):
        raise Exception

    @classmethod
    def run(cls, submission_file):
        raise Exception

    @classmethod
    def fileExtension(cls):
        raise Exception

    @classmethod
    def getSubmitted(cls, identifier='name'):
        raise NotImplementedError

    @classmethod
    def writeSubmitted(cls):
        raise NotImplementedError

    def header(self, run):
        raise NotImplementedError

    def cleanAll(self):
        raise NotImplementedError

    def waitToFinish(self, time_step):
        """
        Will wait for the currently submitted tasks to finish.
        """
        raise NotImplementedError
