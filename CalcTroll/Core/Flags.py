# Written by Mads Engelund, 2017, http://espeem.com
# Contains flags and exception that are used in both core and modules.

class CalcTrollFlag:
    @classmethod
    def className(cls):
        return str(cls).split('.')[-1]

# Generic word flags
class ALL(CalcTrollFlag): pass
class MINIMAL(CalcTrollFlag): pass
class DEFAULT(CalcTrollFlag): pass
class NO_DEFAULT(CalcTrollFlag): pass
class INTEGRATED(CalcTrollFlag): pass
class DIFFERENTIAL(CalcTrollFlag): pass

# Approximation flags.
class FULL(CalcTrollFlag):pass
class COLLINEAR(CalcTrollFlag):pass

# File-based flags.
class NO_SUCH_FILE(CalcTrollFlag):pass
class NOT_STARTED(CalcTrollFlag):pass
class FAILED(CalcTrollFlag):pass
class NOT_CONVERGED(CalcTrollFlag):pass
class INCOMPLETE(CalcTrollFlag):pass
class DONE(CalcTrollFlag):pass

# Path-based flags.
class TAIL(CalcTrollFlag): pass
class LOCAL(CalcTrollFlag): pass
class REMOTE(CalcTrollFlag): pass

# Performance limit flags.
class SOFT_MEMORY_LIMIT(CalcTrollFlag):pass

# Exceptions that will sometimes be automatically handled.
class CalcTrollException(CalcTrollFlag, Exception): pass
class TooHeavy(CalcTrollFlag, Exception): pass
class InconsistentApproximation(CalcTrollException): pass
class TooSmallBoundingBox(CalcTrollException): pass
