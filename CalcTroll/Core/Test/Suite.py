# Written by Mads Engelund, 2017, http://espeem.com
import os
import sys
import unittest
import importlib
import inspect

from CalcTroll.Core.Test.Case import CalcTrollTestCase

TEST_SUITE = unittest.TestSuite()

def importWalk(tests_path, method=None):
    if method is None:
        def method(imp):
            print "Importing %s..."% imp
            importlib.import_module(imp)

    for path, dirs, files in os.walk(tests_path):
        if 'Test' in path:
            continue

        if 'Setup' in path:
            continue

        for f in files:
            if f == 'Tests.py':
                continue
            elif f[-3:] != '.py':
                continue
            relpath = os.path.relpath(path, tests_path)
            if relpath == '.':
                imp = f[:-3]
            else:
                relpath = relpath.split(os.path.sep)
                imp = '.'.join(relpath)
                imp = '.'.join([imp, f[:-3]])

            method(imp)

def isTestModule(module_name):
    is_suite = module_name[-4:] == 'Test'
    return is_suite

def findTestCases(imp):
    if not isTestModule(imp):
        return

    #print "Importing %s..."%imp
    mod = importlib.import_module(imp)
    for key in mod.__dict__.keys():
        if '__' in key:
            continue
        exec('from %s import %s as tmp'%(imp, key))

        if inspect.isclass(tmp) and issubclass(tmp, CalcTrollTestCase):
            TEST_SUITE.addTest(unittest.makeSuite(tmp))

class ImportTests(CalcTrollTestCase):
    def testImportAll(self):
        importWalk(tests_path=self.tests_path)
