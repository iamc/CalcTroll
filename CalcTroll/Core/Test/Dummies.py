# Written by Mads Engelund, 2017, http://espeem.com
from ase.atoms import Atoms

from CalcTroll.Core.Task import Task
from CalcTroll.Core.Flags import DEFAULT, MINIMAL, DONE
from CalcTroll.Core.Host import Host
from CalcTroll.Core.ParameterControl import ParameterControl
from CalcTroll.Core.Program import Method
from CalcTroll.Core.System import System
from CalcTroll.Core.Analysis import Analysis
from CalcTroll.Core.Relaxation.Relaxed import Relaxed

TEST_ATOMS = Atoms('CC', [[0, 0, -1], [0, 0, 1]])

class TestSystemParameters(ParameterControl):
    def __init__(self, arg1=4, arg2='test'): pass

    def ignoreLongRangeCorrelation(self):
        return self

class TestRelaxationTask(Task):
    def __init__(self,
                 system,
                 method,
                 path=DEFAULT,
                 host=DEFAULT,
                 submit_type=DEFAULT,
                 ):
        Task.__init__(
                self,
                method=method,
                path=path,
                host=host,
                submit_type=submit_type,
                )
        self.__system = system

    def _getStatus(self):
        return DONE

    def relaxation(self):
        atoms = self.__system.atoms()
        atoms.positions *= 1.1

        return atoms


class TestHost(Host):
    def dataPath(self):
        return self.localDataPath()

    def localPath(self):
        return CalcTrollTestCase.pathForTesting(__file__)

class TestAnalysis(Analysis):
    pass

class TestMethod(Method):
    def makeRelaxation(self, item):
        path = item.systemClass().className()
        task = TestRelaxationTask(
                     system=item.system(),
                     method=self,
                     path=path,
                     host=TestHost(),
                     submit_type='Test')

        return task

    def makeTasks(self, items):
        return [Task()]

    def outFileEnding(self):
        return 'test'

    def runFileEnding(self):
        return 'test'

    def checkIfFinished(self, outfile, runfile):
        return True


class TestRelaxationSystem(System):
    def systemClass(self):
        return self.__system.__class__

    def __new__(cls, system, relaxation, parameters=DEFAULT):
        name = 'Test' + system.className()
        cls = type(name,
                   (cls, system.__class__),
                    system.__dict__)

        instance = System.__new__(cls)

        return instance

    def __init__(self, system, relaxation, parameters=DEFAULT):
        self.__relaxation = relaxation
        self.__system = system
        self.__parameters = parameters

    def relaxation(self):
        return self.__relaxation

    def fullyUnrelaxed(self):
        return self.__system.fullyUnrelaxed()


class TestSubSystem(System):
    @classmethod
    def className(cls):
        return 'TestSub'

    @classmethod
    def parameterClass(cls):
        return TestSystemParameters

    def defaultParameters(self, parameters=DEFAULT):
        return self.parameterClass()()

    def atomsTemplate(self, parameters=DEFAULT):
        return TEST_ATOMS.copy()

    def subSystems(self):
        return []


class TestSystem(System):
    @classmethod
    def className(cls):
        return 'TestSystem'

    @classmethod
    def parameterClass(cls):
        return TestSystemParameters

    def __init__(self, method=DEFAULT, parameters=DEFAULT):
        System.__init__(self, sub_systems=[TestSubSystem()])

    def defaultParameters(self, parameters=DEFAULT):
        return self.parameterClass()()

    def subParameters(self, parameters=DEFAULT):
        return (TestSystemParameters(), )

    def makeAtomsRelaxed(self, atoms, tolerance=1):
        atoms = self.makeSubsystemsRelaxed(atoms)
        if self.isRelaxed():
            atoms.positions *= 1.1

        return atoms

    def atomsTemplate(self, parameters=DEFAULT):
        atoms = self.subSystems()[0].atomsTemplate()

        return atoms
