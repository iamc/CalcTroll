# Written by Mads Engelund, 2017, http://espeem.com
import os
import sys
from CalcTroll.Core.Flags import DEFAULT

def makeSubmissionScript(filename, submit_type=DEFAULT):
    #module = loadScript(filename)
    #ALL_RUNS.script()
    with open(filename, 'r') as f:
        lines = f.readlines()

    name = '.'.join(filename.split('.')[:-1])

    s_filename = name + '__submit.py'
    with open(s_filename, 'w') as f:
        f.write(SUBMIT_TYPE_IDENTIFIER + ': %s\n' % submit_type)
        for line in lines:
            f.write(line)

    return s_filename

def loadScript(filename, should_reload=True):
    assert os.path.exists(filename)
    filename = os.path.abspath(filename)
    dirname, basename = os.path.split(filename)
    sys.path.append(dirname)
    name = '.'.join(basename.split('.')[:-1])
    try:
        module = __import__(name)
    except Exception, error:
        print "Warning: Reading the script: '%s' raised the following error:" % basename
        print error.message
    else:
        if should_reload:
            reload(module)

    sys.path.pop(-1)
    os.chdir(dirname)


def loadAllScripts(scripts, should_reload=True):
    modules = []
    for script in scripts:
        with open(script, 'r') as f:
            first_line = f.readline().strip()
        if SUBMIT_TYPE_IDENTIFIER in first_line:
            submit_type = first_line.split()[-1]
        else:
            submit_type = DEFAULT
        ALL_RUNS.setSubmitType(submit_type)
        module = loadScript(script, should_reload=should_reload)
