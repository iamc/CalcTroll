# Written by Mads Engelund, 2017, http://espeem.com
import os
from collections import OrderedDict

class Clusters(OrderedDict):
    def __getitem__(self, item):
        try:
            value = dict.__getitem__(self, item)
        except KeyError:
            raise Exception("The current hostname '%s' was not included in the variable""" % item +
                            " 'Clusters' in the CalcTrollHosts.py file. Please edit this file.")
        else:
            return value

CLUSTERS = Clusters()
DEFAULT_CLUSTER_KEY = os.uname()[1]
