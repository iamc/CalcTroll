# Written by Mads Engelund, 2017, http://espeem.com
import os
import fnmatch
from os.path import isfile, join
import numpy as np
from CalcTroll.Core.Task import Task, AbstractHasTasks, HasInputs
from CalcTroll.Core.Analysis import CollatableAnalysis, AnalysisCollator
from CalcTroll.Core.Flags import DEFAULT, MINIMAL, ALL
from CalcTroll.HOSTS import CLUSTERS, DEFAULT_CLUSTER_KEY
from CalcTroll.Core.Utilities import unique
from CalcTroll.Core.Explanation import Explanation

SUBMIT_TYPE_IDENTIFIER = '# Submit type:'


class Workflow:
    def __init__(self, host=DEFAULT):
        self.reset()
        self.setHost(host)

    def reset(self):
        self.__requests = []
        self.__tasks = []

    def __getitem__(self, sl):
        return self.requests()[sl]

    def append(self, item):
        assert not isinstance(item, Task)
        self.__requests.append(item)

    def requests(self):
        return list(self.__requests)

    def setHost(self, host):
        if host is DEFAULT:
            host = CLUSTERS[DEFAULT_CLUSTER_KEY]
        self.__host = host

    def setSubmitType(self, submit_type):
        self.host().setSubmitType(submit_type)

    def submitType(self):
        return self.host().submitType()

    def host(self):
        return self.__host

    def updateTasks(self):
        i = 0
        runs = self.requests()
        for run in runs:
            run.updateStatus()

        while i < len(runs):
            run = runs[i]
            if run.hasInputs():
                runs.extend(run.inputs())

            i+=1

        runs = unique(runs)

        has_collator = np.array([isinstance(run, CollatableAnalysis) for run in runs], bool)
        ctasks = np.array(runs)[has_collator]
        left_tasks = np.array(runs)[np.logical_not(has_collator)]
        runs = list(left_tasks)

        collators = []
        for task in ctasks:
            collator = task.collator()
            if not (collator in collators):
                collators.append(collator)


        new_collators = []
        for collator in collators:
            if collator is None:
                continue
            tasks = [task for task in ctasks if task.collator() == collator]
            tasks = unique(tasks)
            new_collators.append(tasks[0].collator(items=tasks))

        runs.extend(new_collators)

        i = 0
        while i < len(runs):
            run = runs[i]
            if isinstance(run, HasInputs):
                runs.extend(run.inputs())
            i+=1

        i = 0
        while i < len(runs):
            run = runs[i]
            if isinstance(run, AbstractHasTasks):
                run.updateTasks()
                runs.extend(run.tasks())
            if isinstance(run, HasInputs):
                runs.extend(run.inputs())

            if isinstance(run, Task):
                i+=1
            else:
                runs.pop(i)

        self.__tasks = unique(runs)

        return self.tasks()

    def updateStatus(self):
        for task in self.tasks():
            task.updateStatus()

    def tasks(self):
        return self.__tasks


    def explain(self):
        described = []
        tasks = list(self.tasks())
        while len(tasks) > 0:
            for task in tasks:
                undescribed = set(task.inputs()) - set(described)
                if len(undescribed) == 0:
                    described.append(task)
                    tasks.remove(task)

        ex = Explanation()
        for task in described:
            this_ex = task.explain()
            ex += this_ex

        return ex

    def status(self, detail_level=MINIMAL):
        host = self.host()
        done = []
        running = []
        not_ready = []
        ready = []
        queued = []
        failures = []
        for run in self.tasks():
            if run.isDone():
                done.append(run)
            elif not run.isReady():
                not_ready.append(run)
            elif host.isRunning(run):
                running.append(run)
            elif host.isQueued(run):
                queued.append(run)
            elif run.hasFailed():
                failures.append(run)
            else:
                ready.append(run)

        if detail_level == ALL:
            script = writeAllInfo(done=done, not_ready=not_ready, running=running, queued=queued, failures=failures, ready=ready)
        elif detail_level == MINIMAL:
            script = writeMinimalInfo(done=done, not_ready=not_ready, running=running, queued=queued, failures=failures, ready=ready)

        return script

    def cleanUp(self, dry_run=True):
        protect_patterns = dict([(task.runDirectory(), set()) for task in self.tasks()])

        for task in self.tasks():
            protect_patterns[task.runDirectory()].update(task.neededFiles())
            protect_patterns[task.runDirectory()].update(task.resultFiles())

        for path, protect in protect_patterns.iteritems():
            filenames = [f for f in os.listdir(path) if isfile(join(path, f))]
            protected = []
            for pattern in protect:
                protected.extend(fnmatch.filter(filenames, pattern))

            delete = set(filenames) - set(protected)
            for filename in delete:
                filename = os.path.join(path, filename)
                print 'remove %s'  % filename
                if not dry_run:
                    os.remove(filename)


def writeAllInfo(done, not_ready, running, queued, failures, ready):
    script = ''
    script += writeJobInfo('Done', done)
    script += writeJobInfo('Not ready', not_ready)
    script += writeJobInfo('Failed', failures)
    script += writeJobInfo('Running', running)
    script += writeJobInfo('Queued', queued)
    script += writeJobInfo('Ready but not submitted', ready)

    return script

def writeMinimalInfo(done, not_ready, running, queued, failures, ready):
        script = ''
        script += '%d are done\n' % len(done)
        script += '%d are running\n' % len(running)
        script += '%d are queued\n' % len(queued)
        script += '%d are not ready\n' % len(not_ready)
        script += '%d failed\n'%len(failures)

        if len(ready) > 0:
            script += '%d ready but not submitted\n'%len(ready)

        return script

def writeJobInfo(text, run_list):
    if len(run_list) == 0:
        return ''

    script = text + ':\n'
    for run in run_list:
        script += run.runFile() + '\n'

    return script

WORKFLOW = Workflow()
