# Written by Mads Engelund, 2017, http://espeem.com
import os
import hashlib
from os.path import join

from CalcTroll.Core.Flags import DEFAULT, ALL
from Utilities import loadScript

MAX_TIME = 10*24*60*60
TIME_STEP = 2


def submitWorkflow(
        workflow,
        dry_run=False,
        skip_fail=False,
        ):
    cwd = os.getcwd()
    workflow.updateTasks()
    host = workflow.host()

    for task in workflow.tasks():
        if task.isDone():
            pass
        elif task.isReady() and \
           not task.isRunning() and \
           not task.isQueued() and \
           ((not task.hasFailed()) or skip_fail):

            print 'Writing %s'%task.jobName()
            task.makeReadyToWrite()
            task.write()

            args = host.setup(task)
            if not dry_run:
                host.run(*args)
                break

    workflow.updateStatus()
    print workflow.status(detail_level=ALL)
    more_to_do = [run.isReady() \
                     and not (run.isDone() or run.hasFailed()) for run in workflow.tasks()]
    more_to_do = any(more_to_do)

    os.chdir(cwd)

    return more_to_do


def isDone(dry_run, more_to_do, timeout, loop):
    return dry_run or (not more_to_do) or timeout or (not loop)


def calculate(run):
    from CalcTroll.Core.Submission.Workflow import WORKFLOW
    WORKFLOW.append(run)

def submit(
        filename,
        dry_run=False,
        loop=False,
        skip_fail=False,
        cleanup=False,
        explain=False,
        host=DEFAULT,
):
    # Create file such that a parallel thread can see
    # if the submission is in progress.
    progress_filename = createProgressFile(filename)

    from Workflow import WORKFLOW
    WORKFLOW.setHost(host)
    loadScript(filename, should_reload=False)
    more_to_do = submitWorkflow(
        workflow=WORKFLOW,
        dry_run=dry_run,
        skip_fail=skip_fail,
    )
    timeout = False
    done = isDone(
        dry_run=dry_run,
        more_to_do=more_to_do,
        timeout=timeout,
        loop=loop,
    )

    total_time=0
    while not done:
        WORKFLOW.host().waitToFinish(time_step=TIME_STEP)
        WORKFLOW.updateStatus()

        more_to_do = submitWorkflow(workflow=WORKFLOW, dry_run=dry_run)
        timeout = total_time > MAX_TIME
        done = isDone(
            dry_run=dry_run,
            more_to_do=more_to_do,
            timeout=timeout,
            loop=loop,
        )

    WORKFLOW.updateStatus()
    loadScript(filename, should_reload=True)

    if explain:
        ex = WORKFLOW.explain()
        with open('explanation.txt', 'w') as f:
            f.write(ex.text())

    if cleanup:
        WORKFLOW.cleanUp(dry_run=dry_run)

    # Remove the progress file on termination.
    os.remove(progress_filename)


def createProgressFile(filename):
    progress_filename = os.path.splitext(filename)[0] + "_mysubmit_progress"
    with open(progress_filename, 'a') as f:
        f.write(progress_filename)

    return progress_filename

if __name__=='__main__':
    from systems import names
    cwd=os.getcwd()
    paths=[join(cwd,name) for name in names]
    submitter(paths)
