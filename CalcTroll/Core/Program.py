# Written by Mads Engelund, 2017, http://espeem.com
from CalcTroll.Core.Plugin.Types import ProgramProvider

class Method(ProgramProvider):
    def program(self):
        raise NotImplementedError

    def makeTasks(self, items):
        raise NotImplementedError

class Program(Method):
    def program(self):
        return self
