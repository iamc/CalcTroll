# Written by Mads Engelund, 2017, http://espeem.com
import unittest

from CalcTroll.Core.Test.Suite import CalcTrollTestCase
from CalcTroll.Core.Test.Dummies import TEST_ATOMS, TestSystemParameters
from CalcTroll.Core.Test.Dummies import TestSystem

from Unrelaxed import Unrelaxed

class UnrelaxedTest(CalcTrollTestCase):
    def setUp(self):
        self.__system = TestSystem()
        CalcTrollTestCase.setUp(self)

    def testConstruction(self):
        relaxed = Unrelaxed(self.__system)
        self.assertEqual(repr(relaxed), 'Unrelaxed(system=TestSystem())')
        self.assertEqual(relaxed.defaultParameters(), TestSystemParameters())
        self.assertEqual(relaxed.subParameters(), (TestSystemParameters(), ))
        self.assertAtomsEqual(relaxed.atoms(), TEST_ATOMS)


if __name__=='__main__':
    unittest.main()
