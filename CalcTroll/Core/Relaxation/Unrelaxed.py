# Written by Mads Engelund, 2017, http://espeem.com
from CalcTroll.Core.System import System
from CalcTroll.Core.Task import HasInputs
from CalcTroll.Core.Flags import DEFAULT, MINIMAL
from CalcTroll.Core.ParameterControl import ParameterControl

from AbstractRelaxed import AbstractRelaxed

class Unrelaxed(System, AbstractRelaxed):
    @classmethod
    def className(cls):
        return 'Unrelaxed'

    def __new__(cls, system, name=DEFAULT, **kwargs):
        name = cls.className() + system.className()

        cls = type(name,
                   (cls, system.__class__),
                   system.__dict__)

        return ParameterControl.__new__(cls)

    def __init__(self, system, name=DEFAULT, parameters=DEFAULT):
        self._system = system

        if name is DEFAULT:
            name = system.name()
        self._name = name

        if parameters is DEFAULT:
            parameters = system.parameters()
        self._parameters = parameters

    def systemClass(self):
        return self._system.__class__

    def name(self):
        return self._name

    def system(self):
        return self._system

    def parameters(self):
        return self._parameters

    def tasks(self):
        return []

    def hasInputs(self):
        return False

    def updateStatus(self):
        pass
