# Written by Mads Engelund, 2017, http://espeem.com
from CalcTroll.Core.Task import Task
from CalcTroll.Core.Relaxation import AbstractRelaxed

class RelaxationTask(Task):
    def __init__(
            self,
            item,
            shorthand,
            ):
        parameters = item.parameters()
        identifier = parameters.identifier()

        Task.__init__(
                self,
                path=item.path(),
                shorthand=shorthand,
                identifier=identifier,
                inputs=item.inputs(),
                )
        self.__system = item

    def system(self):
        return self.__system

    def parameters(self):
        return self.system().parameters()

    def method(self):
        return self.system().method()

    def updateInputs(self):
        pass

    def relaxation(self):
        raise NotImplementedError

    def totalEnergy(self):
        return self.method().totalEnergy(self.runDirectory(), filename='relaxation.out')
