# Written by Mads Engelund, 2017, http://espeem.com
from os.path import join
import numpy as np
import string

from CalcTroll.Core.System import System
from CalcTroll.Core.Task import AbstractHasTasks
from CalcTroll.Core.Flags import DEFAULT, MINIMAL
from CalcTroll.Core.ParameterControl import ParameterControl

from Unrelaxed import Unrelaxed
from AbstractRelaxed import AbstractRelaxed

class RelaxedSubSystems(System, AbstractHasTasks, AbstractRelaxed):
    @classmethod
    def className(cls):
        return 'RelaxedSubSystems'

    def path(self):
        return join(self.method().path(), self.system().path())

    def systemClass(self):
        return self.__system.__class__

    def __new__(cls, system, method=DEFAULT, name=DEFAULT, parameters=DEFAULT, **kwargs):
        name = cls.className() + system.className()

        if not parameters is DEFAULT:
            system = system.copy(parameters=parameters)

        parameters = system.parameters()

        if isinstance(system, AbstractRelaxed):
            relaxed_system = system
            cls = relaxed_system.__class__
        else:
            sub_parameters = system.subParameters(parameters)

            assert len(system.subSystems()) == len(sub_parameters)
            it = zip(system.subSystems(), sub_parameters)
            sub_systems = [sub if isinstance(sub, AbstractRelaxed)
                else Relaxed(sub.copy(parameters=p), method=method, parameters=p)
                for sub, p in it]

            relaxed_system = system.setSubSystems(sub_systems)
            cls = type(name,
                       (cls, relaxed_system.__class__),
                        relaxed_system.__dict__)

        instance = System.__new__(cls,)

        instance.__system = relaxed_system

        return instance

    def copy(self):
        return RelaxedSubSystems(
                system=self.system(),
                name=self.name(),
                parameters=self.parameters(),
                )

    def __init__(self,
                 system,
                 method=DEFAULT,
                 name=DEFAULT,
                 parameters=DEFAULT,
                 ):
        if method is DEFAULT:
            from CalcTroll.HOSTS import DEFAULT_METHOD
            method = DEFAULT_METHOD
        self.__method = method

        inputs = self.createInputs()
        AbstractHasTasks.__init__(self, inputs=inputs)

    def tasks(self):
        return []

    def createInputs(self):
        sub_systems = self.system().subSystems()
        inputs = []
        for sub_system in sub_systems:
            inputs.extend(sub_system.tasks())

        return inputs

    def isDone(self):
        return AbstractHasTasks.isDone(self)

    def method(self):
        return self.__method

    def system(self):
        return self.__system

    def update(self):
        pass

class Relaxed(RelaxedSubSystems):
    @classmethod
    def className(cls):
        return 'Relaxed'

    def path(self):
        if isinstance(self.system(), AbstractRelaxed):
            return self.system().path()
        else:
            return join(self.method().path(), self.system().path())

    def __init__(self,
                 system,
                 method=DEFAULT,
                 name=DEFAULT,
                 parameters=DEFAULT,
                 constraint=None,
                 ):
        if method is DEFAULT:
            from CalcTroll.HOSTS import DEFAULT_METHOD
            method = DEFAULT_METHOD

        self.__constraint = constraint

        RelaxedSubSystems.__init__(
                self,
                system=system,
                method=method,
                name=name,
                parameters=parameters,
                )

    def parameters(self):
        parameters = self.system().parameters()
        parameters = parameters.ignoreLongRangeCorrelation()
        parameters = self.system().defaultParameters(parameters)

        return parameters


    def tasks(self):
        return [self.method().makeRelaxation(self)]

    def constraint(self):
        return self.__constraint

    def copy(self, parameters=DEFAULT):
        if parameters is DEFAULT:
            parameters = self.parameters()

        return Relaxed(
                system=self.system(),
                method=self.method(),
                constraint=self.constraint(),
                name=self.name(),
                parameters=parameters,
                )

    def isRelaxed(self):
        return True

    def fullyUnrelaxed(self):
        return self.system().fullyUnrelaxed()

    def createInputs(self):
        system = self.system()
        if isinstance(system, AbstractRelaxed):
            inputs = system.tasks()
        else:
            inputs = RelaxedSubSystems.createInputs(self)

        return inputs

    def mainTask(self):
        return self.tasks()[0]

    def relaxation(self):
        return self.mainTask().relaxation()
