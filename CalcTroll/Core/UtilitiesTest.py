# Written by Mads Engelund, 2017, http://espeem.com
import os
import unittest
from CalcTroll.Core.Test.Case import CalcTrollTestCase

from Utilities import *

class UtilitiesTest(CalcTrollTestCase):
    def testConvolve(self):
        kernel = np.zeros((4, 4, 5))
        kernel[:,:,1] = 0.5
        kernel[:,:,0] =1
        target = np.ones((4, 4, 1))
        result = convolve(target, kernel, center=(0,0,0)).real
        self.assertAlmostEqual(result.sum(), 384.0, 5)
        self.assertAlmostEqual(result[:,:,0].sum(), 256.0, 5)
        self.assertAlmostEqual(result[:,:,1].sum(), 128.0, 5)


if __name__=='__main__':
    unittest.main()

