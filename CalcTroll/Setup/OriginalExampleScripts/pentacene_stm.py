from CalcTrollInterface import *
from matplotlib import pylab as plt

molecule = Acene(5)
voltage = LUMO(plus=0, offset=0.03)
mode = ConstantCurrent(
    state_broadening=0.01,
    spatial_broadening=0.01,
    voltage=voltage,
    current=50,
    )
image = STMImage(molecule, mode=mode)

ax = plt.gca()
plot(image, ax=ax)
plt.savefig('stm_pentacene.png', bbox_inches='tight', pad_inches=0)
