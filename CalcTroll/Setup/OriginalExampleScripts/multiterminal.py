# Written by Mads Engelund, 2017, http://espeem.com

# This example CalcTroll script is meant to show off
# how to set up a complex calculation when you are not
# satisfied with the defaults.
# The plugins used
from ase.units import Ry
from CalcTrollInterface import *
from matplotlib import pylab as plt

# Define the calculational parameters to use.
# This represents a quite poor approximation.
method = Siesta(
        mesh_cutoff=200*Ry,
        basis_set='SZ',
        xc='LDA',
        spin='UNPOLARIZED',
        version=('ts-scf', 492, 662),
        )

# Define a semi-infinite 1D structure - a zigzag
# graphene nanoribbon terminated by hydrogen.
wire = TerminatedZigzagRibbon(n=2)

# And a 2D-surface with a 1D defect - a dimer
# row without hydrogen passivation.
slab = DimerWire('Si')

# Set the geometric parameters.
# The parameters keywords depend on the type of
# system.
parameters = dict(on_parameters=dict(
                    free_layers=6*3,
                    bound_layers=0,
                    electrode_layers=6,
                    ),
                  slab_parameters=dict(
                      vectors=((2, 0), (0, 12)),
                      free_layers=1,
                      bound_layers=1,
                      kpts=(1,1,1),
                      cell_size=45,
                      ),
                  )

# Make the combined system.
system = LiftOff(
        polymer=wire,
        slab=slab,
        parameters=parameters,
        )

# Uncomment to see the final system.
#view(system)

# For speed and simplicity we use the unrelaxed version of
# the physical system. This has to be explicitly enforced
# since the program will otherwise fail-safe to using
# relaxed coordinates.
system = Unrelaxed(system)

# Below shows how you can have more control over the matplotlib
# figures by using the 'ax' argument to 'plot'.
figure = plt.figure(figsize=(4, 4))
ax = figure.gca()
plot(Transmission(system, method=method), ax=ax)
plt.tight_layout(pad=0.1)
plt.savefig('transmission.jpg')

# To simply see the transmission use the 'plot' function with
# default arguments.
#plot(Transmission(system, method=method))
