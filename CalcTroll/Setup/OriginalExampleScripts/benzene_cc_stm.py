from CalcTrollInterface import *
from matplotlib import pylab as plt

molecule = Acene(1)
orbital = HOMO(minus=0)
image = STMOrbitalTopography(
    molecule=molecule,
    orbital=orbital,
    current=50,
    state_broadening=0.01,
    spatial_broadening=0.01,
    )

ax = plt.gca()
plot(image, ax=ax)
plt.savefig('stm.png')
