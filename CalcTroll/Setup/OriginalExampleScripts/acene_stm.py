import os
from CalcTrollInterface import *
from matplotlib import pylab as plt

os.chdir('Acenes')

max_carbon_rings = 6

for carbon_rings in range(max_carbon_rings + 1):
    molecule = Acene(carbon_rings)
    for orbital in [HOMO(), LUMO()]:
        image = STMOrbitalTopography(
            molecule=molecule,
            orbital=orbital,
            current=50,
            state_broadening=0.01,
            spatial_broadening=0.01,
            )

        frame = save(image, molecule.name() + '_' + orbital.name() +'.jpg')

    save(image.system(), molecule.name() + '_relaxed.jpg', frame=frame)
    save(molecule, molecule.name() + '.jpg', frame=frame)
