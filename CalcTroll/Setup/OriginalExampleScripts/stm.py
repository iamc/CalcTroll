# Written by Mads Engelund, 2017, http://espeem.com
from CalcTrollInterface import *
plot(STMImage(Dimer2DBDefect('Ge')))

# This script is meant to be absolutely minimal way of using the scripting
# interface.
# 'plot' uses the predefined plotting settings on STMImage to interpret the
# calculated data.
# If no data is stored STMImage is stored internally as 'must-calculate'.
# Writing 'python stm.py' will return nothing if the data is not ready.
# In stead the user can use 'mysubmit stm.py' which will find the next
# unfinished task needed to calculate STMImage.
# The tasks that are defined behind the scenes are:
#
# 1. Relax Ge(alpha) crystal.
# 2. Relax Ge(001):H surface.
# 3. Relax a dimer depassivation defect on the Ge(001):H surface.
# 4. Calculate the wavefunctions projected onto an iso-electron-density
#    surface.
# 5. Propagate the wavefunctions into vacuum using the Paz/Soler method and
#    evaluate current in the TersoffHamann approximation.
#
# When all these calculations are done the script will show the STMImage.
