from CalcTrollInterface import *
from matplotlib import pylab as plt

atoms = Atoms('O2', positions=[[0, 0.0, 0], [1.5, 0, 0]])
system = Molecule(name='O2', atoms=atoms)

base_method = Siesta(spin='COLLINEAR', basis_set='SZ')
method = PazSoler(base_method)

parameters = system.parameters().copy(padding=30)
image1 = STMLUMOTopography(system, method=method, state_broadening=0.01, parameters=parameters)
image2 = STMHOMOTopography(system, method=method, state_broadening=0.01, parameters=parameters)

fig, axes = plt.subplots(2)

plot(image1, ax=axes[0])
plot(image2, ax=axes[1])

if image1.isDone() and image2.isDone():
    plt.show()

