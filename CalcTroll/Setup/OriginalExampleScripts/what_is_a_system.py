# Written by Mads Engelund, 2017, http://espeem.com
from CalcTrollInterface import *
# CalcTroll basically uses four types of things to organize calculations
# 1. The physical systems under iterest. "System"
# 2. Methods to calculate things. "Method"
# 3. Types of analysis. "Analysis"
# 4. Object that handles how a specific computer handles calculations -
#    which includes sending the calculation to another computer. "Host"

# A system represents your physical system under investigation.
# Take as an example if a surface, here a predefined one.
surface = Diamond001('Si')

# The atoms of the surface can be visualized the following line.
#view(surface.atoms())
# Comment out the line after trying it.

# Try in stead to call 'atoms' with some non-default parameters.
d_parameters = surface.parameters()
parameters = d_parameters.copy(vectors=((2,0),(0, 3)), free_layers=2, bound_layers=0)
#view(surface.atoms(parameters=parameters))

# The surface is defined by three things:
# 1. The underlying crystal
#view(surface.crystal().atoms())

# 2. A set of miller indices
print 'miller_indices = %s'%str(surface.millerIndices())

# 3. The extra atoms needed to recreate the surface reconstruction.
#view(surface.extraAtoms())

# This is what a system basically is:
# a number of sub_systems(here only one : a crystal),
#, some extra parameters (here miller indices and extra_atoms)
# a method, atoms, for generating atoms from this information.
