from CalcTrollInterface import *
from matplotlib import pylab as plt

# Choose predefined molecule.
molecule = StarPhene()

image1 = STMHOMOTopography(
    molecule,
    state_broadening=0.01,
)
image2 = STMLUMOTopography(
    molecule,
    state_broadening=0.01,
)

# Populate the matplotlib figures with data.
plot(image1, ax=plt.subplot(1, 2, 1))
plot(image2, ax=plt.subplot(1, 2, 2))
## Adjust plot.
#plt.subplots_adjust(wspace=None, hspace=None)
#
#plt.show()
# Show plot.
plt.savefig('starphene_stm.png')