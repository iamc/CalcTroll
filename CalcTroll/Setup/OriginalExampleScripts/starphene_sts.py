from CalcTrollInterface import *
from matplotlib import pylab as plt

# Choose predefined molecule.
molecule = StarPhene()

height_mode = ConstantCurrent(
    voltage=HOMOLevel(offset=-0.03),
    current=1,
)
spatial_broadening = 1.45

# Setup different "experiments" on the molecule.
sts1 = ScanningTunnelingSpectroscopy(
    molecule,
    mode=VoltageSweep(
        position=(3,0),
        spatial_broadening=spatial_broadening,
        state_broadening=0.1,
        height=height_mode,
    )
)

sts2 = ScanningTunnelingSpectroscopy(
    molecule,
    mode=VoltageSweep(
        position=(0,0),
        spatial_broadening=spatial_broadening,
        state_broadening=0.1,
        height=height_mode,
    )
)

ax = plt.gca()
# Populate the matplotlib figures with data.
plot(sts1, ax=ax)
plot(sts2, ax=ax)
#plot(image2, ax=plt.subplot(2, 3, 2))
#plot(image3, ax=plt.subplot(2, 3, 5))
#plot(image4, ax=plt.subplot(2, 3, 3))
#plot(image5, ax=plt.subplot(2, 3, 6))
#
## Adjust plot.
#plt.subplots_adjust(wspace=None, hspace=None)
#
#plt.show()
# Show plot.
plt.savefig('starphene_sts.png')