from CalcTrollInterface import *
from matplotlib import pylab as plt

molecule = StarPhene()
voltages = [
    HOMO(offset=-0.02),
    LUMO(offset=+0.02),
]

for i, voltage in enumerate(voltages):
    mode1 = ConstantHeight(
        state_broadening=0.01,
        spatial_broadening=0.01,
        voltage=voltage,
        height=7,
    )

    mode2 = ConstantCurrent(
        state_broadening=0.01,
        spatial_broadening=0.01,
        voltage=voltage,
        current=1,
    )
    modes = [mode1, mode2]

    for j, mode in enumerate(modes):
        image = STMImage(
           system=molecule,
           mode=mode,
           parameters=molecule.parameters().copy(padding=30),
           )

        f_index = i*len(modes) + j + 1
        plot(image, ax=plt.subplot(2, 2, f_index))

plt.savefig('stm_ch.jpg')
