from CalcTrollInterface import *
from matplotlib import pylab as plt

atoms = Atoms('H2', positions=[[0, 0.7, 0], [0, 0, 0]])
system = Molecule(name='H2', atoms=atoms)

image1 = STMLUMOTopography(system, state_broadening=0.01)
image2 = STMHOMOTopography(system, state_broadening=0.01)

fig, axes = plt.subplots(2)

plot(image1, ax=axes[0])
plot(image2, ax=axes[1])

plt.show()

