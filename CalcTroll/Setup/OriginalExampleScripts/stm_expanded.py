# Written by Mads Engelund, 2017, http://espeem.com
from CalcTrollInterface import *

# Define system with non-default calculational parameters.
parameters = dict(free_layers=2,
                  bound_layers=2,
                  vectors=((2, 0), (0, 3)),
                  kpts=(2, 3),
                  )
system = Dimer2DBDefect(symbol='Ge', parameters=parameters)

# Define at which voltage the STM simulation should be calculated.
mode = Topographic(voltage=1.0)
method = PazSoler(base_method=Siesta(xc='KBM', spin='UNPOLARIZED', force_tolerance=0.4))
image = STMImage(system, mode=mode, method=method)

# Plot/calculate the image.
plot(image)
