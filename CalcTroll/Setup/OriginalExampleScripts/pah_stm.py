import os
from ase.units import Ry
from ase.io import write
from CalcTrollInterface import *
from matplotlib import pylab as plt




os.chdir('Order1')

base_method = Siesta(xc="PBE",
                     mesh_cutoff=200 * Ry,
                     spin='UNPOLARIZED',
                     )
molecule = Relaxed(Pentahelicene(), method=base_method)
save(molecule, molecule.name() + '_relaxed.jpg')

method = PazSoler(base_method=base_method)
molecules = [
    #(Acene(0), (0, 0, -1)),
    #(Acene(0), (0, 0,  1)),
    #Acene(1),
    #(Decaphene(), (0, 0, 1)),
    #(Order1Large(), (0, 0, 1)),
    (Pentahelicene(), (0, 0, 1)),
    (Pentahelicene(), (0, 0,-1)),
]

spatial_broadenings = [
    0.01,
    0.5,
]
modes = [
    ConstantCurrent(current=50, image_type=INTEGRATED),
    ConstantHeight(height=5, image_type=INTEGRATED),
    ConstantHeight(height=5, image_type=DIFFERENTIAL),
]

#for molecule, normal in molecules:
#    system = Relaxed(system=molecule, method=base_method)
#    write(molecule.name() + '.xyz', system.atoms())

#for molecule, normal in molecules:
#    for orbital in [
#            HOMO(),
#            HOMO(minus=1),
#            HOMO(minus=2),
#            LUMO(),
#            LUMO(plus=1),
#            LUMO(plus=2),
#            ]:
#        for spatial_broadening in spatial_broadenings:
#            for mode in modes:
#                mode = mode.copy(
#                    state_broadening=0.1,
#                    spatial_broadening=spatial_broadening,
#                    voltage=orbital,
#                    image_type=image_type,
#                    normal=normal,
#                )
#                image = STMImage(
#                    system=molecule,
#                    method=method,
#                    mode=mode,
#                )
#                mod = {(0, 0, 1):'', (0, 0, -1):'_mz'}[normal]
#
#                frame = save(image, '%s%s_%s_%s_%.2f.jpg'%(
#                    molecule.name(),
#                    mod,
#                    orbital.title(),
#                    mode.title(),
#                spatial_broadening),
#                     )
#
#    save(image.system(), molecule.name() + '_relaxed.jpg', frame=frame)
#    save(molecule, molecule.name() + '.jpg', frame=frame)


molecule = Pentahelicene()
modes = [
    ConstantHeight(height=5, image_type=DIFFERENTIAL),
    ConstantHeight(height=5, image_type=INTEGRATED),
]
for orbital in [
    HOMO(),
    HOMO(minus=1),
    HOMO(minus=2),
    LUMO(),
    LUMO(plus=1),
    LUMO(plus=2),
]:
    for spatial_broadening in spatial_broadenings:
        for mode in modes:
            mode = mode.copy(
                state_broadening=0.1,
                spatial_broadening=spatial_broadening,
                voltage=orbital,
                normal=(0, 0, 1),
                )
            image1 = STMImage(
                system=molecule,
                method=method,
                mode=mode,
            )
            mode = mode.copy(
                normal=(0, 0, -1),
            )
            image2 = STMImage(
                system=molecule,
                method=method,
                mode=mode,
            )
            av_image = AverageSTMImage(
                images=[image1, image2],
            )


            frame = save(av_image, '%s_%s_%s_%s_%.2f.jpg'%(
                molecule.name(),
                "av",
                orbital.title(),
                mode.title(),
                spatial_broadening),
                )




