# Written by Mads Engelund, 2017, http://espeem.com
import os
import argparse
import sys
from CalcTroll.Core.Flags import LOCAL, REMOTE
from CalcTroll.Core.Submission.Workflow import WORKFLOW

parser = argparse.ArgumentParser(\
"""Sync the file from the remote server to local machine.
""")
parser.add_argument("filename")
args = parser.parse_args()

# Make absolute path.
path = os.getcwd()
filename = os.path.join(path, args.filename)

host = WORKFLOW.host()
filename = host.changePathType(filename, to=REMOTE, fro=LOCAL)
host.syncFile(filename, to=LOCAL, fro=REMOTE)
