# Written by Mads Engelund, 2017, http://espeem.com
from CalcTroll.Core.Submission.Workflow import WORKFLOW

host = WORKFLOW.host()
parser = host.estimateParser()
kwargs = vars(parser.parse_args())
print host.estimate(**kwargs)
