# Written by Mads Engelund, 2017, http://espeem.com
import numpy as np
from ase.units import eV

#from ParameterGroup import ParameterGroup, ALL
from CalcTroll import API

class BasisSet(API.Parameters):
    def shorthand(self):
        energy_shift = np.around(self.energyShift()*1000.0)
        return '%s_%d'%(self.specification(), energy_shift)

    def __init__(self, specification='DZP', split_norm=0.5, energy_shift=0.150): pass

    def specification(self):
        return self.get('specification', BasisSet)

    def energyShift(self):
        return self.get('energy_shift', BasisSet)


class DZP(BasisSet):
    def __init__(self, split_norm=0.5, energy_shift=0.150):
        BasisSet.__init__(self, specification='DZP', split_norm=split_norm, energy_shift=energy_shift)

class SZP(BasisSet):
    def __init__(self, split_norm=0.5, energy_shift=0.150):
        BasisSet.__init__(self, specification='SZP', split_norm=split_norm, energy_shift=energy_shift)

class SZ(BasisSet):
    def __init__(self, split_norm=0.5, energy_shift=0.150):
        BasisSet.__init__(self, specification='SZ', split_norm=split_norm, energy_shift=energy_shift)

class DZ(BasisSet):
    def __init__(self, split_norm=0.5, energy_shift=0.150):
        BasisSet.__init__(self, specification='DZ', split_norm=split_norm, energy_shift=energy_shift)


