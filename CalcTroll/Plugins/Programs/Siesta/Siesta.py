# Written by Mads Engelund, 2017, http://espeem.com
import numpy as np
import os
import re
from collections import OrderedDict
from os.path import join

from ase import io
from ase.calculators.calculator import all_changes
from ase.calculators.siesta.siesta import Siesta4_1 as ASESIESTA
from ase.constraints import FixAtoms, FixedMode
from ase.units import Ry, eV
from ase import db as ASE_DB

from CalcTroll import API
from CalcTroll.API import DEFAULT, ALL, DONE, NOT_CONVERGED, FAILED, LOCAL
from CalcTroll.API import TAIL
from CalcTroll.Core.ParameterControl.ParameterControl import makeScript
from CalcTroll.Core.Utilities import vectorLength
from CalcTroll.Core.Utilities import unique
from CalcTroll.Plugins.Analysis import AdsorptionEnergy
from CalcTroll.Plugins.Analysis import BandStructure
from CalcTroll.Plugins.Analysis import Charges
from CalcTroll.Plugins.Analysis import Forces
from CalcTroll.Plugins.Analysis import EnergySpectrum
from CalcTroll.Plugins.Analysis import PDOS
from CalcTroll.Plugins.Analysis import TotalEnergy
from CalcTroll.Plugins.Analysis import Transmission
from CalcTrollASE.AtomUtilities import areAtomsEqual
from CalcTrollASE.Atoms import Atoms
from CalcTrollASE.Trajectory import write_traj
from SiestaIO import readPDOS, getFermiEnergy, readTotalEnergy, readEigenvalues
from SiestaIO import readTransmissionFile, extractHirshfeldCharge, readValenceBandEdge, readBandGap

meV = 0.001*eV

FUNCTIONAL_SHORTHANDS = {
        ('LDA', 'PZ'): 'LDA',
        ('GGA', 'PBE'): 'PBE',
        ('VDW', 'KBM'): 'OPTB88',
        }

SPIN_SHORTHANDS = {
        'UNPOLARIZED': 'F',
        'COLLINEAR': 'T',
        }

VALENCE_DICT = {'Ge': 4, 'Si': 4, 'H': 1}

def makeIndicesGroup(indices):
    indices = np.unique(indices)

    groups=[]
    i=0
    while i<len(indices):
        j=i
        together=True
        while together and j+1<len(indices):
            if indices[j+1]-indices[j]!=1 or j+1==len(indices):
                break
            j+=1
        groups.append([indices[i],indices[j]])
        i=j+1

    return groups

def makeElectrodeConform(electrode):
    atoms = electrode.atoms()
    origin = -atoms.get_celldisp()
    atoms.positions += origin
    atoms.set_celldisp(np.zeros(3, float))

    return atoms

def uniqueElectrodes(regions):
    regions = [region for region in regions if region.isElectrode()]
    candidates = np.ones(len(regions), bool)
    matches = [[i] for i in range(len(regions))]
    for i, candidate in enumerate(regions):
        atoms_c = makeElectrodeConform(candidate)

    for i, candidate in enumerate(regions):
        atoms_c = makeElectrodeConform(candidate)
        for j, tester in enumerate(regions[i+1:]):
            atoms_t = makeElectrodeConform(tester)

            if areAtomsEqual(atoms_c, atoms_t):
                candidates[i + j + 1] = False
                matches[i].append(i + j + 1)

    result = []
    for i, region in enumerate(regions):
        if candidates[i]:
            names = [regions[j].name() for j in matches[i]]
            result.append((region, names))

    return result

class SOLUTION_METHOD:
    @classmethod
    def text(cls):
        raise Exception

class DIAGONALIZATION(SOLUTION_METHOD):
    @classmethod
    def text(cls):
        return 'SolutionMethod diagon\n'

class TRANSIESTA_SOLUTION_METHOD(SOLUTION_METHOD):pass

class TRIDIAGONAL(TRANSIESTA_SOLUTION_METHOD):
    @classmethod
    def text(cls):
        return 'SolutionMethod transiesta\n'

class MUMPS(TRANSIESTA_SOLUTION_METHOD):
    @classmethod
    def text(cls):
        return """SolutionMethod transiesta
TS.SolutionMethod MUMPS
TS.MUMPS.BlockingFactor 120
"""

class SiestaCalculator(ASESIESTA):
    def read_eigenvalues(self):
        pass

    def write_input(self, atoms, properties=None, system_changes=None):
        result = ASESIESTA.write_input(self, atoms=atoms, properties=properties, system_changes=system_changes)

        if len(atoms.electrodes())>0:
            runfile = self.label + '.fdf'
            self.writeTS(runfile, atoms)

        return result

    def make_electrodes(self, atoms):
        unique_electrodes = uniqueElectrodes(atoms.electrodes())
        electodes = []
        for electrode, names in unique_electrodes:
            electrode_atoms = makeElectrodeConform(electrode)
            s_dir = electrode.semiInfiniteDirection()
            parameters = self.parameters.copy()
            label = '_'.join(names)
            label = 'EL_%s' % label
            parameters['label'] = label

            kpts = np.array(parameters['kpts'] )
            l = vectorLength(electrode.cell()[s_dir])
            k = int(np.ceil(400/l))
            kpts[s_dir] = k
            parameters['kpts'] = tuple(kpts)

            electrode_calculator = self.__class__(**parameters)
            electrode_atoms.set_calculator(electrode_calculator)
            electodes.append(electrode_atoms)

        return electodes

    def calculate(self,
                  atoms=None,
                  properties=['energy'],
                  system_changes=all_changes
                  ):
        #self.write_input(atoms=atoms, properties=properties, system_changes=system_changes)

        electrodes = self.make_electrodes(atoms=atoms)

        for electrode in electrodes:
            calculator = electrode.get_calculator()
            ASESIESTA.calculate(calculator, atoms=electrode, properties=['energy'])

        ASESIESTA.calculate(self, atoms=atoms, properties=properties, system_changes=system_changes)

    def writeTS(self, path, atoms):
        with open(path, 'a') as f:
            f.write(self.writeSolutionMethod(atoms))
            f.write(self.writeChemicalPotentials(atoms))
            f.write(self.writeElectrodes(atoms))
            f.write(self.writeBuffer(atoms))
            f.write(self.writeContours(atoms))

    @classmethod
    def writeSolutionMethod(cls, atoms):
        if atoms.hasOpenBoundaries():
            return TRIDIAGONAL.text()
        else:
            return DIAGONALIZATION.text()

    @classmethod
    def writeChemicalPotentials(cls, atoms):
        electrodes = atoms.electrodes()
        potentials = np.array([el.voltage() for el in electrodes])

        script = """#Define all the different chemical potentials
"""

        script += "%block TS.ChemPots\n"
        for electrode in electrodes:
            script += 'U_%s\n'%electrode.name()
        script += "%endblock TS.ChemPots\n"
        for electrode in electrodes:
            script += '%block TS.ChemPot.U_' + electrode.name() +'\n'
            script += """mu %.8f eV""" % electrode.voltage() + """
  contour.eq
    begin
      c-%s
      t-%s
    end""" % (electrode.name(), electrode.name()) + """
%endblock

"""
        return script

    def writeElectrodes(self, atoms):
        electrodes = atoms.electrodes()
        names = [electrode.name() for electrode in electrodes]
        s_names = ['EL_' + str(name) for name in names]

        script = """%block TS.Elecs
""" + '\n'.join(s_names) + """
%endblock TS.Elecs

"""
        unique_electrodes = uniqueElectrodes(atoms.electrodes())
        unique_electrodes = [entry[1] for entry in unique_electrodes]

        for names in unique_electrodes:
            for name in names:
                electrode = atoms.region(name)
                sd = electrode.semiInfiniteDirection()
                pbc = electrode.pbc()[sd]
                semi_input = {(False, True): '+', (True, False):'-'}[pbc] + 'a' + str(sd+1)
                start = electrode.start() + 1
                name = electrode.name()
                TSHS_name = '_'.join(names)
                TSHS_name = 'EL_%s.TSHS'%TSHS_name
                s_name = 'EL_%s'%name
                cp_name = 'U_%s'%name
                script += """%block TS.Elec.""" + s_name + """
  TSHS %s
  chem-pot %s
  semi-inf-dir %s
  elec-pos begin %d"""%(TSHS_name, cp_name, semi_input, start) + """
%endblock TS.Elec.""" + s_name + """

"""

        return script

    @classmethod
    def writeContours(cls, atoms):
        script = """
TS.Contours.nEq.Eta   0.00001 eV"""
        script += """
# Define contours for contour integrals.
TS.Contours.Eq.Pole.N 10
"""

        for electrode in atoms.electrodes():
            script += """%block""" + """ TS.Contour.c-%s
  part circle
   from  -26.00000 eV + %.5f eV to -10. kT + %.5f eV
    points 70
     method g-legendre""" % (electrode.name(), electrode.voltage(), electrode.voltage()) + """
%endblock""" + """ TS.Contour.c-%s
""" % electrode.name()
            script += '%' + """block TS.Contour.t-%s
  part tail
   from prev to inf
    points 10
     method g-fermi""" % electrode.name() + """
%endblock""" + """ TS.Contour.t-%s
""" % electrode.name()

        potentials = np.array([el.voltage() for el in atoms.electrodes()])
        vdiff = potentials.max() - potentials.min()

        if vdiff > 0.0:
            script += """
%block TS.Contours.Bias.Window
  neq
%endblock TS.Contours.Bias.Window
%block TS.Contour.Bias.Window.neq
  part line
   from -|V|/2 to |V|/2
    delta 0.01 eV
     method simpson-mix
%endblock TS.Contour.Bias.Window.neq

%block TS.Contours.Bias.Tail
  neq-tail
%endblock TS.Contours.Bias.tail
%block TS.Contour.Bias.Tail.neq-tail
  part tail
   from 0. kT to 12. kT
    delta 0.01 eV
     method simpson-mix
%endblock TS.Contour.Bias.Tail.neq-tail

"""
        return script

    @classmethod
    def writeBuffer(cls, atoms):
        buffers = atoms.buffers()
        if len(buffers) == 0:
            return ''

        buffer_indices = []
        for buf in buffers:
            buffer_indices.extend(buf.indices())
        buffer_indices = np.array(buffer_indices) + 1

        groups = makeIndicesGroup(buffer_indices)
        if len(groups) > 0:
            script = """%block TS.Atoms.Buffer
"""
            for group in groups:
                script += 'atom from %d to %d\n'%(group[0],group[1])


            script += """%endblock TS.Atoms.Buffer
"""
        else:
            script = ''
        return script

class Specie(API.Parameters):
    def __init__(
            self,
            symbol,
            basis_set='DZP',
            pseudopotential=None,
            tag=None,
            ghost=False,
            ):
        pass


class Siesta(API.Program):
    def __init__(self,
            mesh_cutoff=200*Ry,
            energy_shift=100*meV,
            basis_set='DZP',
            xc=('LDA', 'PZ'),
            force_tolerance=0.02,
            spin='COLLINEAR',
            stress_tolerance=0.0002,
            dm_tolerance=0.0001,
            version=None,
            species=tuple(),
            fdf_arguments=(
                ('MinSCFIterations', 1),
                ('MaxSCFIterations', 500),
                ('DM.MixingWeight', 0.02),
                ('DM.KickMixingWeight', 0.1),
                ('DM.NumberPulay', 5),
                ('DM.NumberKick', 50),
                ('DM.MixSCF1', True),
                ('DM.UseSaveDM', True),
                )
            ):
        kwargs = locals()
        kwargs['fdf_arguments'] = dict(fdf_arguments)
        kwargs.pop('self')
        kwargs.pop('force_tolerance')
        kwargs.pop('stress_tolerance')
        kwargs.pop('dm_tolerance')
        kwargs.pop('version')
        kwargs['fdf_arguments']['DM.Tolerance'] = dm_tolerance
        kwargs['fdf_arguments']['WriteMullikenPop'] = 1

        self.__basis_set = basis_set
        self.__version = version
        self.__ase_calculator = SiestaCalculator(**kwargs)

    def _calculator(self):
        return self.__ase_calculator

    def aseScript(self, kpts):
        dictionary = self.__ase_calculator.parameters.copy()
        dictionary.pop('pseudo_qualifier')
        dictionary.pop('pseudo_path')
        dictionary.pop('ignore_bad_restart_file')
        dictionary.pop('label')
        dictionary.pop('atoms')
        dictionary['kpts'] = kpts
        string = makeScript('Siesta', dictionary, detail_level=API.ALL) + '\n'

        return string

    @classmethod
    def charges(cls, path):
        return extractMullikenCharge(join(path, 'siesta.out'))

    @classmethod
    def totalEnergy(cls, path, filename='siesta.out'):
        filename = join(path, filename)

        return readTotalEnergy(filename)

    @classmethod
    def getFermiEnergy(self, directory, label='siesta'):
        return getFermiEnergy(join(directory, label+'.EIG'))

    def executable(self):
        return 'siesta'

    def version(self):
        return self.__version

    def basisSet(self, full_description=False):
        if full_description:
            return {'DZP': 'double-zeta polarized(DZP)'}[self.__basis_set]
        else:
            return self.__basis_set

    def path(self):
        functional = FUNCTIONAL_SHORTHANDS[self.__ase_calculator['xc']]
        spin = SPIN_SHORTHANDS[self.__ase_calculator['spin']]
        basis_set = self.__ase_calculator['basis_set']
        energy_shift = self.__ase_calculator['energy_shift']*1000
        mesh = np.around(self.__ase_calculator['mesh_cutoff']/Ry*eV)

        name = "ASE_%s_%s_%s_%d_%d"%(functional, basis_set, spin, mesh, energy_shift)

        return name

    def pseudoPath(self):
        return join(
                os.path.dirname(__file__),
                'pseudopotentials',
                'UOR',
                )

    def makeRelaxation(self, item):
        if isinstance(item.system(), API.System):
            return ASESiestaRelaxation(
                item=item,
                )
        elif isinstance(item.system(), API.Collection):
            return ASESiestaCollectionRelaxation(
                item=item,
                )

    def makeTasks(
            self,
            items,
            ):
        tasks = []
        general_analysis = False
        method = items[0].method().copy()
        parameters = items[0].parameters()

        for item in items:
            if isinstance(item, Transmission):
                task = TBTransRun(
                        transmission=item,
                        )
                tasks.append(task)
            elif isinstance(item, AdsorptionEnergy):
                task1 = FragmentOneShot(
                        item=item,
                        shorthand='GhostSlab',
                        part=0,
                        ghosted=True,
                        )
                task2 = FragmentOneShot(
                        item=item,
                        shorthand='GhostMolecule',
                        part=1,
                        ghosted=True,
                        )
                task3 = FragmentOneShot(
                        item=item,
                        shorthand='RemoveSlab',
                        part=0,
                        ghosted=False,
                        )
                task4 = FragmentOneShot(
                        item=item,
                        shorthand='RemoveMolecule',
                        part=1,
                        ghosted=False,
                        )
                tasks.extend([task1, task2, task3, task4])
            else:
                general_analysis = True
                method.addAnalysis(item)

        if general_analysis:
            task = ASESiestaAnalysis(
                    items=items,
                    method=method,
                    parameters=parameters,
                    )
            tasks.append(task)

        return tasks

    def addAnalysis(self, analysis):
        fdf_arguments = self.__ase_calculator['fdf_arguments']
        fdf_arguments['TimeReversalSymmetryForKpoints'] = False
        if isinstance(analysis, Charges):
            fdf_arguments['WriteMullikenPop'] = 1
            fdf_arguments['WriteVoronoiPop'] = True
            fdf_arguments['WriteHirshfeldPop'] = True
        elif isinstance(analysis, Forces):
            fdf_arguments['WriteForces'] = True
        elif isinstance(analysis, PDOS):
            fdf_arguments['ProjectedDensityOfStates'] = [(-20.0, 0.0, 0.100, 1000, 'eV')]
        elif isinstance(analysis, BandStructure):
            k_path = analysis.kPath()
            point, name = k_path[0]
            ka, kb, kc = point
            path = [(1, ka, kb, kc, name)]
            for point, name in k_path[1:]:
                 ka, kb, kc = point
                 path.append((100, ka, kb, kc, name))

            fdf_arguments['BandLinesScale']='ReciprocalLatticeVectors'
            fdf_arguments['BandLines'] = path

        else:
            pass

    def writeWaveFunctions(self, band_range=(1, 1)):
        parameters = self.__ase_calculator.parameters
        fdf_arguments = parameters['fdf_arguments']
        fdf_arguments['WFS.Write.For.Bands'] = True
        fdf_arguments['WFS.Band.Min'] = band_range[0]
        fdf_arguments['WFS.Band.Max'] = band_range[1]
        fdf_arguments['WriteDenchar'] = True
        fdf_arguments['COOP.Write'] = True
        fdf_arguments['TimeReversalSymmetryForKpoints'] = False
        self.__ase_calculator.set(**parameters)

    def writeDensity(self):
        parameters = self.__ase_calculator.parameters
        fdf_arguments = parameters['fdf_arguments']
        fdf_arguments['SaveRho'] = True

        self.__ase_calculator.set(**parameters)

    def readDensity(self, label='siesta', d_type='nc'):
        if d_type == 'nc':
            dirname = os.path.dirname(label)
            filename = join(dirname, 'Rho.grid.nc')

            return readNCDensity(filename)
        else:
            raise ValueError

    @classmethod
    def runFileEnding(cls):
        return 'py'

    @classmethod
    def outFileEnding(cls):
        return 'out'

    def neededFiles(self):
        return ['*.fdf', '*.py', '*.traj']

    def resultFiles(self):
        return ['*.XV', '*.traj', '*.out', '*.EIG']

    def checkIfFinished(self, filename, runfile=None):
        if not os.path.exists(filename):
            return API.NOT_STARTED

        good = False
        with open(filename, 'r') as f:
            for line in f:
                good = 'BFGS' in line
        if good:
            final = float(line.split(' ')[-1])
            if final <= self['force_tolerance']:
                return DONE
            else:
                return NOT_CONVERGED
        else:
            return FAILED

    def relaxedAtoms(self, runpath, pbc=None):
        filename = join(runpath, 'relax.traj')
        if not os.path.exists(filename):
            raise Exception("File %s missing" % filename)

        atoms = io.read(filename + '@-1')
        atoms = Atoms(atoms)

        return atoms

    def writeCharges(self):
        fdf_arguments = self.__ase_calculator.parameters['fdf_arguments']
        fdf_arguments['WriteMullikenPop'] = 1
        fdf_arguments['WriteVoronoiPop'] = True
        fdf_arguments['WriteHirshfeldPop'] = True
        self.__ase_calculator.set(fdf_arguments=fdf_arguments)

    def writeRelaxation(self,
              runfile,
              atoms,
              kpts,
              relax_cell,
              save_behaviour,
              ):
        atoms = atoms.copy()

        net_charge = 0
        for doping in atoms.dopings():
            net_charge += doping.charge()

        # Remove electrodes to avoid transiesta relaxation (for now).
        atoms.purgeTags()

        c_string = 'calculator = '
        dictionary = self.__ase_calculator.parameters.copy()
        dictionary['fdf_arguments']['NetCharge'] = net_charge
        dictionary.pop('pseudo_qualifier')
        dictionary.pop('pseudo_path')
        dictionary.pop('ignore_bad_restart_file')
        dictionary.pop('label')
        dictionary.pop('atoms')
        dictionary['kpts'] = kpts
        c_string += makeScript('Siesta', dictionary, detail_level=API.ALL) + '\n'
        path = os.path.dirname(runfile)
        filename = join(path, 'input.traj')

        if relax_cell:
            eff_force_tolerance = np.min([self['force_tolerance'], self['stress_tolerance']])
        else:
            eff_force_tolerance = self['force_tolerance']

        write_traj(filename, atoms)

        script =  """from CalcTrollASE.Trajectory import myread_traj, write_traj, Trajectory
from ase.constraints import UnitCellFilter
from ase.optimize import FIRE, BFGS
from CalcTroll.Plugins.Programs.Siesta.Siesta import SiestaCalculator as Siesta
from ase.calculators.siesta.parameters import Specie
from CalcTroll.Plugins.Programs.Siesta.SiestaIO import convertXVtoASE
"""
        script += c_string
        script += """
atoms = myread_traj('input.traj', -1)
try:
    restart_atoms = myread_traj('relax.traj', -1)
except:
    print 'initializing from input.traj...'
else:
    print 'restarting from previous geometry...'
    atoms.positions = restart_atoms.positions
    atoms.cell = restart_atoms.cell
atoms.set_calculator(calculator)
"""
        if relax_cell:
            pbc = tuple(atoms.get_pbc())
            if pbc == (False, False, True):
                mask = (False, False, True, False, False, False)
            elif pbc == (True, True, True):
                mask = None
            script += """
relax = UnitCellFilter(atoms, mask=%s)
""" % str(mask)

        else:
            script += "relax = atoms\n"

        script += """
opt = BFGS(relax)
traj = Trajectory('relax.traj', 'w', atoms)
opt.attach(traj)
opt.run(fmax=%.5f)
"""%eff_force_tolerance

        with open(runfile, 'w') as f:
            f.write(script)

    def writeNEB(
            self,
            runfile,
            neb,
            kpts,
            ):
        dictionary = self.__ase_calculator.parameters.copy()
        dictionary.pop('pseudo_qualifier')
        dictionary.pop('pseudo_path')
        dictionary.pop('ignore_bad_restart_file')
        dictionary.pop('label')
        dictionary.pop('atoms')
        dictionary['kpts'] = kpts
        c_string = 'calculator = '
        c_string += makeScript('Siesta', dictionary, detail_level=ALL) + '\n'
        path = os.path.dirname(runfile)

        n_images = len(neb.images)
        filename = join(path, 'neb.traj')
        write_traj(filename, neb.images)
        script =  """from CalcTrollASE.Trajectory import myread_traj, write_traj, Trajectory
from ase import io
from ase.neb import NEB
from ase.optimize import FIRE
from CalcTroll.Plugins.Programs.Siesta.Siesta import SiestaCalculator as Siesta
"""
        script += """calculator = %s""" % c_string + """
kwargs = calculator.parameters.copy()
images = []
for i in range(%d):""" % n_images + """
    kwargs['label'] = 'siesta_%d' % i
    calculator = Siesta(**kwargs)
    image = myread_traj('neb.traj', i)
    image.set_calculator(calculator)
    image.get_total_energy()
    images.append(image)
""" + """
k=0.5
neb = NEB(images, k=k, climb=False)
qn = FIRE(neb, trajectory='neb.traj')
qn.run(fmax=%.3f)
""" % self['force_tolerance']


        with open(runfile, 'w') as f:
            f.write(script)

    def calculatorScript(
            self,
            atoms,
            kpts,
            min_iterations=True,
            save_behaviour=None,
            ):
        net_charge = 0
        for doping in atoms.dopings():
            net_charge += doping.charge()
        c_string = 'calculator = '
        dictionary = self.__ase_calculator.parameters.copy()
        dictionary['fdf_arguments']['NetCharge'] = net_charge
        dictionary.pop('pseudo_qualifier')
        dictionary.pop('pseudo_path')
        dictionary.pop('ignore_bad_restart_file')
        dictionary.pop('label')
        dictionary.pop('atoms')
        if not min_iterations:
            dictionary['fdf_arguments']['MinSCFIterations'] = 1
        dictionary['kpts'] = kpts
        c_string += makeScript('Siesta', dictionary, detail_level=API.ALL) + '\n'

        return c_string

    def writeOneShot(self,
              runfile,
              atoms,
              kpts,
              save_behaviour=None,
              min_iterations=True,
              ):
        c_string = self.calculatorScript(
                atoms,
                kpts=kpts,
                min_iterations=min_iterations,
                save_behaviour=save_behaviour,
                )
        path = os.path.dirname(runfile)
        filename = join(path, 'analysis.traj')
        write_traj(filename, atoms)

        script =  """from CalcTrollASE.Trajectory import myread_traj, write_traj, Trajectory
from ase.constraints import UnitCellFilter
from CalcTroll.Plugins.Programs.Siesta.Siesta import SiestaCalculator as Siesta
from ase.calculators.siesta.parameters import Specie
from CalcTroll.Plugins.Programs.Siesta.SiestaIO import convertXVtoASE
"""
        script += c_string
        script += """
atoms = myread_traj('analysis.traj', -1)
atoms.set_calculator(calculator)
"""

        script += """
atoms.get_total_energy()
"""

        with open(runfile, 'w') as f:
            f.write(script)

    def evaluateBandstructure(self, k_path):
        point, name = k_path[0]
        ka, kb, kc = point
        path = [(1, ka, kb, kc, name)]
        for point, name in k_path[1:]:
             ka, kb, kc = point
             path.append((100, ka, kb, kc, name))

        dictionary = self.__ase_calculator.parameters.copy()
        dictionary['fdf_arguments']['BandLinesScale']='ReciprocalLatticeVectors'
        dictionary['fdf_arguments']['BandLines'] = path
        self.__ase_calculator.set(**dictionary)

    def constraintsString(self, constraints):
        string = 'constraints = []\n'
        for constraint in constraints:
            if isinstance(constraint, FixAtoms):
                import_string="from ase.constraints import FixAtoms\n"
                c_string = """FixAtoms(mask=%s)"""%np.array(constraint.index, int).tolist()
            elif isinstance(constraint, FixedMode):
                import_string="from ase.constraints import FixedMode\n"
                c_string = repr(constraint)
            else:
                eeeeeeeeeee
            string += import_string
            string += 'constraints.append(' + c_string + ')\n'

        return string

class ASESiestaElectronicStructure(API.Task):
    def __init__(
            self,
            system,
            method,
            parameters=DEFAULT,
            path=DEFAULT,
            shorthand='ES',
            identifier=DEFAULT,
            host=DEFAULT,
            inputs=tuple(),
            ):
        self.__system = system
        self.__method = method
        if parameters is DEFAULT:
            parameters=system.parameters()

        if path is DEFAULT:
            path = join(method.path(), system.path())

        self.__parameters = parameters
        inputs = unique(list(inputs) + system.tasks())

        API.Task.__init__(
                self,
                path=path,
                shorthand=shorthand,
                identifier=identifier,
                host=host,
                inputs=inputs,
                )

    def parameters(self):
        return self.__parameters

    def executable(self):
        return {False:'siesta', True:'transiesta'}[self.hasOpenBoundaries()]

    def neededFiles(self):
        return ['*.traj', '*.fdf', '*.py', '*.psf']

    def resultFiles(self):
        return ['*.out', '*.XV', '*.EIG']

    def hasOpenBoundaries(self):
        return self.inputAtoms().hasOpenBoundaries()

    def system(self):
        return self.__system

    def method(self):
        return self.__method

    def kpts(self, atoms):
        kpts = np.ones(3, int)
        n_kpts = self.parameters().kpts()
        kpts[:len(n_kpts)] = n_kpts

        return kpts

    def makeCommands(self, form):
        return [form%(self.runFile(path=TAIL), 'TMP.out'),
                'mv TMP.out %s'%self.outFile(path=TAIL)
                ]

    def fermiEnergy(self):
        return self.method().getFermiEnergy(self.runDirectory(), label='analysis.siesta')

    def totalEnergy(self):
        return self.method().totalEnergy(self.runDirectory())

    def saveBehaviour(self):
        return {'hamiltonian': False, 'density_matrix':False}

    def inputAtoms(self):
        return self.system().atoms(parameters=self.parameters())

    def atoms(self, parameters=DEFAULT, constrained=True):
        if parameters is DEFAULT:
            parameters = self.parameters()

        return self.system().atoms(parameters=parameters, constrained=constrained)

    def write(self):
        atoms = self.inputAtoms()
        kpts = self.kpts(atoms)

        self.method().writeOneShot(
                runfile=self.runFile(),
                atoms=atoms,
                kpts=kpts,
                save_behaviour=self.saveBehaviour(),
                )

    def _getLocalStatus(self):
        outfile = self.outFile(path=API.LOCAL)
        if not os.path.exists(outfile):
            return API.NOT_STARTED

        with open(outfile, 'r') as f:
             output = f.read()

        if 'RuntimeError:' in output:
            return API.FAILED
        else:
            return API.DONE


class FragmentOneShot(ASESiestaElectronicStructure):
    def __init__(
            self,
            item,
            shorthand='GH',
            part=0,
            ghosted=True,
            path=DEFAULT,
            ):
        self.__part = part
        self.__ghosted = ghosted
        if path is DEFAULT:
            path = item.path()

        ASESiestaElectronicStructure.__init__(
            self,
            system=item.system(),
            method=item.method(),
            path=path,
            shorthand=shorthand,
            identifier=None,
            )

    def write(self):
        atoms = self.inputAtoms()
        symbols = np.array(atoms.get_chemical_symbols())
        if self.__part == 0:
            keep_indices = self.system().moleculeIndices(atoms)
        else:
            keep_indices = self.system().slabIndices(atoms)

        if self.__ghosted:
            tags = np.ones(len(atoms), bool)
            tags[keep_indices] = 0
            symbols = np.unique(symbols[tags!=0])
            atoms.set_tags(tags)
            species = [Specie(symbol=symbol, tag=1, ghost=True) for symbol in symbols]
        else:
            atoms = atoms[keep_indices]
            species = []

        kpts = self.kpts(atoms)

        method = self.method().copy(species=species)
        method.writeOneShot(
                runfile=self.runFile(),
                atoms=atoms,
                kpts=kpts,
                save_behaviour=self.saveBehaviour(),
                )

    def resultFiles(self):
        return ['*.out', '*.XV', '*.EIG']


class OpenSystemOneShot(ASESiestaElectronicStructure):
    def __init__(self,
            system,
            method,
            path=DEFAULT,
            shorthand=DEFAULT,
            identifier=DEFAULT,
            host=DEFAULT,
            ):
        inputs = system.tasks()
        ASESiestaElectronicStructure.__init__(self,
                system=system,
                method=method,
                path=path,
                shorthand=shorthand,
                identifier=identifier,
                host=host,
                inputs=inputs,
                )

    def saveBehaviour(self):
        return {'hamiltonian': True, 'density_matrix':False}

    def resultFiles(self):
        return ['*.out', '*.XV', '*.EIG', '*.TSHS', '*.fdf']


class ASESiestaAnalysis(ASESiestaElectronicStructure):
    def __init__(
            self,
            items,
            method,
            parameters,
            path=DEFAULT,
            shorthand=DEFAULT,
            identifier=DEFAULT,
            host=DEFAULT,
            ):
        self.__items = items
        self.__result_files = None
        system = items[0].system()
        for item in items:
            assert item.system() == system

        if path is DEFAULT:
            if parameters != system.parameters():
                path = system.mainTask().runDirectory()
                if shorthand is DEFAULT:
                    shorthand = 'Analysis'
            else:
                path = system.path()
                shorthand = system.mainTask().shorthand()

        if identifier is DEFAULT:
            identifier = parameters.identifier()

        if shorthand is DEFAULT:
            if is_relaxed:
                shorthand = system.mainTask().shorthand()
            else:
                shorthand = 'Analysis'

        if isinstance(system, API.HasInputs):
            inputs = system.tasks()
        else:
            inputs = []

        ASESiestaElectronicStructure.__init__(
                self,
                system=system,
                method=method,
                parameters=parameters,
                path=path,
                shorthand=shorthand,
                identifier=identifier,
                host=host,
                inputs=inputs,
                )

    def explain(self):
        vac = self.system().parameters()['padding'] * 2
        return API.Explanation("Final evaluation was performed with %d Ang of vacuum." % vac)

    def estimate(self):
        NE = self.system().inputAtoms().number_of_valence_electrons()

        return float(NE**3)

    def makeCommands(self, form):
        return [
                form%(self.runFile(path=API.TAIL), 'TMP.out'),
                'mv TMP.out %s'%self.outFile(path=API.TAIL),
                'mv siesta.out analysis.siesta.out',
                'mv siesta.EIG analysis.siesta.EIG',
                ]

    def write(self):
        ASESiestaElectronicStructure.write(self)

    def siestaOutFile(self, path=LOCAL):
        return join(self.runDirectory(path=path), 'analysis.siesta.out')

    def resultFiles(self):
        if not self.__result_files is None:
            return self.__result_files

        files = []
        for item in self.__items:
            files.extend(self.resultFilesForItem(item))

        files.extend(self.method().resultFiles())
        self.__result_files = list(np.unique(files))

        return self.__result_files

    def resultFilesForItem(self, item):
        files = []
        if isinstance(item, BandStructure):
            files.append('siesta.bands')
        elif isinstance(item, PDOS):
            files.append('siesta.PDOS')

        return files

    def readData(self, item, path=LOCAL, **kwargs):
        directory = self.runDirectory(path=path)
        if isinstance(item, Charges):
            return extractHirshfeldCharge(self.siestaOutFile(path=path))

        elif isinstance(item, BandStructure):
            filename = join(directory, 'siesta.bands')
            return readBandGap(filename)

        elif isinstance(item, PDOS):
            filename = join(directory, 'siesta.PDOS')
            return readPDOS(filename=filename, **kwargs)

        elif isinstance(item, EnergySpectrum):
            filename = join(directory, 'analysis.siesta')
            return readEigenvalues(filename)

        elif isinstance(item, TotalEnergy):
            filename = self.siestaOutFile(path=path)
            return readTotalEnergy(filename=filename)

        raise ValueError

    def runFile(self, path=LOCAL):
        filename = 'ANALYSIS.' + self.method().runFileEnding()

        return join(self.runDirectory(path=path), filename)

    def outFile(self, path=LOCAL):
        filename = 'ANALYSIS.' + self.method().outFileEnding()

        return join(self.runDirectory(path=path), filename)

    def _getLocalStatus(self):
        status = ASESiestaElectronicStructure._getLocalStatus(self)
        if status is not DONE:
            return status

        for item in self.__items:
            for f in self.resultFilesForItem(item):
                f = join(self.runDirectory(path=LOCAL), f)
                if not os.path.exists(f):
                    return API.NOT_STARTED

        return DONE

class ASESiestaCollectionRelaxation(API.RelaxationTask):
    def __init__(
            self,
            item,
            shorthand='CG',
            ):
        API.RelaxationTask.__init__(
                self,
                item,
                shorthand=shorthand,
                )

    def explain(self):
        basis_set = self.method().basisSet(full_description=True)
        energy_shift = self.method()['energy_shift']/meV
        xc = self.method()['xc']
        spin = self.method()['spin']
        spin = {'UNPOLARIZED': '', 'COLLINEAR': 'spin-polarized ', 'FULL': 'non-collinear spin '}[spin]
        citations =  [
                 "J. M. Soler, E. Artacho, J. D. Gale, A. Garcia, J. Junquera, P. Ordejon and D. Sanchez-Portal, J. Phys.:Condens. Matter, 2002, 14, 2745" ]

        if xc == 'PBE':
            xc = 'Perdew-Burke-Ernzerhof(PBE)'
            citations.append( "J. Perdew, K. Burke and M. Erzerhof, Phys. Rev. Lett., 1996, 77, 3865")

        force_tolerance = self.method()['force_tolerance']
        mesh_cutoff = self.method()['mesh_cutoff']/Ry
        t = (spin, basis_set, energy_shift, xc, mesh_cutoff, force_tolerance)
        return API.Explanation(
                "Geometric relaxations were performed using %sdensity-functional theory(DFT) using the SIESTA code.[*] We used a %s basis set with orbital radii defined using a %d meV energy shift. The %s exchange-correlation potential,[*] and a real-space grid equivalent to a %d Ry plane-wave cutoff. Forces were relaxed until forces were smaller than %.3f eV/Ang." % t,
                citations,
                )

    def estimate(self):
        NE = self.system().inputAtoms().number_of_valence_electrons()
        relaxation_steps = 20

        return float(NE**3)*relaxation_steps

    def runDirectory(self, path=LOCAL):
        base_run = API.RelaxationTask.runDirectory(self, path=path)

        if self.system().constraint() is None:
            path = base_run
        else:
            path =join(base_run, 'CCG_%s' % self.system().constraint().identifier())

        return path

    def executable(self):
        return 'siesta'

    def makeCommands(self, form):
        commands = []
        commands.append(form%(self.runFile(path=API.TAIL), 'TMP.out'))
        commands.append('mv TMP.out %s'%self.outFile(path=API.TAIL))
        commands.append('mv siesta.out %s' % self.siestaOutFile(path=API.TAIL))

        return commands

    def inputAtoms(self):
        atoms = self.system().system().atoms()

        return atoms

    def relaxation(self):
        if not self.isDone():
            print self.outFile()
            print self.path()
            print self._getLocalStatus()
            print self.system()
            raise Exception('%s is not done'%self.system().name())

        pbc = self.system().pbc()
        atoms = self.method().relaxedAtoms(self.runDirectory(), pbc)

        return atoms

    def hasOpenBoundaries(self):
        return self.inputAtoms().hasOpenBoundaries()

    def _getLocalStatus(self):
        outfile = self.outFile(path=API.LOCAL)
        if not os.path.exists(outfile):
            return API.NOT_STARTED

        with open(outfile, 'r') as f:
             output = f.read()

        if 'RuntimeError:' in output:
            return API.FAILED
        else:
            return API.DONE


    def write(self):
        runfile = self.runFile()

        collection = self.system()
        kpts = collection.parameters().kpts()
        method = self.method()

        for i in range(len(collection)):
            atoms = collection.entry(i).atoms()
            atoms.purgeTags()
            inputfile = join(self.runDirectory(), 'input%d.traj' % i)
            write_traj(inputfile, atoms)

        c_string = 'calculator = ' + method.aseScript(kpts=kpts)
        path = os.path.dirname(runfile)
        eff_force_tolerance = method['force_tolerance']

        script =  """import os
from CalcTrollASE.Trajectory import myread_traj, write_traj, Trajectory
from ase.constraints import UnitCellFilter
from ase.optimize import FIRE, BFGS
from ase import db
from CalcTroll.Plugins.Programs.Siesta.Siesta import SiestaCalculator as Siesta
from ase.calculators.siesta.parameters import Specie
from CalcTroll.Plugins.Programs.Siesta.SiestaIO import convertXVtoASE
"""
        script += c_string
        script += """
for i in range(%d):""" % len(collection) + """
    relax_traj = 'relax%d.traj' % i
    if os.path.exists(relax_traj):
        continue
    atoms = myread_traj('input%d.traj' % i, -1)
    try:
        restart_atoms = myread_traj('relax.traj', -1)
    except:
        print 'initializing from input.traj...'
    else:
        print 'restarting from previous geometry...'
        atoms.positions = restart_atoms.positions
        atoms.cell = restart_atoms.cell
    atoms.set_calculator(calculator)
    opt = BFGS(atoms)
    traj = Trajectory('relax%d.traj' % i""" + """, 'w', atoms)
    opt.attach(traj)
    opt.run(fmax=%.5f)
"""%eff_force_tolerance

        with open(runfile, 'w') as f:
            f.write(script)

    def saveBehaviour(self):
        return {'hamiltonian': False, 'density_matrix':False}

    def siestaOutFile(self, path=API.TAIL):
        return join(self.runDirectory(path=path), 'relaxation.siesta.out')

    def neededFiles(self):
        return self.method().neededFiles()

    def resultFiles(self):
        return ['*.out', '*.XV', '*.EIG', '*.traj', '*.fdf']

    def _getLocalStatus(self):
        outfile = self.outFile(path=API.LOCAL)
        if not os.path.exists(outfile):
            return API.NOT_STARTED

        with open(outfile, 'r') as f:
             output = f.readlines()

        last_tolerance = 1000
        for line in output:
            line = line.split()
            if len(line) > 0:
                try:
                    last_tolerance = float(line[-1])
                except ValueError:
                    pass

        target_tolerance = self.method()['force_tolerance']
        if last_tolerance <= target_tolerance:
            return DONE
        else:
            return NOT_CONVERGED

class ASESiestaRelaxation(API.RelaxationTask):
    def __init__(
            self,
            item,
            shorthand='CG',
            ):
        API.RelaxationTask.__init__(
                self,
                item,
                shorthand=shorthand,
                )

    def explain(self):
        basis_set = self.method().basisSet(full_description=True)
        energy_shift = self.method()['energy_shift']/meV
        xc = self.method()['xc']
        spin = self.method()['spin']
        spin = {'UNPOLARIZED': '', 'COLLINEAR': 'spin-polarized ', 'FULL': 'non-collinear spin '}[spin]
        citations =  [
                 "J. M. Soler, E. Artacho, J. D. Gale, A. Garcia, J. Junquera, P. Ordejon and D. Sanchez-Portal, J. Phys.:Condens. Matter, 2002, 14, 2745" ]

        if xc == 'PBE':
            xc = 'Perdew-Burke-Ernzerhof(PBE)'
            citations.append( "J. Perdew, K. Burke and M. Erzerhof, Phys. Rev. Lett., 1996, 77, 3865")

        force_tolerance = self.method()['force_tolerance']
        mesh_cutoff = self.method()['mesh_cutoff']/Ry
        t = (spin, basis_set, energy_shift, xc, mesh_cutoff, force_tolerance)
        return API.Explanation(
                "Geometric relaxations were performed using %sdensity-functional theory(DFT) using the SIESTA code.[*] We used a %s basis set with orbital radii defined using a %d meV energy shift. The %s exchange-correlation potential,[*] and a real-space grid equivalent to a %d Ry plane-wave cutoff. Forces were relaxed until forces were smaller than %.3f eV/Ang." % t,
                citations,
                )

    def estimate(self):
        NE = self.system().inputAtoms().number_of_valence_electrons()
        relaxation_steps = 20

        return float(NE**3)*relaxation_steps

    def runDirectory(self, path=LOCAL):
        base_run = API.RelaxationTask.runDirectory(self, path=path)

        if self.system().constraint() is None:
            path = base_run
        else:
            path =join(base_run, 'CCG_%s' % self.system().constraint().identifier())

        return path

    def executable(self):
        return 'siesta'

    def makeCommands(self, form):
        commands = []
        for name, force_tolerance, dm_tolerance in self.preCalculations():
            fdf_name = name + '.' + self.method().runFileEnding()
            if os.path.exists(join(self.runDirectory(), fdf_name)):
                command = form%(fdf_name, 'TMP.out')
                commands.append(command)
                commands.append('mv TMP.out ' + name + '.out')

        commands.append('rm siesta.DM')
        commands.append('rm siesta.CG')
        commands.append(form%(self.runFile(path=API.TAIL), 'TMP.out'))
        commands.append('mv TMP.out %s'%self.outFile(path=API.TAIL))
        commands.append('mv siesta.out %s' % self.siestaOutFile(path=API.TAIL))

        return commands

    def inputAtoms(self):
        atoms = self.system().system().atoms()

        return atoms

    def relaxation(self):
        if not self.isDone():
            print self.outFile()
            print self.path()
            print self._getLocalStatus()
            print self.system()
            raise Exception('%s is not done'%self.system().name())

        pbc = self.system().pbc()
        atoms = self.method().relaxedAtoms(self.runDirectory(), pbc)

        return atoms

    def hasOpenBoundaries(self):
        return self.inputAtoms().hasOpenBoundaries()

    def _getLocalStatus(self):
        outfile = self.outFile(path=API.LOCAL)
        if not os.path.exists(outfile):
            return API.NOT_STARTED

        with open(outfile, 'r') as f:
             output = f.read()

        if 'RuntimeError:' in output:
            return API.FAILED
        else:
            return API.DONE

    def preCalculations(self):
        return [
                ('VERY_BAD', 1.0, 0.01),
                ('BAD', 0.1, 0.001),
                ('OK', 0.05, 0.0001),
               ]

    def write(self):
        system = self.system()
        atoms = system.system().atoms()
        kpts = system.parameters().kpts()
        method = self.method()

        fair_kpts = [max(1, int(np.ceil(k/2.0))) for k in kpts]
        for name, force_tolerance, dm_tolerance in self.preCalculations():
            tmp_method = method.copy(force_tolerance=force_tolerance, dm_tolerance=dm_tolerance)
            ending = '.' + self.method().runFileEnding()
            filename = join(self.runDirectory(), name + ending)
            tmp_method.writeRelaxation(
                runfile=filename,
                atoms=atoms,
                kpts=fair_kpts,
                relax_cell=self.system().relaxCell(),
                save_behaviour=self.saveBehaviour(),
                )

        method.writeRelaxation(
                runfile=self.runFile(),
                atoms=atoms,
                kpts=kpts,
                relax_cell=system.relaxCell(),
                save_behaviour=self.saveBehaviour(),
                )

    def saveBehaviour(self):
        return {'hamiltonian': False, 'density_matrix':False}

    def siestaOutFile(self, path=API.TAIL):
        return join(self.runDirectory(path=path), 'relaxation.siesta.out')

    def neededFiles(self):
        return self.method().neededFiles()

    def resultFiles(self):
        return ['*.out', '*.XV', '*.EIG', '*.traj', '*.fdf']

    def _getLocalStatus(self):
        outfile = self.outFile(path=API.LOCAL)
        if not os.path.exists(outfile):
            return API.NOT_STARTED

        with open(outfile, 'r') as f:
             output = f.readlines()

        last_tolerance = 1000
        for line in output:
            line = line.split()
            if len(line) > 0:
                try:
                    last_tolerance = float(line[-1])
                except ValueError:
                    pass

        target_tolerance = self.method()['force_tolerance']
        if last_tolerance <= target_tolerance:
            return DONE
        else:
            return NOT_CONVERGED


class TBTransRun(API.Task):
    def __init__(self,
                 transmission,
                 ):
        if hasattr(transmission.system(), 'mainTask'):
            path = transmission.system().mainTask().runDirectory()
        else:
            path = join(transmission.method().path(),
                        transmission.system().path())

        open_boundary = OpenSystemOneShot(
                system=transmission.system(),
                method=transmission.method(),
                path=path,
                shorthand='Analysis',
                identifier=None,
                )
        inputs = [open_boundary]
        self.__transmission = transmission
        API.Task.__init__(
                self,
                path=open_boundary.path(),
                shorthand=open_boundary.shorthand(),
                identifier=open_boundary.identifier(),
                method=open_boundary.method(),
                host=open_boundary.host(),
                inputs=inputs,
                )

    def readData(self, item, **kwargs):
        files = os.listdir(self.runDirectory())
        start = 'siesta.TBT.AVTRANS'
        test = re.compile(start + '*')
        files = filter(test.search, files)
        results = OrderedDict()
        for filename in files:
            end = filename[len(start)+1:]
            fro, to = end.split('-')
            fro, to = [entry[3:] for entry in (fro, to)]
            filename = join(self.runDirectory(), filename)
            results[(fro, to)] = readTransmissionFile(filename)

        return results

    def neededFiles(self):
        return ['*.TSHS', '*.fdf']

    def resultFiles(self):
        return ['*.out', '*.TBT.*']

    def executable(self):
        return 'tbtrans'

    def hasOpenBoundaries(self):
        return True

    def energies(self):
        return self.__transmission.energies()

    def kpts(self):
        return self.__transmission.kpts()

    def write(self):
        energies = self.energies()
        kpts = np.ones(3)
        kpts[:len(self.kpts())] = self.kpts()
        script = """%include siesta.fdf
%block TBT.Contours
   tbt-1
%endblock
%block TBT.Contour.tbt-1
    part line""" + """
    from %.2f eV to %.2f eV
    delta %.3f eV
    method simpson-mix"""%(energies.start, energies.stop, energies.step) + """
%endblock

%block TBT.kgrid_Monkhorst_Pack""" + """
%d  0  0 0.0
0  %d  0 0.0
0  0  %d 0.0"""% tuple(kpts) + """
%endblock"""
        with open(self.runFile(), 'w') as f:
            f.write(script)

    def makeCommands(self, form):
        return [
                form % (self.runFile(path=TAIL), 'TMP.out'),
                'mv TMP.out %s' % self.outFile(path=TAIL),
                ]

    def runFile(self, path=LOCAL):
        filename = 'TRANSMISSION.fdf'
        return join(self.runDirectory(path=path), filename)

    def outFile(self, path=LOCAL):
        filename = 'TRANSMISSION.out'
        return join(self.runDirectory(path=path), filename)

    def _getLocalStatus(self):
        outfile = self.outFile(path=API.LOCAL)
        if not os.path.exists(outfile):
            return API.NOT_STARTED

        with open(outfile, 'r') as f:
             output = f.read()

        if 'RuntimeError:' in output:
            return API.FAILED
        else:
            return API.DONE
