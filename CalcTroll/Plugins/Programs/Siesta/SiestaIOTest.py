# Written by Mads Engelund, 2017, http://espeem.com
import unittest
from CalcTroll.Core.Test.Case import CalcTrollTestCase

from SiestaIO import *

class SiestaIOTest(CalcTrollTestCase):
    def testReadEigenvaluesH2(self):
        test_h2 = join(os.path.dirname(__file__), 'TestFiles', 'h2')
        EF, eigs = readEigenvalues(test_h2)

        self.assertEqual(EF, -4.918)
        self.assertEqual(eigs.shape, (1, 2, 1))

    def testReadEigenvaluesStarphene(self):
        test_starphene = join(os.path.dirname(__file__), 'TestFiles', 'starphene')
        EF, eigs = readEigenvalues(test_starphene)

        self.assertEqual(EF, -6.5176)
        self.assertEqual(eigs.shape, (1, 138, 1))

if __name__=='__main__':
    unittest.main()
