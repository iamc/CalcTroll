from ase.constraints import ModelSurfacePotential
import numpy as np
from CalcTroll.API import DEFAULT

from Molecule import Molecule
from PolyaromaticHydroCarbon import PolyaromaticHydroCarbon

class Test(PolyaromaticHydroCarbon):
    def __init__(self, name=DEFAULT, parameters=DEFAULT):
        indices = [
            (1, 2),
            (-1, 0), (0, 0),
        ]
        folds = (1, 2, -30),

        PolyaromaticHydroCarbon.__init__(
            self,
            indices,
            name=name,
            folds=folds,
            parameters=parameters,
        )

class PentaHeliceneIntermediary(PolyaromaticHydroCarbon):
    def __init__(self, name=DEFAULT, parameters=DEFAULT):
        indices = [
            (1, 2),
            (1, 1), (2, 1),
            (-2, -0),  (-1, 0), (0, 0), (1, 0),
            (0, -1), (1, -1),
            (-1, -2),
        ]
        folds = (4, 5, -5),

        PolyaromaticHydroCarbon.__init__(
            self,
            indices,
            name=name,
            folds=folds,
            parameters=parameters,
        )


class PentaHelicene(PolyaromaticHydroCarbon):
    def __init__(self, name=DEFAULT, parameters=DEFAULT):
        indices = [
            (1, 2),
            (1, 1), (2, 1),
            (-2, -0),  (-1, 0), (0, 0), (1, 0),
            (0, -1), (1, -1),
            (-1, -2),
        ]
        folds = (4, 5, -70), (0, 1, -70)

        PolyaromaticHydroCarbon.__init__(
            self,
            indices,
            name=name,
            folds=folds,
            rotations=({'a':-np.pi/6, 'v':'-y'},),
            parameters=parameters,
        )
