# Written by Mads Engelund, 2017, http://espeem.com
import unittest
import numpy
from ase.visualize import view
from CalcTroll.Core.Test.Case import CalcTrollTestCase

from ConjugatedMolecules import *

class ConjugatedMoleculesTest(CalcTrollTestCase):
    def testConstruction(self):
        Pentacene = Acene(5)
        self.assertEqual(Pentacene.name(), 'Pentacene')

    def testConstruction(self):
        molecule = Decaphene()
        self.assertEqual(molecule.name(), 'Decaphene')

if __name__=='__main__':
    unittest.main()
