from ase.atoms import Atom
from ase.atoms import Atoms as ASEAtoms
import numpy as np
from ase.visualize import view

from CalcTroll.Core.Utilities import vectorLength
from CalcTrollASE.Atoms import Atoms
from CalcTrollASE.AtomUtilities import removeCopies, orderAtoms
from CalcTroll.Plugins.Systems.Molecule import Molecule

from CalcTroll import API
from CalcTroll.API import DEFAULT

ATOM_UNIT = Atoms(
    [
        Atom('C', (-1.2, -0.7 ,0.0,)),
        Atom('C', (-1.2,  0.7 ,0.0,)),
        Atom('C', ( 0.0,  1.4 ,0.0,)),
        Atom('C', ( 1.2,  0.7 ,0.0,)),
        Atom('C', ( 1.2, -0.7 ,0.0,)),
        Atom('C', ( 0.0, -1.4 ,0.0,)),
    ],
)

class PolyaromaticHydroCarbon(Molecule):
    def __init__(
            self,
            indices,
            folds=tuple(),
            passivate=True,
            flourinate=tuple(),
            rotations=tuple(),
            name=DEFAULT,
            parameters=DEFAULT,
    ):
        indices = np.array(indices)
        groups = [range(len(indices))]
        for i1, i2, a in folds:
            fold_group = None
            for group in groups:
                if i1 in group and i2 in group:
                    fold_group = group
                    continue
            if fold_group is None:
                raise ValueError()
            groups.remove(fold_group)
            group.remove(i2)
            g_indices = indices[np.array(group)]
            take1 = connectedGroup(indices[i1], g_indices)
            take2 = np.logical_not(take1)

            group1 = list(np.array(group)[take1])
            group2 = list(np.array(group)[take2])
            group2.append(i2)
            group2 = sorted(group2)
            groups.extend([group1, group2])

        v1 = np.array([2.4, 0.0, 0.0])
        a = np.pi/6
        v2 = 2.4*np.array([-np.sin(a), np.cos(a), 0.0])

        group_folds = []
        for i1, i2, a in folds:
            index1 = indices[i1]
            index2 = indices[i2]
            vec1 = index1[0]*v1 + index1[1]*v2
            vec2 = index2[0]*v1 + index2[1]*v2
            av = (vec1 + vec2)/2.
            diff = vec2 - vec1
            normal = np.array([-diff[1], diff[0], 0])
            for j in range(len(groups)):
                if i1 in groups[j]:
                    j1 = j
                if i2 in groups[j]:
                    j2 = j
            a = float(a)/180*np.pi
            group_folds.append((j1, j2, av, normal, a))

        atoms = Atoms()

        tags = []
        for i, group in enumerate(groups):
            g_indices = indices[np.array(group)]
            for index in g_indices:
                tmp = ATOM_UNIT.copy()
                tmp.positions += index[0]*v1 + index[1]*v2
                atoms += tmp
                tags.extend([i]*len(tmp))

        atoms.set_tags(tags)

        positions = []
        for j, n in flourinate:
            pos = ATOM_UNIT.get_positions()[n]
            index = indices[j]
            pos += index[0]*v1 + index[1]*v2
            positions.append(pos)
        positions = np.array(positions)

        atoms = passivate_partial(atoms, positions, symbol='F')
        tags = atoms.get_tags()

        for i1, i2, av, norm, a in group_folds:
            take = np.arange(len(atoms))[tags==i1]
            tmp = atoms[take]
            tmp.rotate(a=a, v=norm, center=av)
            atoms.positions[take] = tmp.positions


        padding = 10
        maxi = atoms.positions.max(axis=0)
        mini = atoms.positions.min(axis=0)

        ran = maxi - mini + padding
        atoms.cell[0, 0] = ran[0]
        atoms.cell[1, 1] = ran[1]
        atoms.cell[2, 2] = ran[2]

        atoms = removeCopies(atoms)
        atoms = orderAtoms(atoms)

        if passivate:
            atoms = self.passivate(atoms)

        for rotation in rotations:
            atoms.rotate(**rotation)

        Molecule.__init__(self, atoms, name=name, parameters=parameters)

    def passivate(self, atoms):
        return passivate_atoms(atoms)




class CarbonRingChain(Molecule):
    def __init__(
            self,
            indices,
            passivate=True,
            flourinate=tuple(),
            raise_rings=DEFAULT,
            name=DEFAULT,
            parameters=DEFAULT,
    ):
        if raise_rings is DEFAULT:
            raise_rings = {}
        v1 = np.array([2.4, 0.0, 0.0])
        a = np.pi/6
        v2 = 2.4*np.array([-np.sin(a), np.cos(a), 0.0])

        atoms = Atoms()

        tags = []
        for i, index in enumerate(indices):
            tmp = ATOM_UNIT.copy()
            tmp.positions += index[0]*v1 + index[1]*v2
            atoms += tmp
            tags.extend([i]*len(tmp))

        atoms.set_tags(tags)

        positions = []
        for j, n in flourinate:
            pos = ATOM_UNIT.get_positions()[n]
            index = indices[j]
            try:
                raise_ring = raise_rings[j]
            except KeyError:
                raise_ring = 0
            pos += index[0]*v1 + index[1]*v2 + np.array([0, 0, raise_ring])
            positions.append(pos)
        positions = np.array(positions)

        tags = atoms.get_tags()
        for ring, raise_ring in raise_rings.iteritems():
            take = np.arange(len(atoms))[tags==ring]
            atoms.positions[take, 2] += raise_ring

        atoms = passivate_partial(atoms, positions, symbol='F')


        padding = 10
        maxi = atoms.positions.max(axis=0)
        mini = atoms.positions.min(axis=0)

        ran = maxi - mini + padding
        atoms.cell[0, 0] = ran[0]
        atoms.cell[1, 1] = ran[1]
        atoms.cell[2, 2] = ran[2]

        atoms = removeCopies(atoms)
        atoms = orderAtoms(atoms)

        if passivate:
            atoms = passivate_atoms(atoms)

        Molecule.__init__(self, atoms, name=name, parameters=parameters)


CHAIN_LINKS = np.array([
    (-2, -1),
    (-1, 1),
    (1, 2),
    (2, 1),
    (1, -1),
    (-1, -2),
])

class CarbonRingChain2(Molecule):
    def __init__(
            self,
            links,
            name=DEFAULT,
            parameters=DEFAULT,
        ):
        indices = [np.array((0, 0))]
        start = 0
        for link in links:
            new = (start + link)%6
            print new
            next_index = indices[-1] + CHAIN_LINKS[new]
            indices.append(next_index)
            start = new + 3

        view(PolyaromaticHydroCarbon(indices=indices).atoms())
        print indices
        ddddddddddd

        Molecule.__init__(self, atoms, name=name, parameters=parameters)


def passivate_atoms(atoms):
    symbols = np.array(atoms.get_chemical_symbols())
    pos = atoms.get_positions()[symbols=='C']

    return passivate_partial(atoms, pos)

def passivate_partial(atoms, pos, symbol='H'):
    a_pos = atoms.get_positions()
    tags = atoms.get_tags()
    hydrogens = []
    new_tags = []
    for i, p in enumerate(pos):
        diff = np.sqrt((abs(a_pos - p)**2).sum(axis=1))
        take1 = diff < 0.1
        take2 = np.logical_and(diff < 1.7, diff > 0.1)
        npos = a_pos[take2]
        if len(npos) == 2 and (take1).sum() == 1:
            av = npos.sum(axis=0)/2
            diff = p - av
            diff /= vectorLength(diff)
            hydrogens.append(p+diff)
            new_tags.append(tags[take1][0])

    hydrogens = np.array(hydrogens)
    hydrogens = Atoms(symbol*len(hydrogens), positions=hydrogens)
    hydrogens.set_tags(new_tags)
    atoms = atoms.copy()
    atoms += hydrogens

    return atoms

ADJACENT_TO_00 = np.array([
    ( 0,-1),
    (-1,-1),
    ( 1, 0),
    (-1, 0),
    ( 0, 1),
    ( 1, 1),
])

def connectedGroup(index, indices):
    """ Find the group of indices that is adjacantly connected"""
    o_indices = indices
    index = tuple(index)
    indices = set(map(tuple, indices))
    group = set()
    adjacent = set()
    add_to_group = set([index])
    while len(add_to_group) > 0:
        group.update(add_to_group)
        indices.difference_update(add_to_group)

        new_adjacent = []
        for index in add_to_group:
            new_adjacent.extend(map(tuple, ADJACENT_TO_00 + index))
        new_adjacent = set(new_adjacent)
        new_adjacent.difference_update(group)
        new_adjacent.difference_update(adjacent)

        adjacent = new_adjacent
        add_to_group = indices.intersection(adjacent)

    connected = np.array([tuple(i) in group for i in o_indices])

    return connected

