import numpy as np
from ase.atoms import Atom
from CalcTroll.Plugins.Systems.Molecule import Molecule

from CalcTrollASE.Atoms import Atoms
from CalcTroll import API
from CalcTroll.API import DEFAULT

class Rotated(Molecule):
    def __init__(
            self,
            molecule,
            a=180,
            v='x',
            potential_strength=0.2,
            name=DEFAULT,
            parameters=DEFAULT,
            ):
        self.__molecule = molecule
        if name is DEFAULT:
            name = '%s_%d_%s' % (molecule.name(), a, v)

        if parameters is DEFAULT:
            parameters = molecule.parameters()

        atoms = self.__molecule.atoms(parameters=parameters)
        atoms.rotate(a=a*(np.pi/180), v=v)

        Molecule.__init__(
                self,
                atoms,
                potential_strength=potential_strength,
                name=name,
                parameters=parameters)
