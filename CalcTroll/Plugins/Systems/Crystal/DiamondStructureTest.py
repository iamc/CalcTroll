# Written by Mads Engelund, 2017, http://espeem.com
import unittest
from CalcTroll.Core.Test.Case import CalcTrollTestCase
from CalcTroll.Core.Test.Dummies import TestRelaxationSystem
from CalcTroll.Plugins.Systems.Crystal.Crystal3D import CrystalParameters3D
from DiamondStructure import DiamondStructure

class DiamondStructureTest(CalcTrollTestCase):
    def testDiamond(self):
        crystal = DiamondStructure(symbol='C')

    def testCalculate(self):
        crystal = DiamondStructure(symbol='Si')
        atoms = crystal.atoms()
        atoms.cell *= 1.5
        store = atoms.copy()
        new_crystal = TestRelaxationSystem(crystal, atoms)
        atoms = new_crystal.atoms()

        self.assertArraysEqual(store.cell, atoms.cell)

    def testCopy(self):
        crystal = DiamondStructure(symbol='Si')
        atoms = crystal.atoms()
        atoms.cell *= 1.5
        store = atoms.copy()
        crystal = TestRelaxationSystem(crystal, atoms)
        new_crystal = crystal.copy()
        self.assertIsInstance(new_crystal, DiamondStructure)
        self.assertEqual(new_crystal.symbol(), 'Si')
        atoms = new_crystal.atoms()
        self.assertArraysEqual(store.cell, atoms.cell, 5)

if __name__=='__main__':
    unittest.main()

