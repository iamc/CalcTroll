# Written by Mads Engelund, 2018, http://espeem.com
import numpy as np
from numpy.linalg import norm
from CalcTroll.Core.Utilities import vectorLength
from CalcTroll.Core.Utilities import convertToBasis
from CalcTroll.Core.Utilities import convertToIntegers
from CalcTrollASE.AtomUtilities import wrap
from CalcTrollASE.AtomUtilities import removeCopies

def getEquivalentPoints(unit_cell, distance):
    unit_cell = np.array(unit_cell)

    l_b=len(unit_cell)
    size = 2*distance

    # Find the amount of repetitions that will contain the necessary atoms.
    l0 = norm(unit_cell[0])
    n0=int(np.ceil(size/l0))

    l1 = norm(unit_cell[1])
    comp1 = unit_cell[1]-np.dot(unit_cell[1]/l1,unit_cell[0]/l0)*unit_cell[0]
    lc1=np.sqrt(np.sum(comp1**2))
    n1=int(np.ceil(size/lc1))

    l2 = norm(unit_cell[2])
    comp2=unit_cell[2] - np.dot(unit_cell[2]/l2,unit_cell[0]/l0)*unit_cell[0]
    lc2=np.sqrt(np.sum(comp2**2))
    comp2=comp2-np.dot(comp2/lc2,comp1/lc1)*comp1
    lc2=np.sqrt(np.sum(comp2**2))
    n2=int(np.ceil(size/lc2))

    R = np.zeros((n0*n1*n2, 3), np.float)
    for i0 in range(n0):
        for i1 in range(n1):
            for i2 in range(n2):
                R[i0*n1*n2 + i1*n2 + i2] = unit_cell[0]*i0 + unit_cell[1]*i1 + unit_cell[2]*i2

    # Find the box with angles close to 90 degress to contain the atoms.
    v0=n0*unit_cell[0]
    v1=n1*unit_cell[1]
    x=np.dot(unit_cell[0]/l1,v1/l1)
    v1=v1-np.floor(x)*unit_cell[0]

    v2=n2*unit_cell[2]
    x=np.dot(unit_cell[0]/l0,v2/l2)
    v2=v2-np.floor(x)*unit_cell[0]
    x=np.dot(unit_cell[1]/l1,v2/l2)
    v2=v2-np.floor(x)*unit_cell[1]
    vectors=np.array([v0,v1,v2])

    # Move the atoms to the correct box dimensions and centered around the correct position.
    M=np.transpose(np.matrix(vectors))
    Minv=M**(-1)
    R=np.transpose(np.tensordot(Minv,R,(1,1)))
    temp_center = np.zeros(3, np.float)
    shift=temp_center - np.array([.5,.5,.5])
    R[:,:l_b]=((R-shift)%1+shift)[:,:l_b]
    R=np.transpose(np.tensordot(M,R,(1,1)))

    length = vectorLength(R, 1)
    condition = np.less(length, distance + 1e-13)
    R = R[condition]

    return R


def findNeededRepetitions(unit_cell, radius, extent=(0,0,0)):
    # Find the amount of repetitions that will contain the necessary atoms.
    l0=np.sqrt(np.sum(unit_cell[0]**2))
    n0=int(np.ceil(sum(abs(radius*unit_cell[0]/l0**2))))
    n0+=int(np.ceil(sum(extent*unit_cell[0]/l0**2)))
    if len(unit_cell) == 1:
        return (n0,)

    l1=np.sqrt(np.sum(unit_cell[1]**2))
    comp1=unit_cell[1]-np.dot(unit_cell[1]/l1,unit_cell[0]/l0)*unit_cell[0]
    lc1=np.sqrt(np.sum(comp1**2))
    n1=int(np.ceil(radius/lc1))
    n1+=int(np.ceil(np.sum(extent/lc1)))

    if len(unit_cell) == 2:
        return n0, n1

    cross = np.cross(unit_cell[0], unit_cell[1])
    cross /= vectorLength(cross)
    pro = abs(np.dot(cross, unit_cell[2]))
    n2=int(np.ceil(radius/pro))
    n2+=int(np.ceil(np.sum(extent/pro)))

    return n0, n1, n2


def fitBoxIntoBox(box, other_box):
    radius = max(map(vectorLength, other_box))
    repeats = findNeededRepetitions(box, radius)
    return repeats


def makeLargeEnoughQuiteOrthogonalBox(unit_cell, radius, extent=(0,0,0)):
    repeats = findNeededRepetitions(unit_cell, radius, extent)
    vectors = makeQuiteOrthogonalVectors(unit_cell, repeats)

    return vectors, repeats


def makeQuiteOrthogonalVectors(unit_cell, repeats):
    # Find the box with angles close to 90 degress to contain the atoms.
    n = repeats
    l = map(vectorLength, unit_cell)

    v0=n[0]*unit_cell[0]
    if len(unit_cell) == 1:
        return np.array([v0])

    v1=n[1]*unit_cell[1]
    x=np.dot(unit_cell[0],v1/l[1])/l[1]
    v1=v1-np.around(x)*unit_cell[0]
    if len(unit_cell) == 2:
        return np.array([v0, v1])

    v2=n[2]*unit_cell[2]
    v2 = matrix(v2).transpose()
    A = matrix(unit_cell[:2]).transpose()
    Ainv = moorePenroseInverse(A)
    x = Ainv*v2
    x = np.array(np.around(x), dtype=int)
    v2 = np.array(v2 - A*x)
    v2 = v2.transpose()[0]

    return np.array([v0, v1, v2])


def makeSurface(crystal, miller_indices):
    unit_cell = crystal.unitCell()
    normal_vector = crystal.normalVector(miller_indices)
    result = makeSurfaceUnitCell(unit_cell, normal_vector)

    return result


def makeSurfaceUnitCell(unit_cell, normal_vector):
    l_b = len(unit_cell)

    if l_b == 1:
        return unit_cell

    # Make estimate of the length of the surface vectors.
    max_length = 7

    # Exclude 0,0,0
    R = getEquivalentPoints(unit_cell, distance=max_length)
    length = np.sqrt(np.sum(R**2,1))
    condition = np.greater(length,1e-13)
    R = R[condition]

    pro = np.tensordot(R, normal_vector, (1,0))
    condition = np.less(abs(pro),1e-10)
    inplane = R[condition]

    condition = np.greater(pro, 1e-10)
    pointing_outwards = R[condition]

    pro_out = abs(np.repeat(pro, condition,0))
    condition = np.less(abs(pro_out-np.min(pro_out)),1e-13)
    pointing_outwards = pointing_outwards[condition]

    length = np.sqrt(np.sum(pointing_outwards**2,1))
    condition = np.less(abs(length-np.min(length)),1e-13)
    pointing_outwards = pointing_outwards[condition]
    vector_out = pointing_outwards[0]

    discard = True
    while discard:
        length = vectorLength(inplane**2,1)
        max = np.max(length)
        condition = np.less(abs(length-max),1e-13)
        not_condition = np.greater_equal(abs(length-max),1e-13)

        longest = inplane[condition]
        smaller = inplane[not_condition]

        v1 = longest[0]
        discard = False
        for v2 in smaller:
            for v3 in smaller:
                for i,j in [(1,1),(1,-1),(-1,-1),(-1,1)]:
                    if np.sum((i*v2+j*v3-v1)**2) < 1e-3:
                        discard = True

        if discard:
            inplane = smaller

    if l_b ==3:
        v1 = inplane[0]
        for v2 in inplane[1:]:
            area = vectorLength(np.cross(v2,v1))
            if area>1e-5:
                break

        dot = np.dot(v1,vector_out)
        if dot<0:
            v1=-v1
        dot = np.dot(v2,vector_out)
        if dot<0:
            v2=-v2
        cross = np.cross(v1,v2)
        dot = np.dot(cross,vector_out)
        if dot<0:
            v1,v2 = v2,v1
        v3 = vector_out


    elif l_b==2:
        v1 = inplane[0]
        v2 = vector_out
        cross = np.cross(v1,v2)
        v3 = cross/vectorLength(cross)

    new_unit_cell = np.array([v1, v2, v3])
    new_unit_cell, remainder = convertToBasis(new_unit_cell, unit_cell)
    new_unit_cell = convertToIntegers(new_unit_cell)

    return new_unit_cell


def volumeOfCell(cell):
    v1, v2, v3 = np.array(cell)
    tmp=np.cross(v1, v2)
    area=np.dot(tmp, v3)

    return area


def makePeriodicSample(crystal, radius=5):
    unit_cell=crystal.unitCell()
    l_b=len(unit_cell)
    atoms = crystal.toAseAtoms()

    repeats = findNeededRepetitions(unit_cell, np.array([radius]*3))
    sample = atoms.repeat(repeats)

    vectors = makeQuiteOrthogonalVectors(unit_cell, repeats)

    sample.set_cell(vectors)
    sample = removeCopies(sample)
    sample = wrap(sample)

    return sample

def makeSample(crystal, vectors):
    radius = np.max(vectorLength(vectors))
    repeats = findNeededRepetitions(crystal.unitCell(), radius)
    atoms = crystal.toAseAtoms()
    atoms = atoms.repeat(repeats)
    atoms.set_cell(vectors)
    atoms = wrap(atoms)
    atoms = removeCopies(atoms)

    return atoms

def makeSlabWithVectors(atoms, vectors, n_layers):
    unit_cell = atoms.get_cell()
    vectors[2] *= -1
    vectors = np.dot(unit_cell.transpose(), vectors.transpose()).transpose()
    repeats = fitBoxIntoBox(unit_cell, vectors)
    atoms = atoms.repeat(repeats)
    atoms.set_cell(vectors)
    atoms = wrap(atoms)
    atoms = removeCopies(atoms)

    atoms = atoms.repeat((1,1,n_layers))

    return atoms


