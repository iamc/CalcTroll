# Written by Mads Engelund, 2017, http://espeem.com
# Class describing 1-dimensional crystals such as:
# graphene nanoribbons,
# nanotubes,
# idealized mono-atomic metal wires,

import numpy as np
from CalcTrollASE.Atoms import Atoms
from CalcTroll.Core.Utilities import vectorLength
from CalcTrollASE.AtomUtilities import orderAtoms
from CalcTrollASE.AtomUtilities import removeCopies
from CalcTrollASE.AtomUtilities import makeAtomsRelaxed
from CalcTroll.Core.System.BasisChanger import BasisChanger, Transformation
from CalcTroll.Plugins.Systems.Crystal.Crystal import Crystal, CrystalParameters
from CalcTroll import API
from CalcTroll.API import DEFAULT

class CrystalParameters1D(CrystalParameters):
    def neededKpts(self, cell):
        scales = self['length_scales']
        kpts = np.ones(3, int)

        length=np.max(abs(cell[-1]))
        number=int(np.ceil(scales['correlation_length']/length))
        kpts[-1] = number

        return kpts

class Crystal1D(Crystal):
    @classmethod
    def parameterClass(cls):
        return CrystalParameters1D

    def __init__(
            self,
            atoms,
            parameters=DEFAULT,
            ):
        assert all(atoms.pbc == np.array([False, False, True]))
        self.__atoms = Atoms(atoms)
        API.System.__init__(
                self,
                sub_systems=tuple(),
                parameters=parameters,
                )

    def setUpBasisChanger(self):
        atoms = self.atoms()
        cell = atoms.cell

        basis_changer = BasisChanger(
                transformations=[Transformation(to='orth_crystal', fro='crystal', trans_matrix=cell.transpose())]
                )
        return basis_changer

    def pbc(self):
        return [False, False, True]

    def defaultParameters(self, parameters=DEFAULT):
        if parameters is DEFAULT:
            parameters = CrystalParameters1D()

        vectors = parameters['vectors']
        if parameters['vectors'] is DEFAULT:
            vectors = ((1,),)

        kpts = parameters['kpts']
        if kpts is DEFAULT:
            kpts = parameters.neededKpts(self.__atoms.cell[self.__atoms.pbc])

        parameters = parameters.copy(kpts=kpts, vectors=vectors)

        return parameters

    def inputAtoms(self):
        return self.__atoms.copy()

    def relaxationFactor(self):
        if self.relaxation() is None:
            return 1.0
        lr = vectorLength(self.relaxation().get_cell()[2])
        lu = vectorLength(self.__atoms.get_cell()[2])

        return lr/lu

    def atomsTemplate(self, parameters=DEFAULT):
        if parameters is DEFAULT:
            parameters = self.defaultParameters()

        vectors = np.array(parameters['vectors'])
        lengths = vectorLength(vectors, 1)
        assert lengths.all()

        atoms = self.__atoms.copy()
        pos = atoms.get_positions()
        size = np.array([pos[:, i].max() - pos[:,i].min() for i in range(3)])
        size = size + parameters['length_scales']['correlation_length']

        repeats = (1, 1, vectors[0][0])
        vectors = atoms.cell[2] * vectors[0][0]

        atoms = atoms.repeat(repeats)
        atoms.set_cell(size)
        atoms.cell[2] = vectors
        atoms = removeCopies(atoms)
        atoms = orderAtoms(atoms)

        return atoms

    def makeAtomsRelaxed(self, atoms, tolerance=1):
        factor = self.relaxationFactor()
        atoms.positions *= factor
        atoms.cell[2] *= factor
        atoms = makeAtomsRelaxed(atoms, self.relaxation(), tolerance=tolerance)

        return atoms

    def unitCell(self, coordinates='orth_crystal'):
        if self.isRelaxed():
            unit_cell = self.relaxation().get_cell()
        else:
            unit_cell = self.__atoms.get_cell()

        unit_cell = unit_cell[np.array(self.pbc())]

        return self.change(unit_cell, to=coordinates, fro='orth_crystal')

