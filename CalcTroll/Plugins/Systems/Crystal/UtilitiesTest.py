# Written by Mads Engelund, 2017, http://espeem.com
import unittest
from CalcTroll.Core.Test.Case import CalcTrollTestCase

from Utilities import *

class UtilitiesTest(CalcTrollTestCase):
    def testMakeSurfaceUnitCell(self):
        unit_cell = [[4.59, 0, 0], [0, 4.59, 0], [0, 0, 2.96]]
        normal_vector = (0, 1, 1)
        result = makeSurfaceUnitCell(unit_cell, normal_vector)

if __name__=='__main__':
    unittest.main()

