# Written by Mads Engelund, 2018, http://espeem.com
import numpy as np
from math import sqrt
from ase.atoms import Atom

from CalcTrollASE.Atoms import Atoms
from CalcTroll.Plugins.Systems.Crystal.Crystal3D import Crystal3D
from CalcTroll.Core.Utilities import vectorLength
from CalcTroll.Plugins.Systems.Crystal.Crystal3D import CrystalParameters3D
from CalcTroll.API import DEFAULT

LATTICE_DICT = {'W': 3.1652}

class BCC(Crystal3D):
    def name(self):
        return 'BCC'

    def __init__(
            self,
            symbol='W',
            parameters=DEFAULT,
            ):
        a = LATTICE_DICT[symbol]
        pos = np.array([[0,0,0]])
        cell = np.array( [
            [1, -1, 1],
            [1,  1, 1],
            [0 ,0 , 2],
            ], np.float)*a/2.0
        atoms = Atoms(symbol, pos, cell=cell, pbc=[True]*3)

        Crystal3D.__init__(self, atoms=atoms, parameters=parameters)

    def conventionalCell(self, coordinates='orth_crystal'):
        c = np.transpose(np.array([[1, 1, -1], [-1, 1, 0], [0, 0, 1]]))
        pc = np.matrix(np.transpose(self.unitCell(coordinates=coordinates)))
        uc = np.transpose(c*pc)

        return np.array(uc)
