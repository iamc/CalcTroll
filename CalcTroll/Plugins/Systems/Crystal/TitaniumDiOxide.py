# Written by Mads Engelund, 2018, http://espeem.com
import numpy as np
from math import sqrt
from ase.atoms import Atom
from CalcTroll.API import DEFAULT

from CalcTrollASE.Atoms import Atoms
from CalcTroll.Plugins.Systems.Crystal import Crystal3D
from CalcTroll.Core.Utilities import vectorLength
from CalcTroll.Plugins.Systems.Crystal.Crystal3D import CrystalParameters3D

class TitaniumDiOxide(Crystal3D):
    def name(self):
        return 'TiO2'

    def __init__(
            self,
            parameters=DEFAULT,
            ):
        a, c = 4.59, 2.96
        ar = np.array
        q = (1./(sqrt(2) + 2))*a
        center = np.array([a, a, c])/2.
        atom1 = Atom('Ti', np.array([0,0,0])     )
        atom2 = Atom('Ti', center)
        atom3 = Atom('O', ar([1,1,0])*q)
        atom4 = Atom('O', ar([1,1,0])*(a - q))
        atom5 = Atom('O', center + ar([1, -1, 0])*q)
        atom6 = Atom('O', center - ar([1, -1, 0])*q)

        cell = np.array([[a, 0, 0],[0, a, 0],[0 ,0 ,c]])
        atoms = Atoms([atom1,
                       atom2,
                       atom3,
                       atom4,
                       atom5,
                       atom6,
                       ], cell=cell, pbc=[True]*3)

        Crystal3D.__init__(self, atoms=atoms, parameters=parameters)

    def conventionalCell(self, coordinates='orth_crystal'):
        return self.unitCell(coordinates=coordinates)

    def relaxationFactor(self):
        relaxation = self.relaxation()
        if relaxation is None:
            return np.identity(3)

        v1, v2, v3 = relaxation.cell
        ra = vectorLength(np.cross(v1, v2))

        c_atoms = self.atomsTemplate(parameters=self.parameters())
        v1, v2, v3 = c_atoms.cell
        aa = vectorLength(np.cross(v1, v2))

        rv = relaxation.get_volume()
        av = c_atoms.get_volume()

        l1 = (ra/aa)**(1./2)
        l2 = (rv/ra)/(av/aa)

        return np.array([[l1, 0, 0], [0, l1, 0], [0, 0, l2]])

