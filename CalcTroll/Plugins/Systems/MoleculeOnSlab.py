# Written by Mads Engelund, 2017, http://espeem.com
import os
from os.path import join
from math import pi
import numpy as np
from ase.visualize import view

from CalcTroll.Plugins.Systems.Molecule import Molecule
from CalcTroll.Plugins.Systems.Surface import Surface
from CalcTroll.Core.System.BasisChanger import BasisChanger, Transformation
from CalcTrollASE.AtomUtilities import makeAtomsRelaxed, orderAtoms
from CalcTroll import API
from CalcTroll.API import DEFAULT

def circleTransform(p, R, direction=0):
    if R is None:
        return p

    maxi, mini = p[:, direction].max(), p[:, direction].min()
    xc = (maxi + mini)/2
    zmin = p[:, 2].min()
    p[:, direction] -= xc

    phi = p[:, direction]/R
    p[:, 2] += R*np.cos(phi)
    p[:, direction] = R*np.sin(phi)

    p[:, 2] += zmin - p[:, 2].min()

    return p

class MoleculeOnSlabParameters(API.Parameters):
    def __init__(
            self,
            molecule_parameters=DEFAULT,
            slab_parameters=DEFAULT,
            ):
        pass

    def kpts(self):
        return self['slab_parameters'].kpts()

    def identifier(self):
        return self['slab_parameters'].identifier()


class MoleculeOnSlab(API.System):
    @classmethod
    def parameterClass(cls):
        return MoleculeOnSlabParameters

    def name(self):
        basename = self.baseName()
        name = ''
        if (self.displacement() != (0.,0.,0.)).any():
            name += 'S%.3f_%.3f_%.3f' % tuple(self.displacement())

        if (self.angles() != (0.,0.,0.)).any():
            name += '_'
            name += 'A%.3f_%.3f_%.3f' % tuple(self.angles())

        curvature = self.curvature()
        if not (curvature is None):
            name += '_'
            if hasattr(curvature, '__len__'):
                name += 'C'
                if curvature[0] is None:
                    name += '_'
                else:
                    name += '%.3f' % curvature[0]
                name += '_'

                if curvature[1] is None:
                    name += '_'
                else:
                    name += '%.3f' % curvature[1]

            else:
                name += 'C%.3f' % curvature

        name = join(basename, name)

        return name

    def baseName(self):
        return join(self.molecule().name(), 'On' + self.slab().name())

    def subParameters(self, parameters=DEFAULT):
        parameters = self.defaultParameters(parameters)

        return (parameters['molecule_parameters'], parameters['slab_parameters'])

    def defaultParameters(self, parameters=DEFAULT):
        slab = self.slab().fullyUnrelaxed()
        surface = self.surface().fullyUnrelaxed()
        molecule = self.molecule().fullyUnrelaxed()

        if parameters is DEFAULT:
            molecule_parameters = DEFAULT
            slab_parameters = DEFAULT
        elif isinstance(parameters,
                (slab.parameterClass(),
                 surface.parameterClass()),
                ):
            molecule_parameters = DEFAULT
            slab_parameters = slab.defaultParameters(parameters=parameters)
        else:
            molecule_parameters = parameters['molecule_parameters']
            slab_parameters = parameters['slab_parameters']

        centers = molecule.atoms().get_positions()

        molecule_parameters = molecule.defaultParameters(parameters=molecule_parameters)
        slab_parameters = slab.defaultParameters(parameters=slab_parameters, centers=centers)

        return MoleculeOnSlabParameters(
                molecule_parameters=molecule_parameters,
                slab_parameters=slab_parameters,
                )

    def __init__(
            self,
            molecule,
            slab,
            displacement=(0,0,0),
            angles=(0,0,0),
            curvature=None,
            parameters=DEFAULT,
            ):
        assert isinstance(molecule, Molecule)
        sub_systems = [molecule, slab]
        self.__displacement = displacement
        self.__angles = angles
        self.__curvature = curvature

        API.System.__init__(
                self,
                sub_systems,
                parameters=parameters,
                )

    def angles(self):
        return np.array(self.__angles, float)

    def curvature(self):
        return self.__curvature

    def displacement(self):
        return  np.array(self.__displacement, float)

    def setUpBasisChanger(self):
        basis_changer = BasisChanger()
        sub_systems = self.subSystems()
        if isinstance(sub_systems[1], Surface):
            surface = sub_systems[1]
        else:
            surface = sub_systems[1].surface()
        extra_atoms = surface.extraAtoms('orth_surface')
        point = extra_atoms.positions.mean(axis=0)
        point = surface.minimalCell()[2]
        point[2] = extra_atoms.positions[:,2].max()
        transformation = Transformation(to='orth_surface', fro='orth_system', rotation_point=[point, [0,0,0]])
        basis_changer.addTransformation(transformation)
        basis_changer.include(self.slab().basisChanger())

        return basis_changer

    def slab(self):
        return self.subSystems()[1]

    def surface(self):
        slab = self.slab()
        if isinstance(slab, Surface):
            return slab
        else:
            return slab.surface()

    def molecule(self):
        return self.subSystems()[0]

    def findAtomIndices(self, identifiers, atoms):
        indices = self.moleculeIndices(atoms)
        if identifiers is None:
            pass
        else:
            raise ValueError

        return indices

    def moleculeIndices(self, atoms):
        return np.arange(len(atoms))[-len(self.molecule()):]

    def slabIndices(self, atoms):
        return np.arange(len(atoms))[:-len(self.molecule())]

    def atoms(self,
            constrained=True,
            parameters=DEFAULT,
            ):
        system = self.fullyUnrelaxed()
        if parameters is DEFAULT:
            parameters = self.parameters()

        parameters = system.defaultParameters(parameters)
        slab = system.slab()

        slab_parameters = parameters['slab_parameters']
        atoms = slab.atoms(
                constrained=constrained,
                initialized=True,
                parameters=slab_parameters,
                )
        atoms = system.change(atoms, to='orth_system', fro='orth_defect')
        atoms.wrap(center=0)
        atoms = orderAtoms(atoms)


        molecule_atoms = self.moleculeAtoms(parameters=parameters['molecule_parameters'])
        atoms += molecule_atoms

        if len(molecule_atoms) > 0:
            name = self.name().replace(os.sep, '_')
            atoms.setRegion(name=name, picker=slice(-len(molecule_atoms), None),
                              cell=atoms.get_cell(), pbc=[False, False, False])

        atoms = self.makeAtomsRelaxed(atoms, self.relaxation())

        return atoms

    def makeSubsystemsRelaxed(self, atoms):
        system = self.fullyUnrelaxed()
        size = len(self.molecule())
        atoms = system.change(atoms, to='orth_defect', fro='orth_system')
        slab = self.slab()
        slab_atoms = atoms[:-size]
        slab_atoms = slab.makeAtomsRelaxed(slab_atoms, self.slab().relaxation())
        atoms.cell = slab_atoms.cell
        atoms.positions[:-size] = slab_atoms.positions

        atoms = self.change(atoms, fro='orth_defect', to='orth_system')

        return atoms

    def makeAtomsRelaxed(self, atoms, relaxation):
        atoms = self.makeSubsystemsRelaxed(atoms)
        if relaxation is None:
            return atoms.copy()

        size = len(self.molecule())
        slab_atoms = atoms[:-size]
        relaxed_slab_atoms = relaxation[:-size]
        atoms.positions[:-size] = makeAtomsRelaxed(slab_atoms, relaxed_slab_atoms, 1.0).positions
        atoms.positions[-size:] = relaxation[-size:].positions

        return atoms

    def tilt(self):
        if self.__shift is None or isinstance(self.shift(), (int,float)) or len(self.__shift) < 4:
            return 0.0
        else:
            return self.__shift[3]

    def inplaneRotation(self):
        if self.__shift is None or isinstance(self.shift(), (int,float)) or len(self.__shift) < 5:
            return 0.0
        else:
            return self.__shift[4]

    def bandStructurePath(self):
        return self.slab().bandStructurePath()

    def rattleAtoms(self):
        atoms = self.molecule()

        return atoms

    def centers(self):
        atoms = self.molecule().atoms()
        positions = atoms.get_positions()
        atoms.rotate([0, 0, 1], a = pi/2.)
        more_positions = atoms.get_positions()

        return np.concatenate((positions, more_positions))

    def moleculeAtoms(self, parameters):
        atoms = self.molecule().atoms(parameters=parameters)
        R = self.curvature()
        if R is None:
            pass
        elif hasattr(R, '__len__'):
            assert len(R) <= 3
            for i in range(len(R)):
                atoms.positions = circleTransform(atoms.positions, R[i], direction=i)

        translation = self.displacement()
        atoms.translate(translation)

        a1, a2, a3 = self.angles()
        atoms.rotate(v=np.array([1,0,0]),a=-(np.pi/180)*a1)
        atoms.rotate(v=np.array([0,1,0]),a=-(np.pi/180)*a2)
        atoms.rotate(v=np.array([0,0,1]),a=-(np.pi/180)*a3)


        return atoms

    def removedAtoms(self):
        r_atoms = self.slab().removedAtoms(coordinates='orth_surface')
        r_atoms = self.change(r_atoms, fro='orth_surface', to='orth_system')

        return r_atoms

    def pbc(self):
        return (True, True, False)
