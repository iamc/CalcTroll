# Written by Mads Engelund, 2018, http://espeem.com
import numpy as np

from CalcTroll.Plugins.Systems.Surface.Surface3D import Surface3D
from CalcTroll.Plugins.Systems.Crystal import TitaniumDiOxide
from CalcTrollASE.Atoms import Atoms
from CalcTroll import API
from CalcTroll.API import DEFAULT

class TitaniumDiOxide011(Surface3D):
    def __init__(
            self,
            parameters=DEFAULT,
            ):
        crystal = TitaniumDiOxide()
        surface = Surface3D(
            crystal=crystal,
            miller_indices=(0, 1, 1),
            surface_cell=[[1,0], [0, 2]],
        )

        p = np.array([
            (0.375, 0 , 1),
            (0.5, 0.5 , 1),
            (-0.125, -0.5 , 1),
            (-1, -1 , 1),
            (0.25, 0.25 , 0),
            (0.75, 0.25 , 0),
            (0.25, 1.25 , 0),
            (0.75, 1.25 , 0),
            (0.0,  1.75 , 0),
            (0.5,  1.75 , 0),
            (0.0,  0.75 , 0),
            (0.5,  0.75 , 0),
            (0.0,  0.75 , 0),
            (0.5,  0.75 , 0),
            (0.375, -0.25 , 0),
            (0.875, -0.25 , 0),
        ])
        p = surface.change(p, fro='minimal_unit_cell',
                                   to='orth_surface')
        p[0] += np.array([0.0,-0.5, 0.5])
        p[2] += np.array([0.0, 0.5, 0.5])
        p[4] += np.array([0, 0.0, 1])
        p[5] += np.array([0, 0.0, 1])
        p[6] += np.array([0, 0.0, 1])
        p[7] += np.array([0, 0.0, 1])
        p[8] += np.array([-0.75, 0.0, 2.0])
        p[9] += np.array([-0.75, 0.0, 2.0])
        p[10] += np.array([0, 0.0, 2.0])
        p[11] += np.array([0, 0.0, 2.0])
        p[12] += np.array([1,  2.0, 3.5])
        p[13] += np.array([1, -2.0, 3.5])
        p[14] += np.array([0.0,  0.0, 4.5])
        p[15] += np.array([0.0, -0.0, 4.5])
        p = surface.change(p, to='minimal_unit_cell',
                                   fro='orth_surface')

        extra_atoms = Atoms('Ti'*4+'O'*(len(p)-4), p)

        Surface3D.__init__(self,
                           crystal=surface['crystal'],
                           miller_indices=surface['miller_indices'],
                           surface_cell=surface['surface_cell'],
                           extra_atoms=extra_atoms,
                           parameters=parameters,
                           )


