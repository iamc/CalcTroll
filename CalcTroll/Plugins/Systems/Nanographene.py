from ase.atoms import Atom
from ase.atoms import Atoms as ASEAtoms
import numpy as np
from ase.visualize import view
from ase.constraints import Hookean

from CalcTrollASE.Atoms import Atoms
from CalcTrollASE.AtomUtilities import removeCopies
from CalcTroll.Plugins.Systems.Molecule import Molecule

from CalcTroll import API
from CalcTroll.API import DEFAULT

from PolyaromaticHydroCarbon import PolyaromaticHydroCarbon
from ConjugatedMolecules import triplicateEndGroup

class Nanographene(Molecule):
    def __init__(self, name=DEFAULT, parameters=DEFAULT):
        molecule = PolyaromaticHydroCarbon(
           indices=[(1,  0), (2,  0),
                    (2,  1), (3,  1),
                    (1, -1), (2, -1),
                    ],
            passivate=False,
        )
        atoms = molecule.atoms()
        atoms = triplicateEndGroup(atoms)
        atoms = molecule.passivate(atoms)
        atoms.rotate((0, 0, 1), np.pi/6)
        Molecule.__init__(self, atoms, name=name, parameters=parameters)
