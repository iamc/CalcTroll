# Written by Mads Engelund, 2017, http://espeem.com
import numpy
from numpy import matrix
from math import pi,sqrt
from ase.atoms import Atoms
from ase.constraints import FixAtoms

from CalcTrollASE.AtomUtilities import orderAtoms
from CalcTrollASE.AtomUtilities import removeCopies
from CalcTrollASE.AtomUtilities import wrap
from CalcTrollASE.AtomUtilities import findPeriodicallyEqualAtoms

#from CalcTroll.CoreSystems.CrystalUtilities import makeSample
#from CalcTroll.CoreSystems.CrystalUtilities import makeSlabWithVectors
#from CalcTroll.Utilities import convertToIntegers, convertToBasis
#from CalcTroll.Utilities import fitBoxIntoBox
#from CalcTroll.Utilities import getEquivalentPoints
#from CalcTroll.Utilities import makeLargeEnoughQuiteOrthogonalBox
#from CalcTroll.Utilities import vectorLength
from CalcTroll import API


def makeSlabWithVector(surface, vector, radius):
    v1 = numpy.zeros(3, dtype=numpy.int)
    v1[:2] = vector
    v1 = surface.change(v1, to='orth_surface', fro='surface')

    unit_cell = surface.unitCell('orth_surface')

    pro = [abs(numpy.dot(v, v1))/vectorLength(v) for v in unit_cell]
    v2 =  unit_cell[numpy.argmin(pro)]
    l1 = vectorLength(v1)
    pro = vectorLength(numpy.cross(v1/l1, v2))
    n2 = int(numpy.ceil(radius/pro))
    v2 *= n2

    v3 = surface.outOfPlaneVector('orth_surface')
    vectors = numpy.array([v1, v2, v3])
    vectors = surface.change(vectors, to='crystal', fro='orth_surface')

    layer_distance = surface.outOfPlaneVector('orth_surface')[2]
    n_layers = int(numpy.ceil(radius/layer_distance))

    atoms = makeSlabWithVectors(surface.crystal(), vectors, n_layers)
    surface.change(atoms, to='orth_surface', fro='orth_crystal')
    atoms.cell[2, :2] = 0
    atoms.set_pbc([True, True, False])
    atoms = wrap(atoms)

    # Make vaccum in the out-of-surface direction.
    z_pos = atoms.get_positions()[:, 2]
    z_min, z_max = min(z_pos), max(z_pos)
    z_diff = z_max - z_min
    tot_diff = radius + z_diff
    atoms.cell[2] = numpy.array([0,0,tot_diff])
    atoms = wrap(atoms)

    # Put atoms in order.
    atoms = orderAtoms(atoms)

    return atoms

def makeSlab(surface, pertubation_length, correlation_length):
    vectors = surface.cell(coordinates='orth_crystal')
    atoms = makeSample(surface.crystal(), vectors)
    surface.change(atoms, to='orth_surface', fro='orth_crystal')
    vectors = surface.cell(coordinates='orth_surface')

    vectors, repeats = makeLargeEnoughQuiteOrthogonalBox(vectors, pertubation_length)
    atoms = atoms.repeat(repeats)

    # Make repetitions to satisfy correlation length.
    n2 = int(numpy.ceil(correlation_length/vectors[2,2]))
    atoms = atoms.repeat((1,1,n2))

    # Wrap into box with atoms below zero in third direction.
    wrap_vectors = numpy.array([vectors[0], vectors[1], -n2*vectors[2]])
    atoms.set_cell(wrap_vectors)
    atoms = wrap(atoms)
    atoms.set_cell(vectors)
    atoms.set_pbc([True, True, False])

    # Crop number of layers.
    take = atoms.get_positions()[:, 2] > - correlation_length + 1e-5
    atoms = atoms[take]

    extra_atoms = surface.extraAtoms('orth_surface')
    repeats = fitBoxIntoBox(extra_atoms.get_cell()[:2], vectors[:2])
    repeats = list(repeats)
    repeats.append(1)
    extra_atoms = extra_atoms.repeat(tuple(repeats))
    extra_atoms.set_cell(vectors)
    extra_atoms = wrap(extra_atoms)
    extra_atoms = removeCopies(extra_atoms)
    atoms += extra_atoms

    # Passivate lower surface.
    positions = atoms.get_positions()
    z_min = min(positions[:,2])
    lower_positions = positions[(positions[:,2] < z_min + 1e-5)]
    h_positions = [position + [0,0,-0.8] for position in lower_positions]
    passivation = Atoms('H%d'%len(h_positions), h_positions)
    atoms += passivation

    # Make vaccum in the out-of-surface direction.
    z_pos = atoms.get_positions()[:, 2]
    z_min, z_max = min(z_pos), max(z_pos)
    z_diff = z_max - z_min
    tot_diff = correlation_length + z_diff
    atoms.cell[2] = numpy.array([0,0,tot_diff])
    atoms = wrap(atoms)

    # Put atoms in order.
    atoms = orderAtoms(atoms)

    # Fix lowest coordinates.
    constraint = FixAtoms(mask=(atoms.get_positions()[:, 2] < -pertubation_length + 1e-5))
    atoms.set_constraint(constraint)

    return atoms

def findOrthogonalBasisSet(unit_cell):
    v1,v2,v3 = unit_cell

    or_v1 = v1/numpy.sqrt(numpy.sum(v1**2))
    or_v3 = numpy.cross(v1,v2)
    or_v3 = or_v3/numpy.sqrt(numpy.sum(or_v3**2))
    or_v2 = numpy.cross(or_v3,or_v1)

    or_v1*= 1/numpy.sqrt(numpy.sum(or_v1**2))
    or_v2*= 1/numpy.sqrt(numpy.sum(or_v2**2))
    or_v3*= 1/numpy.sqrt(numpy.sum(or_v3**2))

    orth_vs = numpy.array([or_v1,or_v2,or_v3])

    return orth_vs

def iterate(a,b,e,es,W):
    g = (W-e)**(-1)

    an = a*g*a
    bn = b*g*b
    en = e+a*g*b+b*g*a
    esn = es+a*g*b
    return (an,bn,en,esn)

def make_kpoints(correlation_length,vectors):
    length = numpy.sqrt(numpy.sum(vectors**2,1))
    Ks = numpy.array(numpy.ceil(correlation_length/(length)),int)
    x = numpy.indices(Ks)
    if x.shape[0]==1:
        k_points = numpy.transpose((x+.5)*numpy.array(2*pi/Ks))
    else:
        x = (numpy.transpose(x,(1,2,0))+.5)*numpy.array(2*pi/Ks)
        k_points = numpy.reshape(x,(-1,2))
    return k_points

def get_layer_unit_cell_index(R,s_pos,lb):
    diff = (R-s_pos)
    diff[:,:lb] = (diff[:,:lb]+.5)%1-.5
    argmin = numpy.argmin(vectorLength(diff,1))
    diff = R-s_pos[argmin]
    diff = numpy.array(numpy.around(diff),int)
    layer = -diff[lb-1]
    l = layer*len(s_pos)+argmin
    return (diff[:(lb-1)],l)
