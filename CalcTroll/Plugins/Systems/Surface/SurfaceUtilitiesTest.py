# Written by Mads Engelund, 2017, http://espeem.com
import unittest
import numpy
from ase.atoms import Atoms

from CalcTroll.Plugins.Systems.Surface.Surface3D import Surface3D
from CalcTroll.Plugins.Systems.Surface.Utilities import findOrthogonalBasisSet

class SurfaceUtilitiesTest(unittest.TestCase):
    def setUp(self):
        self._crystal = Diamond()
        self._surface = Surface(crystal=self._crystal,
                                miller_indices=(0,0,1),
                                surface_cell=((2, 0), (0, 1)),
                                extra_atoms=Atoms('H', [[0, 0, 0.7]]),
                                )

    def testMakeSurface(self):
        crystal = Diamond(a=3.5)
        surface = makeSurface(crystal, (0, 0, 1))

    def testSurfaceSample(self):
        radius = 5
        vacuum = 10
        sample = makeSlab(self._surface, radius, vacuum)
        from ase.visualize import view
        view(sample)

if __name__=='__main__':
    unittest.main()
