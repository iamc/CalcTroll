# Written by Mads Engelund, 2017, http://espeem.com
import numpy as np
import new
from ase.atoms import Atoms
from ase.constraints import FixAtoms
from ase.visualize import view

from CalcTrollASE.Atoms import Atoms
from CalcTrollASE.AtomUtilities import orderAtoms
from CalcTrollASE.AtomUtilities import findIndices
from CalcTrollASE.AtomUtilities import groupAtoms
from CalcTrollASE.AtomUtilities import removeCopies
from CalcTrollASE.AtomUtilities import minimalDistance
from CalcTrollASE.AtomUtilities import makeAtomsRelaxed
from CalcTrollASE.AtomUtilities import expressAtomsInCoordinates
from CalcTrollASE.AtomUtilities import findPeriodicallyEqualAtoms
from CalcTrollASE.AtomUtilities import fixBetween
from CalcTroll.Core.Utilities import vectorLength
from CalcTroll.Core.System.BasisChanger import BasisChanger, Transformation
from CalcTroll.Plugins.Systems.Surface import Surface
from CalcTroll.Plugins.LengthScales import LengthScales
from CalcTroll import API
from CalcTroll.API import DEFAULT

class SurfaceParameters1D(API.Parameters):
    def __init__(self,
                 free_layers=API.DEFAULT,
                 bound_layers=2,
                 electrode_layers=0,
                 cell_size=API.DEFAULT,
                 length_scales=LengthScales(),
                 ):
        pass

    def kpts(self):
        return (1, 1, 1)

    def nLayers(self):
        return self['free_layers'] + self['bound_layers'] + self['electrode_layers']

    def identifier(self):
        l = []
        if self['free_layers'] is DEFAULT:
            raise Exception('Free layers not set')
        if not self.nLayers() is None:
            if self['free_layers'] is None:
                l.append(self.nLayers())
            else:
                l.append('%sof%s'%(self['free_layers'], self.nLayers()))

        for entry in [self['cell_size']]:
            if not entry is None:
                l.append(entry)

        return tuple(l)

class Surface1D(Surface):
    @classmethod
    def parameterClass(cls):
        return SurfaceParameters1D

    def __init__(self,
                 crystal,
                 direction=1,
                 extra_atoms=Atoms([]),
                 parameters=DEFAULT,
                 ):
        self.__direction = int(np.sign(direction))
        self.__extra_atoms = extra_atoms

        atoms = crystal.atomsTemplate()
        groups = groupAtoms(atoms)
        self.__inequivalent_layers = len(groups)

        Surface.__init__(
                self,
                sub_systems=[crystal],
                parameters=parameters,
                )

    def pbc(self):
        return (False, False, False)

    def inequivalentLayers(self):
        return self.__inequivalent_layers

    def extraAtoms(self, coordinates='orth_surface'):
        atoms = self.__extra_atoms.copy()
        cell = self.cell(coordinates='orth_surface')
        atoms.set_cell(cell)

        atoms.set_pbc([True, True, False])
        atoms = self.basisChanger().transform(atoms, to=coordinates, fro='orth_surface')

        if self.isRelaxed():
            atoms = self.makeAtomsRelaxed(atoms)

        return atoms

    def crystalAtoms(self,
                     start_layer=0,
                     end_layer=1,
                     coordinates='orth_surface',
                     sub_layers=False,
                     minimal=True,
                     ):
        atoms = self.crystal().atoms()

        if sub_layers:
            in_equal_layers = self.inequivalentLayers()
            repeats = int(np.ceil(end_layer/float(in_equal_layers)))
            atoms = atoms.repeat((1, 1, repeats + 1))
            atoms.positions -= self.crystal().unitCell()*(repeats)
            start_layer += in_equal_layers -1
            end_layer += in_equal_layers -1
            atoms = orderAtoms(atoms, (2, 1, 0))
            sl = slice(start_layer, end_layer)

            groups = groupAtoms(atoms)[::-1][sl]
            if len(groups) == 0:
                atoms = Atoms([], cell=atoms.get_cell(), pbc=atoms.get_pbc())
            else:
                atoms, indices = groups[0]
                for group, indices in groups[1:]:
                    atoms += group
        else:
            cell = atoms.unitCell()
            atoms = atoms.repeat((1, 1, end_layer-start_layer))
            atoms.positions += cell*start_layer

        return atoms

    def atomsTemplate(self, parameters):
        unrelaxed = self.fullyUnrelaxed()

        # Add outer surface layer.
        extra_atoms = unrelaxed.surfaceLayer(parameters)
        atoms = unrelaxed.crystalAtoms(
                start_layer=0,
                end_layer=parameters.nLayers(),
                sub_layers=True,
                )
        atoms += extra_atoms

        pos = atoms.positions
        size = np.array([pos[:, i].max() - pos[:,i].min() for i in range(3)])
        atoms.cell[:2,:2] = extra_atoms.cell[:2, :2]
        atoms = orderAtoms(atoms)

        before = len(atoms)
        atoms = unrelaxed.passivateLowerSurface(atoms)
        after = len(atoms)
        n_passivated = after - before

        fro, to = parameters['free_layers'], parameters.nLayers()
        constrain_mask = unrelaxed.findLayers(atoms, start_layer=fro, end_layer=to)

        if parameters['electrode_layers'] > 0:
            fro, to = parameters.nLayers() - parameters['electrode_layers'], parameters.nLayers()
            electrode_mask = unrelaxed.findLayers(atoms, start_layer=fro, end_layer=to)
            electrode_cell = unrelaxed.electrodeCell(parameters['electrode_layers'], atoms.get_cell()[:2])
            central_mask = np.logical_not(electrode_mask)
        else:
            electrode_mask = np.zeros(len(atoms), int)
            electrode_cell = np.identity(3)
            central_mask = np.ones(len(atoms), int)

        # Make vaccum in the out-of-surface direction.
        atoms.set_cell(np.array(parameters['cell_size']) + size)

        if electrode_mask.sum() > 0:
            electrode_cell[:2] = atoms.cell[:2]
            atoms.setRegion(
                name=self.crystal().name(),
                picker=electrode_mask,
                cell=electrode_cell,
                pbc=(False, False, (False, True)),
                )

        atoms.setRegion(name=self.name(),
                          picker=central_mask,
                          cell=atoms.get_cell(),
                          pbc=(False, False, False),
                          )

        constraints = []

        constraint = FixAtoms(mask=constrain_mask)
        atoms.set_constraint(constraint)

        cell_center = atoms.cell.sum(axis=0)/2
        atoms_center = (atoms.positions.max(axis=0) + atoms.positions.min(axis=0))/2
        move = cell_center - atoms_center
        atoms.set_celldisp(-move)

        return atoms

    def findLayers(self,
            atoms,
            start_layer=0,
            end_layer=1,
            ):
        c_atoms = self.crystalAtoms(
                start_layer=start_layer,
                end_layer=end_layer,
                sub_layers=True,
                )

        if len(c_atoms) == 0:
            mask = np.zeros(len(atoms), bool)
        else:
            zmin, zmax = c_atoms.positions[:,2].min(), c_atoms.positions[:,2].max()
            mask1 = atoms.positions[:, 2] > zmin - 1e-5
            mask2 = atoms.positions[:, 2] < zmax + 1e-5
            mask = np.logical_and(mask1, mask2)

        return mask

    def makeAtomsRelaxed(self, atoms, relaxation):
        atoms = atoms.copy()
        atoms = self.fullyUnrelaxed().change(atoms, fro='orth_surface', to='orth_crystal')
        atoms = self.crystal().makeAtomsRelaxed(atoms, self.crystal().relaxation())
        atoms = self.change(atoms, to='orth_surface', fro='orth_crystal')
        atoms = makeAtomsRelaxed(atoms, self.relaxation(), tolerance=1.5)

        return atoms

    def cell(self, coordinates='orth_surface'):
        cell = np.identity(3)*10
        cell[2] = self.change(self.crystal().unitCell(), to=coordinates, fro='orth_crystal')

        return cell

    def surfaceLayer(self, parameters, coordinates='orth_surface'):
        atoms = self.extraAtoms(coordinates)
        atoms.set_pbc([False, False, False])

        return atoms

    def defaultParameters(
            self,
            parameters=DEFAULT,
            centers=((0,0,0),),
            ):
        if parameters is DEFAULT:
            parameters = SurfaceParameters1D()
        elif isinstance(parameters, dict):
            parameters = SurfaceParameters1D(**parameters)

        scales = parameters['length_scales']
        short_length = scales['short_correlation_length']
        depth = scales.depth()

        in_equal_layers = self.inequivalentLayers()
        electrode_layers = parameters['electrode_layers']
        bound_layers = parameters['bound_layers']
        free_layers = parameters['free_layers']
        if bound_layers is DEFAULT:
            bound_layers = 2
        if electrode_layers is DEFAULT:
            electrode_layers = 0

        size = vectorLength(self.cell('orth_surface')[0])
        if free_layers is DEFAULT:
            n_layers = int(np.ceil(in_equal_layers*depth/size))
            free_layers = n_layers - bound_layers - electrode_layers

        cell_size = parameters['cell_size']

        if cell_size is DEFAULT:
            c_length = parameters['length_scales']['correlation_length']
            cell_size = (c_length, c_length, c_length)

        parameters = parameters.copy(
                free_layers=free_layers,
                bound_layers=bound_layers,
                electrode_layers=electrode_layers,
                cell_size=cell_size,
                )

        return parameters

    def setUpBasisChanger(self):
        crystal = self.crystal()
        extra_atoms = self.__extra_atoms
        if len(extra_atoms) == 0:
            top = 0
        else:
            top = np.max(extra_atoms.get_positions()[:, 2])
        shift = [0, 0 , top]

        trans = np.identity(3)
        trans[2] =  crystal.unitCell()

        transformations = [
            Transformation(to='orth_crystal', fro='crystal', trans_matrix=trans.transpose()),
            Transformation(to='from_top', fro='orth_surface', rotation_point=[[0,0,0], shift]),
            Transformation(to='orth_crystal',
                           fro='orth_surface',
                           trans_matrix=np.identity(3)),
            ]
        basis_changer = BasisChanger(transformations=transformations)

        return basis_changer

    def electrodeCell(self, electrode_layers, vectors):
        n = self.inequivalentLayers()
        n, remain  = divmod(electrode_layers, n)
        assert remain == 0
        cell = self.cell()
        cell[2] *= n

        return cell
