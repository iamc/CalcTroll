# Written by Mads Engelund, 2017, http://espeem.com
import numpy as np
import new
from ase.atoms import Atoms
from ase.constraints import FixAtoms
from ase.visualize import view

from CalcTroll.Core.Utilities import vectorLength
from CalcTroll.Plugins.LengthScales import LengthScales
from CalcTroll import API
from CalcTroll.API import DEFAULT

class Surface(API.System):
    COORDINATES='orth_surface'

    def crystal(self):
        return self.subSystems()[0]

    def passivateLowerSurface(self, atoms):
        return atoms

    def subParameters(self, parameters):
        return (DEFAULT,)

def addFullCellTransformation(parameters, basis_changer):
    vectors = np.array(parameters['vectors'])
    full_cell = np.zeros((3,3), dtype=np.float)
    full_cell[:2,:2] = basis_changer.transform(
        vectors,
        fro='surface',
        to='orth_surface',
        )
    full_cell[2,2] = parameters['cell_size']
    t = Transformation(
            to='orth_surface',
            fro='fractional_full_cell',
            trans_matrix=full_cell.transpose(),
            )
    basis_changer.addTransformation(t)

def setCellAndPBC(atoms, periodicity):
    dim = len(periodicity)
    if dim == 0:
        pass
    elif dim == 1:
        v0 = np.dot(periodicity, atoms.cell[:2])[0]
        pro = [vectorLength(np.cross(v0, v)) for v in atoms.cell[:2]]
        v1 = atoms.cell[np.argmax(pro)]
        cell = np.array([v0, v1, atoms.cell[2]])
        atoms.set_cell(cell)
    atoms.set_pbc([True]*dim + [False]*(3-dim))

