# Written by Mads Engelund, 2017, http://espeem.com
# Defines a molecule/cluster system - e.i a 0d collection of atoms.
import numpy as np
from ase.constraints import FixAtoms, ModelSurfacePotential

from CalcTrollASE.AtomUtilities import cellCenter
from CalcTrollASE.Atoms import Atoms
from CalcTroll.Plugins.LengthScales import LengthScales
from CalcTroll import API
from CalcTroll.API import DEFAULT
from CalcTroll.Core.Visualizations import addMeasuringStick
from CalcTroll.Core.Visualizations import flatMatplotlibView

padding = LengthScales.defaults()['correlation_length']

class MoleculeParameters(API.Parameters):
    def __init__(self, padding=DEFAULT):
        if padding is DEFAULT:
            padding = LengthScales.defaults()['correlation_length']
        self.__padding = padding

    def ignoreLongRangeCorrelation(self):
        if self['padding'] is DEFAULT:
            padding = LengthScales.defaults()['short_correlation_length']
            return self.copy(padding=padding)
        else:
            return self

    def padding(self):
        return self.__padding

    def kpts(self):
        return (1,1,1)

    def identifier(self):
        return self.padding()

class Molecule(API.System):
    def parameterClass(cls):
        return MoleculeParameters

    def __len__(self):
        return len(self.__atoms)

    def defaultParameters(self, parameters=DEFAULT):
        if parameters is DEFAULT:
            result = MoleculeParameters()
        else:
            assert isinstance(parameters, MoleculeParameters)
            result = parameters

        return result

    def __init__(
            self,
            atoms,
            potential_strength=1,
            name=DEFAULT,
            parameters=DEFAULT):
        if not isinstance(atoms, Atoms):
            atoms = Atoms(atoms)

        self.__strength = potential_strength
        self.__atoms = atoms.copy()
        API.System.__init__(
                self,
                sub_systems=tuple(),
                name=name,
                parameters=parameters,
                )

    def atoms(
            self,
            constrained=True,
            parameters=DEFAULT,
            ):
        if parameters is DEFAULT:
            parameters = self.parameters()
        parameters = self.defaultParameters(parameters)

        atoms = self.__atoms.copy()
        maxi = atoms.positions.max(axis=0)
        mini = atoms.positions.min(axis=0)

        ran = maxi - mini + parameters.padding()
        atoms.cell[0, 0] = ran[0]
        atoms.cell[1, 1] = ran[1]
        atoms.cell[2, 2] = ran[2]
        atoms = self.makeAtomsRelaxed(atoms)

        cell_center = cellCenter(atoms)
        middle = (mini + maxi)/2
        cell_displacement = - (cell_center - middle)
        atoms.set_celldisp(cell_displacement)

        # Assume polarized molecule to break spin-symmetry.
        atoms.set_initial_magnetic_moments([1]*len(atoms))

        if not constrained:
            del atoms.constraints
        if constrained:
            if len(atoms.constraints) == 0:
                atoms.set_constraint(FixAtoms(mask=np.zeros(len(atoms))))
        else:
            constraint = ModelSurfacePotential(strength=self.__strength)
            atoms.set_constraint(constraint)

        return atoms

    def pbc(self):
        return [False, False, False]

    def makeAtomsRelaxed(
            self,
            atoms,
            tolerance=1.0,
        ):
        if self.isRelaxed():
            atoms.positions = self.relaxation().positions

        return atoms

    def plot(self,
             data=None,
             ax=None,
             frame=None,
             direction='-z',
             size=1.,
             ):
        if data is None:
            atoms = self.atoms()
        else:
            atoms = data

        size = size*0.4
        flatMatplotlibView(atoms, ax, direction=direction, size=size)
        if frame is not None:
            ax.set_xlim((frame[0, 0], frame[1, 0]))
            ax.set_ylim((frame[0, 1], frame[1, 1]))
            addMeasuringStick(ax, color='white', size=5)

        ax.patch.set_facecolor('black')
        ax.set_xticks([])
        ax.set_yticks([])

    def frame(self, data=None, border=0.3, direction='-z'):
        if data is None:
            atoms = self.atoms()
        else:
            atoms = data


        pmin = atoms.positions.min(axis=0)
        pmax = atoms.positions.max(axis=0)

        pmin -= border
        pmax += border

        fc, sc = {'x':(1, 2), 'y':(0,2), 'z':(0, 1)}[direction[-1]]

        return np.array([pmin[[fc, sc]], pmax[[fc, sc]]])

    def jpg_save(
            self,
            filename,
            data=None,
            frame=None,
            border=5,
            direction='-x',
            **kwargs):
        from matplotlib import pylab as plt
        if data is None:
            data = self.read()
        if frame is None:
            frame = self.frame(
                data=data,
                border=border,
                direction=direction,
            )
        frame = np.array(frame)

        pmin, pmax = frame
        d = pmax - pmin
        dx = d[0]
        dy = d[1]
        ratio = dx/dy

        fig = plt.figure(frameon=False, figsize=(4*ratio, 4))
        fig.patch.set_facecolor('black')

        ax = plt.Axes(fig, [0., 0., 1., 1.])
        fig.add_axes(ax)

        self.plot(data=data,
                  ax=ax,
                  frame=frame,
                  direction=direction,
                  **kwargs)

        ax.set_xlim((pmin[0], pmax[0]))
        ax.set_ylim((pmin[1], pmax[1]))
        plt.savefig(filename)
        plt.close('all')

        return frame
