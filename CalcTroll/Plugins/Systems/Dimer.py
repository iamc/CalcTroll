from ase.atoms import Atom
from ase.atoms import Atoms as ASEAtoms
import numpy as np
from ase.visualize import view
from ase.constraints import Hookean

from CalcTrollASE.Atoms import Atoms
from CalcTrollASE.AtomUtilities import removeCopies
from CalcTroll.Plugins.Systems.Molecule import Molecule

from CalcTroll import API
from CalcTroll.API import DEFAULT

from PolyaromaticHydroCarbon import PolyaromaticHydroCarbon, CarbonRingChain, CarbonRingChain2
from ConjugatedMolecules import triplicateEndGroup

class Dimer(Molecule):
    def name(self):
        return self.symbol() + '2' + 'Dimer'

    def symbol(self):
        return self.__symbol

    def __init__(
            self,
            symbol='H',
            parameters=DEFAULT,
    ):
        self.__symbol = symbol
        atoms = Atoms(symbol+'2', positions=[[1, 0, 0], [-1, 0, 0]])

        Molecule.__init__(
            self,
            atoms,
            parameters=parameters,
        )
