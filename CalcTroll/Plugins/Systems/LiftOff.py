# Written by Mads Engelund, 2017, http://espeem.com
import numpy as np
from ase.constraints import FixAtoms

from CalcTroll.Core.Utilities import vectorLength
from CalcTrollASE.AtomUtilities import orderAtoms
from CalcTrollASE.AtomUtilities import groupAtoms
from CalcTrollASE.AtomUtilities import makeAtomsRelaxed
from CalcTroll.Plugins.Systems.Surface.Surface1D import SurfaceParameters1D
from CalcTroll.Plugins.Systems.Surface.Surface3D import SurfaceParameters3D
from CalcTroll.Core.System.BasisChanger import BasisChanger, Transformation
from CalcTroll import API
from CalcTroll.API import view
from CalcTroll.API import DEFAULT

def circleTransform(atoms, curvature=10, curve=5, long_direction=0, curve_direction=2):
    atoms = atoms.copy()
    p = atoms.get_positions()
    maxi, mini = p[:, long_direction].max(), p[:, long_direction].min()
    zmin = p[:, curve_direction].min()

    t = -p[:, long_direction]
    x = np.zeros(len(p))
    y = np.zeros(len(p))
    take = t < curve

    phi = t[take]/curvature
    x[take] = curvature*np.sin(phi)
    y[take] = curvature*(1-np.cos(phi)) + zmin

    take = np.logical_not(take)
    last_angle = float(curve)/curvature

    last_x = curvature*np.sin(last_angle)
    last_y = curvature*(1-np.cos(last_angle)) + zmin
    length = np.sqrt(last_x**2 + last_y**2)

    x[take] = last_x + np.cos(last_angle)*(t[take] - curve)
    y[take] = last_y + np.sin(last_angle)*(t[take] - curve)

    p[:, long_direction] = x
    p[:, curve_direction] = y

    atoms.positions = p
    trans_matrix = np.identity(3)
    trans_matrix[long_direction, long_direction] = np.cos(last_angle)
    trans_matrix[curve_direction, curve_direction] = np.cos(last_angle)
    trans_matrix[curve_direction, long_direction] = np.sin(last_angle)
    trans_matrix[long_direction, curve_direction] = -np.sin(last_angle)

    for electrode in atoms.electrodes():
        cell = np.tensordot(trans_matrix, electrode.cell().transpose(), (1, 0)).transpose()
        electrode.setCell(cell)

    return atoms

class OnSlabParameters(API.Parameters):
    def __init__(
            self,
            on_parameters=DEFAULT,
            slab_parameters=DEFAULT,
            ):
        pass

    def kpts(self):
        return self['slab_parameters'].kpts()

    def identifier(self):
        return self['slab_parameters'].identifier()

class LiftOff(API.System):
    def name(self):
        return 'LiftOff' + self.polymer().name() + self.slab().name()

    def __init__(self,
            polymer,
            slab,
            height=2,
            curvature=13,
            curve_length=15,
            parameters=DEFAULT,
            ):
        self.__height = height
        self.__curvature = curvature
        self.__curve_length = curve_length

        API.System.__init__(self, [polymer, slab], parameters=parameters)

    @classmethod
    def parameterClass(cls):
        return OnSlabParameters

    def polymer(self):
        return self.subSystems()[0]

    def slab(self):
        return self.subSystems()[1]

    def pbc(self):
        return (True, True, False)

    def defaultParameters(self, parameters=DEFAULT):
        polymer = self.polymer().fullyUnrelaxed()
        slab = self.slab().fullyUnrelaxed()

        if parameters is DEFAULT:
            on_parameters = DEFAULT
            slab_parameters = DEFAULT
        elif isinstance(parameters, SurfaceParameters3D):
            on_parameters = DEFAULT
            slab_parameters = parameters
        else:
            on_parameters = parameters['on_parameters']
            slab_parameters = parameters['slab_parameters']

        if on_parameters is DEFAULT:
            n_cell = int(np.ceil(6/vectorLength(polymer.cell()[-1])))
            n_layers = n_cell*polymer.inequivalentLayers()
            on_parameters = polymer.parameterClass()(
                    free_layers=n_layers*2,
                    bound_layers=0,
                    electrode_layers=n_layers,
                    )
        elif isinstance(on_parameters, dict):
            on_parameters = polymer.parameterClass()(**on_parameters)

        if isinstance(slab_parameters, dict):
            slab_parameters = slab.parameterClass()(**slab_parameters)

        centers = polymer.atoms().get_positions()
        on_parameters = polymer.defaultParameters(parameters=on_parameters)
        slab_parameters = slab.defaultParameters(parameters=slab_parameters, centers=centers)

        return OnSlabParameters(
                on_parameters=on_parameters,
                slab_parameters=slab_parameters,
                )

    def subParameters(self, parameters=DEFAULT):
        parameters = self.defaultParameters(parameters=parameters)

        return parameters['on_parameters'], parameters['slab_parameters']

    def setUpBasisChanger(self):
        trans_matrix = np.matrix([[0, 1, 0], [0, 0, 1], [1, 0, 0]])
        transformation= Transformation(
                to='substrate',
                fro='polymer',
                trans_matrix=trans_matrix,
                rotation_point=[[0, 0, self.__height], [0, 0, 0]],
                )
        basis_changer = BasisChanger(transformations=[transformation])

        return basis_changer

    def atomsTemplate(
            self,
            parameters,
            ):
        unrelaxed = self.fullyUnrelaxed()
        p_atoms = unrelaxed.polymer().atomsTemplate(
                parameters=parameters['on_parameters'],
                )
        p_atoms = unrelaxed.change(p_atoms, to='substrate', fro='polymer')

        cell = p_atoms.electrodes()[0].cell()
        cell = np.array([cell[1], cell[0], cell[2]])
        cell[0, 0] -= 20
        cell[1, 2] -= 20
        p_atoms.electrodes()[0].setCell(cell)
        p_atoms = circleTransform(
                p_atoms,
                curvature=self.__curvature,
                curve=self.__curve_length,
                long_direction=1,
                curve_direction=2
                )
        p_atoms.electrodes()[-1].setCellDisp(None)

        pos = p_atoms.positions
        w = np.exp(-pos[:,2]/2)
        center = (np.transpose(pos)*w).sum(axis=1)/w.sum()

        slab = unrelaxed.slab()
        slab_parameters = parameters['slab_parameters']
        atoms = slab.atomsTemplate(parameters=slab_parameters)
        vectors = np.array(slab_parameters['vectors'])
        vectors[1, 1] = 2
        e_parameters = slab_parameters.copy(vectors=vectors, cell_size=20)
        e_atoms = slab.atomsTemplate(parameters=e_parameters)
        e_atoms.center()
        e_cell = e_atoms.get_cell()

        atoms = orderAtoms(atoms, (1, 0, 2))

        atoms.setRegion(
                name='Left',
                picker=slice(0, len(e_atoms)),
                cell=e_atoms.get_cell(),
                pbc=(True, (True, False), False),
                celldisp=None,
                )

        atoms.setRegion(
                name='Right',
                picker=slice(len(atoms) - len(e_atoms), len(atoms)),
                cell=e_atoms.get_cell(),
                pbc=(True, (False, True), False),
                celldisp=None,
                )
        atoms += p_atoms

        atoms.electrodes()[0].setCellDisp(None)
        atoms.electrodes()[1].setCellDisp(None)
        atoms.electrodes()[2].setCellDisp(None)

        p_max = atoms.positions.max(axis=0)
        p_min = atoms.positions.min(axis=0)
        center = (p_max + p_min)/2
        cell_center = atoms.cell.sum(axis=0)/2
        origin = cell_center - center
        atoms.set_celldisp(-origin)

        return atoms

    def makeSubsystemsRelaxed(self, atoms, tolerance=1.0):
        p_ratoms = self.polymer().relaxation()
        if p_ratoms is not None:
            p_ratoms = self.change(p_ratoms, to='substrate', fro='polymer')
            p_ratoms = circleTransform(
                    p_ratoms,
                    curvature=self.__curvature,
                    curve=self.__curve_length,
                    long_direction=1,
                    curve_direction=2
                    )
            p_atoms = atoms[-len(p_ratoms):]
            p_atoms = makeAtomsRelaxed(p_atoms, p_ratoms)
            p_cell = atoms.electrodes()[2].cell()
            p_cell[1:,1:] *= self.polymer().crystal().relaxationFactor()
            atoms.positions[-len(p_ratoms):] = p_atoms.positions
            atoms.electrodes()[2].setCell(p_cell)

        atoms = self.slab().makeAtomsRelaxed(atoms)

        return atoms
