# Written by Mads Engelund, 2019, http://espeem.com
import unittest
import numpy
from ase.visualize import view
from CalcTroll.Core.Test.Case import CalcTrollTestCase

from Phthalocyanine import *

class PhthalocyanineTest(CalcTrollTestCase):

    def testConstruction(self):
        molecule = Phthalocyanine()
        self.assertEqual(len(molecule), 57)

    def testConstructionNone(self):
        molecule = Phthalocyanine(None)
        self.assertEqual(len(molecule), 56)


if __name__=='__main__':
    unittest.main()
