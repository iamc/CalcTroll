# Written by Mads Engelund, 2017, http://espeem.com
import unittest
from ase.constraints import FixAtoms
from ase.visualize import view

from CalcTroll.Core.System import InitialStateAtom, InitialState
from CalcTroll.Core.Test.Dummies import TestRelaxationSystem
from CalcTroll.Core.Test.Case import CalcTrollTestCase
from CalcTrollASE.Atoms import Atoms

from CalcTroll.Plugins.Systems.Surface.Surface3D import SurfaceParameters3D
from Diamond001 import Si001
from DanglingBonds import Diamond001Unpassivated
from CalcTroll import API

class Diamond001Test(CalcTrollTestCase):
    def testRelaxation(self):
        surface = Si001
        surface_parameters1 = surface.defaultParameters().copy(
                free_layers=0,
                bound_layers=1,
                vectors=((2, 0),(0, 2)),
                cell_size=50,
                )
        surface_parameters2 = surface_parameters1.copy(
                free_layers=0,
                bound_layers=1,
                vectors=((2, 0),(0, 4)),
                cell_size=50,
                )
        atoms = surface.atoms(parameters=surface_parameters1)
        crystal = surface.crystal()
        atoms = crystal.atoms()
        atoms.cell *= 1.1
        relaxation = atoms
        rcrystal = TestRelaxationSystem(crystal, relaxation)
        atoms = surface.atoms(parameters=surface_parameters1)
        surface = surface.setSubSystems([rcrystal])
        self.assertTrue(surface.hasRelaxedSubsystems())
        self.assertFalse(surface.isRelaxed())

        atoms = surface.atoms(parameters=surface_parameters1)
        atoms.rattle(0.1)
        surface = TestRelaxationSystem(surface, atoms)

        atoms2 = surface.atoms(parameters=surface_parameters1)
        self.assertAtomsEqual(atoms, atoms2)
        atoms3 = surface.atoms(parameters=surface_parameters2)
        expected = [[-7.19151, -8.43756, -1.13246],
                    [-5.49834, -8.50872, -1.04653],
                    [-2.97292, -8.26343, -1.01871],
                    [-1.41735, -8.3664 , -1.13945],
                    [1.33246 , -8.64462, -1.15018],
                    [2.93243 , -8.37481, -1.00023],
                    [5.52434 , -8.47876, -1.16522],
                    [7.06509 , -8.49472, -0.91165],
                    [-7.0874 , -4.23815, -0.9526 ],
                    [-5.3836 , -4.24774, -1.04078],
                    [-2.75483, -4.14758, -1.06431],
                    [-1.25732, -4.27067, -1.06394],
                    [1.33577 , -4.41565, -1.18986],
                    [2.85652 , -4.32561, -0.98594],
                    [5.4451  , -4.36556, -0.8708 ],
                    [7.1145  , -4.21757, -1.15984],
                    [-7.19151, 0.01109 , -1.13246],
                    [-5.49834, -0.06006, -1.04653],
                    [-2.97292, 0.18523 , -1.01871],
                    [-1.41735, 0.08225 , -1.13945],
                    [1.33246 , -0.19597, -1.15018],
                    [2.93243 , 0.07385 , -1.00023],
                    [5.52434 , -0.03011, -1.16522],
                    [7.06509 , -0.04606, -0.91165],
                    [-7.0874 , 4.2105  , -0.9526 ],
                    [-5.3836 , 4.20091 , -1.04078],
                    [-2.75483, 4.30107 , -1.06431],
                    [-1.25732, 4.17798 , -1.06394],
                    [1.33577 , 4.033   , -1.18986],
                    [2.85652 , 4.12304 , -0.98594],
                    [5.4451  , 4.0831  , -0.8708 ],
                    [7.1145  , 4.23108 , -1.15984],
                    [-8.44865, -8.44865, 0.0     ],
                    [-4.22433, -8.44865, 0.0     ],
                    [-0.0    , -8.44865, 0.0     ],
                    [4.22433 , -8.44865, 0.0     ],
                    [-8.44865, -4.22433, 0.0     ],
                    [-4.22433, -4.22433, 0.0     ],
                    [-0.0    , -4.22433, 0.0     ],
                    [4.22433 , -4.22433, 0.0     ],
                    [-8.44865, -0.0    , 0.0     ],
                    [-4.22433, -0.0    , 0.0     ],
                    [0.0     , 0.0     , 0.0     ],
                    [4.22433 , 0.0     , 0.0     ],
                    [-8.44865, 4.22433 , 0.0     ],
                    [-4.22433, 4.22433 , 0.0     ],
                    [0.0     , 4.22433 , 0.0     ],
                    [4.22433 , 4.22433 , 0.0     ],
                    [-7.75494, -6.38667, 1.44454 ],
                    [-4.58347, -7.14388, 1.40908 ],
                    [1.36746 , -6.99404, 1.28754 ],
                    [3.417   , -6.3757 , 1.20665 ],
                    [-7.60698, -1.47284, 1.09578 ],
                    [-4.9167 , -2.10346, 1.3231  ],
                    [0.78374 , -2.31092, 1.33103 ],
                    [3.48547 , -1.96437, 1.30117 ],
                    [-7.75494, 2.06199 , 1.44454 ],
                    [-4.58347, 1.30477 , 1.40908 ],
                    [1.36746 , 1.45461 , 1.28754 ],
                    [3.417   , 2.07295 , 1.20665 ],
                    [-7.60698, 6.97582 , 1.09578 ],
                    [-4.9167 , 6.34519 , 1.3231  ],
                    [0.78374 , 6.13773 , 1.33103 ],
                    [3.48547 , 6.48428 , 1.30117 ],
                    [-8.25223, -6.34393, 2.73192 ],
                    [-4.39764, -6.33047, 3.17012 ],
                    [0.15143 , -6.30634, 2.92033 ],
                    [3.9368  , -6.22221, 2.99899 ],
                    [-8.24838, -2.08606, 2.92431 ],
                    [-4.41845, -2.2537 , 2.88174 ],
                    [0.13639 , -2.19239, 2.90767 ],
                    [4.09407 , -1.92354, 2.94126 ],
                    [-8.25223, 2.10472 , 2.73192 ],
                    [-4.39764, 2.11819 , 3.17012 ],
                    [0.15143 , 2.14232 , 2.92033 ],
                    [3.9368  , 2.22645 , 2.99899 ],
                    [-8.24838, 6.3626  , 2.92431 ],
                    [-4.41845, 6.19495 , 2.88174 ],
                    [0.13639 , 6.25626 , 2.90767 ],
                    [4.09407 , 6.52511 , 2.94126 ]]

        self.assertArraysEqual(expected, atoms3.positions)


if __name__=='__main__':
    unittest.main()
