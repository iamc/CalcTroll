# Written by Mads Engelund, 2017, http://espeem.com
import numpy as np
import math

from CalcTroll.Core.Utilities import vectorLength
from CalcTrollASE.Atoms import Atoms
from CalcTroll.Plugins.Systems.Surface.Surface3D import Surface3D
from CalcTroll.Plugins.Systems.Crystal import DiamondStructure
from CalcTroll import API
from CalcTroll.API import DEFAULT

def passivateLowerSurface(atoms, direction, length=1, angle=90):
    s_direc = np.sign(direction)
    a_direc = abs(direction) - 1
    max_or_min = {-1: min, 1:max}[s_direc]

    atoms = atoms.copy()
    angle = angle*(math.pi/180)
    positions = atoms.get_positions()
    z_m = max_or_min(positions[:,a_direc])
    lower_positions = positions[s_direc*positions[:,a_direc] > s_direc*(z_m - s_direc*0.5)]
    lower_positions[:,2] = lower_positions[:,2].min()
    upper_positions = positions[s_direc*positions[:,a_direc] < s_direc*(z_m - s_direc*0.5)]

    diff = upper_positions - lower_positions[0]
    l = vectorLength(diff, 1)
    not_direction = np.array([i for i in range(3) if i!=a_direc], int)
    closest = abs(diff[np.argmin(l)][not_direction])

    if abs(closest[0]) > abs(closest[1]):
        h_positions = [position + [0, -length*math.cos(angle), s_direc*length*math.sin(angle)] for position in lower_positions]
        h_positions += [position + [0, length*math.cos(angle), s_direc*length*math.sin(angle)] for position in lower_positions]
    else:
        h_positions = [position + [-length*math.cos(angle), 0, s_direc*length*math.sin(angle)] for position in lower_positions]
        h_positions += [position + [length*math.cos(angle), 0, s_direc*length*math.sin(angle)] for position in lower_positions]

    new_atoms = Atoms('H%d'%len(h_positions),
                      h_positions,
                      cell=atoms.get_cell(),
                      pbc=atoms.get_pbc(),
                      )
    new_atoms += atoms

    return new_atoms

def fixDoping(symbol, crystal, doping, atoms):
    mask = [e==symbol for e in atoms.get_chemical_symbols()]
    if isinstance(doping, AtomicDoping):
        suggestion = doping.dopantElement()
    else:
        suggestion = None

    run.write()
    element = crystal.dopants(doping.level(), suggestion=suggestion)
    if isinstance(doping, ExplicitDoping):
        n_dopants=abs(int(np.around(doping.level())))
        atoms= substitutionalDopingOfMiddleLayer(
                atoms,
                n_dopants=n_dopants,
                dopant_element=element)
    elif isinstance(doping, VCADoping):
        doping.fix(mask, symbol, element)

    return atoms

class Diamond001(Surface3D):
    PASSIVATION = {
            'Ge':{'length':1.550, 'angle':37.2},
            'Si':{'length':1.509, 'angle':37.8},
                   }
    SURFACE_RECONSTRUCTION = {
            'Ge': (2.549, 1.287 ,1.485 , 0.521),
            'Si': (2.432, 1.230 ,1.428 , 0.549)
            }

    def name(self):
        return self.symbol() + '001'

    def __init__(
            self,
            symbol,
            parameters=DEFAULT,
            ):
        crystal = DiamondStructure(symbol=symbol)
        surface = Surface3D(
                crystal=crystal,
                miller_indices=(0, 0, 1),
                surface_cell=[[2,0], [0,1]],
                )

        x_diff, height, h_height, h_add = self.SURFACE_RECONSTRUCTION[symbol]
        a_diff = surface.change(np.array([x_diff, 0, 0]), to='minimal_unit_cell', fro='orth_surface')[0]

        positions = np.array([[-a_diff/2.0, 0, 1], [a_diff/2.0, 0.0, 1]])
        positions = surface.change(positions, fro='minimal_unit_cell', to='orth_surface')
        positions[:, 2] = height
        h_positions = positions + np.array([[-h_add, 0, h_height], [h_add, 0, h_height]])
        positions = np.array(list(positions) + list(h_positions))
        positions = surface.change(positions, to='minimal_unit_cell', fro='orth_surface')
        extra_atoms = Atoms(crystal['symbol'] +'2H2', positions)

        Surface3D.__init__(self,
                           crystal=surface['crystal'],
                           miller_indices=surface['miller_indices'],
                           surface_cell=surface['surface_cell'],
                           extra_atoms=extra_atoms,
                           parameters=parameters,
                           )

    def symbol(self):
        return self.crystal().symbol()

    def fixDoping(self, doping, atoms):
        return fixDoping('Ge', self.crystal(), doping, atoms)

    def passivateLowerSurface(self, atoms):
        passivation = self.PASSIVATION[self.symbol()]
        return passivateLowerSurface(atoms, direction=-3, **passivation)

    def directionLenghtMultipliers(self):
        return np.array((1.0, 1.5))

Ge001 = Diamond001(symbol='Ge')
Si001 = Diamond001(symbol='Si')
