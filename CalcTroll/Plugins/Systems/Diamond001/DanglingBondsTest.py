# Written by Mads Engelund, 2017, http://espeem.com
import unittest
from CalcTroll.Core.Test.Case import CalcTrollTestCase
from CalcTroll.Core.Test.Dummies import TestRelaxationSystem
from ase.constraints import FixAtoms
from ase.visualize import view

from CalcTroll.Plugins.Systems.Defect.SurfacePointDefect import SurfacePointDefect
from CalcTroll.Plugins.Systems.Diamond001 import Ge001
from DanglingBonds import *

class DanglingBondsTest(CalcTrollTestCase):
    def testLineDefectConstruction(self):
        identifiers=[(2, (0, 0)), (3, (0, 0)), (-1, (0, 1))]
        periodicity = [[0, 2]]
        surface = Ge001
        defect = SurfaceLineDefect(surface=surface, identifiers=identifiers, periodicity=periodicity)
        pos1 = defect.removedAtoms(coordinates='orth_surface').get_positions().mean(0)
        pos = [pos1 + np.array([3,0,1]), pos1 + np.array([-3,0,1])]
        added_atoms = Atoms('Ag2', pos)
        name = 'Test'
        test = Diamond001LineDefect(
                name=name,
                symbol='Ge',
                identifiers=identifiers,
                added_atoms=added_atoms,
                periodicity=periodicity,
                )
        self.assertEqual(test.name(), name)

    def testPointDefectConstruction(self):
        identifiers=[(2, (0, 0)), (3, (0, 0)), ]
        surface = Ge001
        defect = SurfacePointDefect(
                surface=surface,
                identifiers=identifiers)
        pos1 = defect.removedAtoms(coordinates='orth_surface').get_positions().mean(0)
        pos = [pos1]
        name = 'Test'
        added_atoms = Atoms('Ag', pos)
        test = Diamond001PointDefect(
                name=name,
                symbol='Ge',
                identifiers=identifiers,
                added_atoms=added_atoms,
                )
        self.assertEqual(test.name(), name)

    def testDBWire(self):
        parameters = dict(
                vectors=((2, 0), (0,2)),
                free_layers=1,
                bound_layers=1,
                electrode_layers=0,
                cell_size=45,
                )
        defect = DBWire(
                'Si',
                initial_state=AFM,
                parameters=parameters,
                )
        atoms = defect.atoms()
        expected = [[-5.60529693, -3.11249238, -4.94062674],
                    [-1.765     , -3.11249238, -4.94062674],
                    [ 2.07529693, -3.11249238, -4.94062674],
                    [ 5.91559386, -3.11249238, -4.94062674],
                    [-5.60529693, -0.72780455, -4.94062674],
                    [-1.765     , -0.72780455, -4.94062674],
                    [ 2.07529693, -0.72780455, -4.94062674],
                    [ 5.91559386, -0.72780455, -4.94062674],
                    [-5.60529693,  0.72780455, -4.94062674],
                    [-1.765     ,  0.72780455, -4.94062674],
                    [ 2.07529693,  0.72780455, -4.94062674],
                    [ 5.91559386,  0.72780455, -4.94062674],
                    [-5.60529693,  3.11249238, -4.94062674],
                    [-1.765     ,  3.11249238, -4.94062674],
                    [ 2.07529693,  3.11249238, -4.94062674],
                    [ 5.91559386,  3.11249238, -4.94062674],
                    [-5.60529693, -1.92014846, -4.01575   ],
                    [-1.765     , -1.92014846, -4.01575   ],
                    [ 2.07529693, -1.92014846, -4.01575   ],
                    [ 5.91559386, -1.92014846, -4.01575   ],
                    [-5.60529693,  1.92014846, -4.01575   ],
                    [-1.765     ,  1.92014846, -4.01575   ],
                    [ 2.07529693,  1.92014846, -4.01575   ],
                    [ 5.91559386,  1.92014846, -4.01575   ],
                    [-7.52544539, -1.92014846, -2.658     ],
                    [-3.68514846, -1.92014846, -2.658     ],
                    [ 0.15514846, -1.92014846, -2.658     ],
                    [ 3.99544539, -1.92014846, -2.658     ],
                    [-7.52544539,  1.92014846, -2.658     ],
                    [-3.68514846,  1.92014846, -2.658     ],
                    [ 0.15514846,  1.92014846, -2.658     ],
                    [ 3.99544539,  1.92014846, -2.658     ],
                    [-2.981     , -3.84029693, -1.428     ],
                    [-0.549     , -3.84029693, -1.428     ],
                    [ 4.69959386, -3.84029693, -1.428     ],
                    [ 7.13159386, -3.84029693, -1.428     ],
                    [-2.981     ,  0.        , -1.428     ],
                    [-0.549     ,  0.        , -1.428     ],
                    [ 4.69959386,  0.        , -1.428     ],
                    [ 7.13159386,  0.        , -1.428     ],
                    [-7.68059386, -3.84029693,  0.        ],
                    [-3.53      , -3.84029693,  0.        ],
                    [ 4.15059386, -3.84029693,  0.        ],
                    [-7.68059386,  0.        ,  0.        ],
                    [-3.53      ,  0.        ,  0.        ],
                    [ 4.15059386,  0.        ,  0.        ]]

        self.assertArraysEqual(expected, atoms.positions)

        mag = atoms.get_initial_magnetic_moments()
        mag_take = np.arange(len(mag))[abs(mag) > 0]
        self.assertArraysEqual(mag_take, [33, 37])

    def testZigZagWire(self):
        defect = ZigZagDBWire('Si')
        self.assertFalse(defect.hasRelaxedSubsystems())
        parameters = defect.defaultParameters().copy(
                vectors=((2, 0), (0,2)),
                free_layers=0,
                bound_layers=1,
                electrode_layers=0,
                cell_size=45,
                )
        atoms = defect.atoms(parameters=parameters)
        expected = [[15.3611877145, 0.0          , 0.0          ],
                    [0.0          , 7.68059385725, 0.0          ],
                    [0.0          , 0.0          , 45.0         ]]

        self.assertArraysEqual(atoms.get_cell(), expected, 5)
        array = [[-6.95279, -3.8403 , -3.58288],
                 [-4.5681 , -3.8403 , -3.58288],
                 [-3.11249, -3.8403 , -3.58288],
                 [-0.7278 , -3.8403 , -3.58288],
                 [0.7278  , -3.8403 , -3.58288],
                 [3.11249 , -3.8403 , -3.58288],
                 [4.5681  , -3.8403 , -3.58288],
                 [6.95279 , -3.8403 , -3.58288],
                 [-6.95279, 0.0     , -3.58288],
                 [-4.5681 , 0.0     , -3.58288],
                 [-3.11249, 0.0     , -3.58288],
                 [-0.7278 , 0.0     , -3.58288],
                 [0.7278  , 0.0     , -3.58288],
                 [3.11249 , 0.0     , -3.58288],
                 [4.5681  , 0.0     , -3.58288],
                 [6.95279 , 0.0     , -3.58288],
                 [-5.76045, -3.8403 , -2.658  ],
                 [-1.92015, -3.8403 , -2.658  ],
                 [1.92015 , -3.8403 , -2.658  ],
                 [5.76045 , -3.8403 , -2.658  ],
                 [-5.76045, 0.0     , -2.658  ],
                 [-1.92015, 0.0     , -2.658  ],
                 [1.92015 , 0.0     , -2.658  ],
                 [5.76045 , 0.0     , -2.658  ],
                 [-6.46459, -1.92015, -1.428  ],
                 [-1.216  , -1.92015, -1.428  ],
                 [1.216   , -1.92015, -1.028  ],
                 [6.46459 , -1.92015, -1.428  ],
                 [-6.46459, 1.92015 , -1.428  ],
                 [-1.216  , 1.92015 , -1.428  ],
                 [1.216   , 1.92015 , -1.428  ],
                 [6.46459 , 1.92015 , -1.428  ],
                 [-5.91559, -1.92015, 0.0     ],
                 [-1.765  , -1.92015, 0.0     ],
                 [5.91559 , -1.92015, 0.0     ],
                 [-5.91559, 1.92015 , 0.0     ],
                 [1.765   , 1.92015 , 0.0     ],
                 [5.91559 , 1.92015 , 0.0     ]]

        self.assertArraysEqual(array, atoms.positions, 3)

    def testSiCross(self):
        defect = Cross2DBDefect('Si', initial_state=AFM)
        parameters = defect.defaultParameters().copy(
                free_layers=0,
                bound_layers=1,
                vectors=((1,0), (0,3)),
                cell_size=45,
                )

        atoms = defect.atoms(parameters=parameters)
        expected = [
                    [ 7.68059386 , 0.         , 0.        ],
                    [ 0.         ,11.52089079 , 0.        ],
                    [ 0.         , 0.         ,45.        ]]

        diff = atoms.get_cell() - expected
        self.assertAlmostEqual(sum(sum(abs(diff))), 0, 5)

        expected = [[-3.11249, -3.8403 , -3.58288],
                    [-0.7278 , -3.8403 , -3.58288],
                    [0.7278  , -3.8403 , -3.58288],
                    [3.11249 , -3.8403 , -3.58288],
                    [-3.11249, -0.0    , -3.58288],
                    [-0.7278 , -0.0    , -3.58288],
                    [0.7278  , -0.0    , -3.58288],
                    [3.11249 , -0.0    , -3.58288],
                    [-3.11249, 3.8403  , -3.58288],
                    [-0.7278 , 3.8403  , -3.58288],
                    [0.7278  , 3.8403  , -3.58288],
                    [3.11249 , 3.8403  , -3.58288],
                    [-1.92015, -3.8403 , -2.658  ],
                    [1.92015 , -3.8403 , -2.658  ],
                    [-1.92015, -0.0    , -2.658  ],
                    [1.92015 , -0.0    , -2.658  ],
                    [-1.92015, 3.8403  , -2.658  ],
                    [1.92015 , 3.8403  , -2.658  ],
                    [-1.216  , -5.76045, -1.428  ],
                    [1.216   , -5.76045, -1.428  ],
                    [-1.216  , -1.92015, -1.428  ],
                    [1.216   , -1.92015, -1.428  ],
                    [-1.216  , 1.92015 , -1.428  ],
                    [1.216   , 1.92015 , -1.428  ],
                    [-1.765  , -5.76045, 0.0     ],
                    [1.765   , -5.76045, 0.0     ],
                    [-1.765  , -1.92015, 0.0     ],
                    [1.765   , 1.92015 , 0.0     ]]

        self.assertArraysEqual(expected, atoms.positions, 5)

    def testSubSystemsRelaxed(self):
        defect = Cross2DBDefect('Si', initial_state=AFM)
        defect_parameters = dict(
                             free_layers=0,
                             bound_layers=1,
                             vectors=((2,0),(0,3)),
                             cell_size=50,
                                )
        surface_parameters = dict(
                free_layers=0,
                bound_layers=1,
                vectors=((1, 0),(0, 1)),
                cell_size=50,
                )

        self.assertFalse(defect.isRelaxed())
        surface = defect.surface()
        crystal = surface.crystal()
        atoms = crystal.atoms()
        test_multiplier = 1.1
        atoms.cell *= test_multiplier
        relaxation = atoms

        rcrystal = TestRelaxationSystem(crystal, relaxation)
        surface = surface.setSubSystems([rcrystal])
        atoms1 = surface.atoms(surface_parameters)
        atoms1.rattle(0.1)
        rsurface = TestRelaxationSystem(surface, atoms1)

        s_atoms = rsurface.extraAtoms()
        atoms2 = rsurface.atoms(parameters=defect_parameters)
        defect = defect.setSubSystems([rsurface])
        s_atoms = defect.change(
                s_atoms,
                fro='orth_surface',
                to='orth_defect',
                )
        self.assertTrue(defect.hasRelaxedSubsystems())
        self.assertTrue(defect.surface().isRelaxed())

        self.assertFalse(defect.isRelaxed())
        self.assertTrue(defect.hasRelaxedSubsystems())

        atoms3 = defect.atoms(parameters=defect_parameters)

        expected = [[-7.58538, -4.24618, -3.9156 ],
                    [-4.96681, -4.21367, -3.80426],
                    [-3.263  , -4.22326, -3.89244],
                    [-0.63423, -4.1231 , -3.91598],
                    [0.86328 , -4.24618, -3.9156 ],
                    [3.48185 , -4.21367, -3.80426],
                    [5.18565 , -4.22326, -3.89244],
                    [7.81442 , -4.1231 , -3.91598],
                    [-7.58538, -0.02186, -3.9156 ],
                    [-4.96681, 0.01066 , -3.80426],
                    [-3.263  , 0.00107 , -3.89244],
                    [-0.63423, 0.10123 , -3.91598],
                    [0.86328 , -0.02186, -3.9156 ],
                    [3.48185 , 0.01066 , -3.80426],
                    [5.18565 , 0.00107 , -3.89244],
                    [7.81442 , 0.10123 , -3.91598],
                    [-7.58538, 4.20247 , -3.9156 ],
                    [-4.96681, 4.23499 , -3.80426],
                    [-3.263  , 4.2254  , -3.89244],
                    [-0.63423, 4.32556 , -3.91598],
                    [0.86328 , 4.20247 , -3.9156 ],
                    [3.48185 , 4.23499 , -3.80426],
                    [5.18565 , 4.2254  , -3.89244],
                    [7.81442 , 4.32556 , -3.91598],
                    [-6.32806, -4.19984, -2.85167],
                    [-2.10373, -4.19984, -2.85167],
                    [2.1206  , -4.19984, -2.85167],
                    [6.34492 , -4.19984, -2.85167],
                    [-6.32806, 0.02449 , -2.85167],
                    [-2.10373, 0.02449 , -2.85167],
                    [2.1206  , 0.02449 , -2.85167],
                    [6.34492 , 0.02449 , -2.85167],
                    [-6.32806, 4.24881 , -2.85167],
                    [-2.10373, 4.24881 , -2.85167],
                    [2.1206  , 4.24881 , -2.85167],
                    [6.34492 , 4.24881 , -2.85167],
                    [-7.1252 , -6.30525, -1.64114],
                    [-1.41997, -6.45323, -1.3521 ],
                    [1.32346 , -6.30525, -1.64114],
                    [7.02869 , -6.45323, -1.3521 ],
                    [-7.1252 , -2.08092, -1.64114],
                    [-1.41997, -2.22891, -1.3521 ],
                    [1.32346 , -2.08092, -1.64114],
                    [7.02869 , -2.22891, -1.3521 ],
                    [-7.1252 , 2.1434  , -1.64114],
                    [-1.41997, 1.99542 , -1.3521 ],
                    [1.32346 , 2.1434  , -1.64114],
                    [7.02869 , 1.99542 , -1.3521 ],
                    [-6.46115, -6.37207, 0.04296 ],
                    [-1.9875 , -6.30091, -0.04296],
                    [1.9875  , -6.37207, 0.04296 ],
                    [6.46115 , -6.30091, -0.04296],
                    [-6.46115, -2.14774, 0.04296 ],
                    [-1.9875 , -2.07659, -0.04296],
                    [6.46115 , -2.07659, -0.04296],
                    [-6.46115, 2.07659 , 0.04296 ],
                    [1.9875  , 2.07659 , 0.04296 ],
                    [6.46115 , 2.14774 , -0.04296]]

        e_atoms = Atoms('H24Si24H10', positions=expected)
        self.assertAtomsEqual(e_atoms, atoms3)

        removed_atoms = defect.removedAtoms()
        mean_z = removed_atoms.positions[:, 2].mean(0)
        self.assertAlmostEqual(mean_z, 0, 5)

    def testRelaxation(self):
        defect = Cross2DBDefect('Si', initial_state=AFM)
        defect_parameters = dict(
                                 free_layers=0,
                                 bound_layers=1,
                                 vectors=((2,0),(0,3)),
                                 cell_size=50,
                )
        surface_parameters = dict(
                free_layers=0,
                bound_layers=1,
                vectors=((2, 0),(0, 2)),
                cell_size=50,
                )

        self.assertFalse(defect.isRelaxed())
        surface = defect.surface()
        crystal = surface.crystal()
        atoms = crystal.atoms()
        test_multiplier = 1.1
        atoms.cell *= test_multiplier
        relaxation = atoms
        rcrystal = TestRelaxationSystem(crystal, relaxation)
        surface = surface.setSubSystems([rcrystal])
        atoms1 = surface.atoms(defect_parameters)
        atoms1.rattle(0.1)
        rsurface = TestRelaxationSystem(surface, atoms1)

        atoms2 = rsurface.atoms(parameters=defect_parameters)
        defect = defect.setSubSystems([rsurface])
        self.assertTrue(defect.hasRelaxedSubsystems())
        self.assertTrue(defect.surface().isRelaxed())

        self.assertFalse(defect.isRelaxed())
        self.assertTrue(defect.hasRelaxedSubsystems())

        atoms3 = defect.atoms(parameters=defect_parameters)
        atoms3.rattle(0.1)
        relaxed_defect = TestRelaxationSystem(defect, atoms3)
        atoms4 = relaxed_defect.atoms(parameters=defect_parameters)
        atoms5 = relaxed_defect.fullyUnrelaxed().atoms(parameters=defect_parameters)
        test_cell = atoms5.cell*test_multiplier
        test_cell[2, 2] = atoms4.cell[2, 2]
        self.assertArraysEqual(atoms4.cell, test_cell)

        expected = [[-7.64734, -4.32634, -4.04222],
                    [-5.01929, -4.09064, -4.10259],
                    [-3.4581 , -4.09345, -4.24685],
                    [-0.81195, -4.49476, -4.25721],
                    [0.75796 , -4.36993, -4.23318],
                    [3.26944 , -4.38385, -4.19425],
                    [4.77561 , -4.43975, -3.82555],
                    [7.48454 , -4.23461, -4.3354 ],
                    [-7.82753, -0.08473, -4.13175],
                    [-4.97075, 0.00494 , -4.19091],
                    [-3.60134, 0.19023 , -3.98162],
                    [-1.04078, 0.03556 , -4.31054],
                    [0.61534 , -0.14284, -4.07502],
                    [3.34972 , 0.14607 , -4.02452],
                    [4.86232 , -0.0221 , -4.07187],
                    [7.52393 , -0.25049, -3.9397 ],
                    [-7.54791, 3.99648 , -4.06883],
                    [-4.99201, 4.20525 , -4.0636 ],
                    [-3.3529 , 4.24299 , -4.20832],
                    [-0.89382, 4.038   , -4.15276],
                    [0.60993 , 4.07635 , -4.15703],
                    [3.12681 , 4.13622 , -3.79563],
                    [4.90862 , 4.30331 , -4.18413],
                    [7.54672 , 4.21851 , -3.85925],
                    [-6.423  , -4.25245, -3.06046],
                    [-2.19867, -4.25245, -3.06046],
                    [2.02565 , -4.25245, -3.06046],
                    [6.24998 , -4.25245, -3.06046],
                    [-6.423  , -0.02813, -3.06046],
                    [-2.19867, -0.02813, -3.06046],
                    [2.02565 , -0.02813, -3.06046],
                    [6.24998 , -0.02813, -3.06046],
                    [-6.423  , 4.1962  , -3.06046],
                    [-2.19867, 4.1962  , -3.06046],
                    [2.02565 , 4.1962  , -3.06046],
                    [6.24998 , 4.1962  , -3.06046],
                    [-7.31199, -6.31338, -1.68029],
                    [-1.52582, -6.41522, -1.45117],
                    [1.18151 , -6.48953, -1.70407],
                    [7.05747 , -5.85841, -1.48724],
                    [-6.58093, -3.06397, -1.59464],
                    [-0.89976, -2.9591 , -1.46092],
                    [1.28275 , -2.35767, -1.67903],
                    [6.86796 , -2.248  , -1.79254],
                    [-7.30645, 2.13742 , -1.55308],
                    [-1.28835, 2.03587 , -1.74314],
                    [1.21557 , 2.07523 , -1.60952],
                    [7.18101 , 1.91584 , -1.88087],
                    [-6.50202, -6.3839 , -0.18165],
                    [-2.0442 , -6.39449, -0.01062],
                    [1.92132 , -6.24777, -0.01498],
                    [6.63044 , -6.28793, -0.32722],
                    [-6.48867, -2.10062, -0.22167],
                    [-1.93469, -2.15504, -0.05665],
                    [6.51196 , -2.29773, -0.12927],
                    [-6.68443, 2.21844 , 0.08272 ],
                    [1.85368 , 2.03797 , -0.29705],
                    [6.36505 , 2.15451 , -0.22624]]

        e_atoms = Atoms('H24Si24H10', positions=expected)
        self.assertAtomsEqual(e_atoms, atoms4)


if __name__=='__main__':
    unittest.main()

