# Written by Mads Engelund, 2017, http://espeem.com
import numpy as np
from ase.constraints import FixAtoms
from os.path import join
from CalcTroll.API import view

from CalcTrollASE.Atoms import Atoms
from CalcTroll.API import InitialState, InitialStateAtom
from CalcTroll.Plugins.Systems.Defect import SurfacePointDefect
from CalcTroll.Plugins.Systems.Defect import SurfaceLineDefect
from CalcTroll.Plugins.Systems.Defect import SurfaceReconstruction
from CalcTroll.Core.Visualizations import flatMatplotlibView
from CalcTroll.Core.Visualizations import addMeasuringStick
from Diamond001 import Diamond001
from CalcTroll import API
from CalcTroll.API import DEFAULT

class DBInitialState(InitialState): pass

SINGLE_SPIN = DBInitialState(
        'SINGLE',
        (
        InitialStateAtom(0, (0, 0, 0.01), 1),
        ),
        )

BUCKLED = DBInitialState(
        'BUCKLED',
        (
            InitialStateAtom(0, (0, 0, -0.4), 1),
            InitialStateAtom(1, (0, 0, +0.4), 1),
           ),
        )

RBUCKLED = DBInitialState(
        'RBUCKLED',
        (
            InitialStateAtom(0, (0, 0, +0.4), 1),
            InitialStateAtom(1, (0, 0, -0.4), 1),
           ),
        )

UNBUCKLED = DBInitialState(
           'UNBUCKLED',
           (
            InitialStateAtom(0, (0, 0, -0.0), 0),
            InitialStateAtom(1, (0, 0, +0.0), 0),
           ),
           )

AFM = DBInitialState(
        'AFM',
        (
        InitialStateAtom(0, (0, 0,  0.0), -1) ,
        InitialStateAtom(1, (0, 0,  0.0),  1),
        ),
        )

FM = DBInitialState(
        'FM',
        (
        InitialStateAtom(0, (0, 0,  0.0),  1) ,
        InitialStateAtom(1, (0, 0,  0.0),  1),
        ),
        )

class DBWireState(InitialState): pass

ONE_RAISED = DBWireState(
        'OR',
        (
        InitialStateAtom(0, (0, 0,  0.4),  1) ,
        ),
        )

ONE_POLARIZED = DBWireState(
        'OP',
        (
        InitialStateAtom(0, (0, 0,  0.01),  1) ,
        ),
        )

WIRE_FM = DBWireState(
        'FM',
        default_magnetic_moment=1,
        )

WIRE_AFM = DBWireState(
        'AFM',
        (
        InitialStateAtom(1,  initial_moment=-1) ,
        InitialStateAtom(-1, initial_moment=-1) ,
        InitialStateAtom(3,  initial_moment=-1) ,
        InitialStateAtom(-3, initial_moment=-1) ,
        InitialStateAtom(5,  initial_moment=-1) ,
        InitialStateAtom(-5, initial_moment=-1) ,
        InitialStateAtom( 7,  initial_moment=-1) ,
        InitialStateAtom(-7, initial_moment=-1) ,
        ),
        default_magnetic_moment=1,
        )

WIRE_BUCKLED = DBWireState(
        'BUCKLED',
        (
        InitialStateAtom(0,  (0.01, 0.01,  0.41)) ,
        InitialStateAtom(1,  (0.01, 0.01, -0.41)) ,
        InitialStateAtom(-1, (0.01, 0.01, -0.41)) ,
        InitialStateAtom(2,  (0.01, 0.01,  0.41)) ,
        InitialStateAtom(-2, (0.01, 0.01,  0.41)) ,
        InitialStateAtom(3,  (0.01, 0.01, -0.41)) ,
        InitialStateAtom(-3, (0.01, 0.01, -0.41)) ,
        InitialStateAtom(4,  (0.01, 0.01,  0.41)) ,
        InitialStateAtom(-4, (0.01, 0.01,  0.41)) ,
        InitialStateAtom(5,  (0.01, 0.01, -0.41)) ,
        InitialStateAtom(-5, (0.01, 0.01, -0.41)) ,
        InitialStateAtom(6,  (0.01, 0.01,  0.41)) ,
        InitialStateAtom(6,  (0.01, 0.01,  0.41)) ,
        ),
        )

WIRE_ALTBUCKLE = DBWireState(
        'BUCKLED',
        (
        InitialStateAtom(-16,(0, 0,  0.4)) ,
        InitialStateAtom(-15,(0, 0, -0.4)) ,
        InitialStateAtom(-14,(0, 0, -0.4)) ,
        InitialStateAtom(-13,(0, 0,  0.4)) ,
        InitialStateAtom(-12,(0, 0,  0.4)) ,
        InitialStateAtom(-11,(0, 0, -0.4)) ,
        InitialStateAtom(-10,(0, 0, -0.4)) ,
        InitialStateAtom(-9, (0, 0,  0.4)) ,
        InitialStateAtom(-8, (0, 0,  0.4)) ,
        InitialStateAtom(-7, (0, 0, -0.4)) ,
        InitialStateAtom(-6, (0, 0, -0.4)) ,
        InitialStateAtom(-5, (0, 0,  0.4)) ,
        InitialStateAtom(-4, (0, 0,  0.4)) ,
        InitialStateAtom(-3, (0, 0, -0.4)) ,
        InitialStateAtom(-2, (0, 0, -0.4)) ,
        InitialStateAtom(-1, (0, 0,  0.4)) ,
        InitialStateAtom( 0, (0, 0,  0.4)) ,
        InitialStateAtom( 1, (0, 0, -0.4)) ,
        InitialStateAtom( 2, (0, 0, -0.4)) ,
        InitialStateAtom( 3, (0, 0,  0.4)) ,
        InitialStateAtom( 4, (0, 0,  0.4)) ,
        InitialStateAtom( 5, (0, 0, -0.4)) ,
        InitialStateAtom( 6, (0, 0, -0.4)) ,
        InitialStateAtom( 7, (0, 0,  0.4)) ,
        InitialStateAtom( 8, (0, 0,  0.4)) ,
        InitialStateAtom( 9, (0, 0, -0.4)) ,
        InitialStateAtom(10, (0, 0, -0.4)) ,
        InitialStateAtom(11, (0, 0,  0.4)) ,
        InitialStateAtom(12, (0, 0,  0.4)) ,
        InitialStateAtom(13, (0, 0, -0.4)) ,
        InitialStateAtom(14, (0, 0, -0.4)) ,
        InitialStateAtom(15, (0, 0,  0.4)) ,
        InitialStateAtom(16, (0, 0,  0.4)) ,
        InitialStateAtom(17, (0, 0, -0.4)) ,
        InitialStateAtom(18, (0, 0, -0.4)) ,
        ),
        )


WIRE_AFM_RAISED = DBWireState(
        'AFM_R',
        (
        InitialStateAtom( 0, (0.01, 0.01,  0.41),  1) ,
        InitialStateAtom( 1, (0.01, 0.01, -0.01), -1) ,
        InitialStateAtom(-1, (0.01, 0.01, -0.01), -1) ,
        InitialStateAtom( 2, (0.01, 0.01,  0.01),  1) ,
        InitialStateAtom( 3, (0.01, 0.01,  0.01), -1) ,
        InitialStateAtom( 4, (0.01, 0.01,  0.01),  1) ,
        InitialStateAtom( 5, (0.01, 0.01,  0.01), -1) ,
        ),
        )

WIRE_AFM_RAISED_ALT = DBWireState(
        'AFM_R_ALT',
        (
        InitialStateAtom( 0, (0.01, 0.01,  0.11),  1) ,
        InitialStateAtom( 1, (0.01, 0.01, -0.01), -1) ,
        InitialStateAtom(-1, (0.01, 0.01, -0.01), -1) ,
        InitialStateAtom( 2, (0.01, 0.01,  0.01),  1) ,
        InitialStateAtom(-2, (0.01, 0.01,  0.01),  1) ,
        InitialStateAtom( 3, (0.01, 0.01,  0.01), -1) ,
        InitialStateAtom(-3, (0.01, 0.01,  0.01), -1) ,
        InitialStateAtom( 4, (0.01, 0.01,  0.01),  1) ,
        InitialStateAtom(-4, (0.01, 0.01,  0.01),  1) ,
        InitialStateAtom( 5, (0.01, 0.01,  0.01), -1) ,
        InitialStateAtom(-5, (0.01, 0.01,  0.01), -1) ,
        ),
        )

WIRE_FM_RAISED = DBWireState(
        'FM_R',
        (
        InitialStateAtom(0,  (0.01, 0.01,  0.41),  1) ,
        InitialStateAtom(1,  (0.01, 0.01, -0.01),  1) ,
        InitialStateAtom(-1, (0.01, 0.01,  0.01),  1) ,
        InitialStateAtom(2,  (0.01, 0.01,  0.01),  1) ,
        InitialStateAtom(-2, (0.01, 0.01,  0.01),  1) ,
        InitialStateAtom(3,  (0.01, 0.01,  0.01),  1) ,
        InitialStateAtom(-3, (0.01, 0.01,  0.01),  1) ,
        InitialStateAtom(4,  (0.01, 0.01,  0.01),  1) ,
        InitialStateAtom(-4, (0.01, 0.01,  0.01),  1) ,
        InitialStateAtom(5,  (0.01, 0.01,  0.01),  1) ,
        InitialStateAtom(-5, (0.01, 0.01,  0.01),  1) ,
        InitialStateAtom( 6, (0.01, 0.01,  0.01),  1) ,
        InitialStateAtom(-6, (0.01, 0.01,  0.01),  1) ,
        InitialStateAtom( 7, (0.01, 0.01,  0.01),  1) ,
        InitialStateAtom(-7, (0.01, 0.01,  0.01),  1) ,
        InitialStateAtom( 8, (0.01, 0.01,  0.01),  1) ,
        InitialStateAtom(-8, (0.01, 0.01,  0.01),  1) ,
        InitialStateAtom( 9, (0.01, 0.01,  0.01),  1) ,
        InitialStateAtom(-9, (0.01, 0.01,  0.01),  1) ,
        ))

class Diamond001Defect(object):
    def __init__(self, name):
        self.__name = name

    def symbol(self):
        return self.surface().symbol()

    def dbIdentifiers(self):
        return tuple([(index-2, cell) for index, cell in self.identifiers()])

    def findDBs(self, atoms, initialized=True):
        identifiers = self.dbIdentifiers()
        indices = self.findAtomIndices(identifiers, atoms, initialized=initialized)

        return indices

    def initialStateIdentifiers(self, identifiers):
        db_identifiers = self.dbIdentifiers()
        if len(self.periodicity()) == 0:
            new_identifiers = [db_identifiers[i] for i in identifiers]

        elif len(self.periodicity()) == 1:
            ndb_identifiers = len(db_identifiers)
            identifiers = [divmod(i, ndb_identifiers) for i in identifiers]
            new_identifiers = []
            for div, remain in identifiers:
                identifier = db_identifiers[remain]
                identifier = (identifier[0], tuple(np.array(identifier[1]) + np.array(self.periodicity()[0])*div))
                new_identifiers.append(identifier)

        return new_identifiers

    def atomsTemplate(self, parameters):
        system = self.fullyUnrelaxed()
        atoms = super(Diamond001Defect, system).atomsTemplate(parameters)

        identifiers = system.dbIdentifiers()
        indices = system.findAtomIndices(
                identifiers,
                atoms,
                initialized=False,
                )
        atoms.setTag('DB', indices)

        return atoms

    def makeSubsystemsRelaxed(self, atoms, tolerance=1.0):
        unrelaxed = self.fullyUnrelaxed()
        atoms = unrelaxed.change(
                atoms,
                fro='orth_defect',
                to='orth_surface')
        atoms = self.surface().makeSubsystemsRelaxed(
                atoms,
                tolerance=tolerance,
                )
        atoms = self.surface().makeAtomsRelaxed(atoms, tolerance)
        atoms = self.change(atoms, fro='orth_surface', to='orth_defect')

        return atoms

    def referenceEnergyMatchIndices(self, parameters):
        atoms1 = self.atomsTemplate(parameters)
        parameters = self.subParameters(parameters)[0]
        atoms2 = self.surface().atomsTemplate(parameters)

        z_min = atoms1.positions[:, 2].min()
        i1 = np.arange(len(atoms1))[atoms1.positions[:, 2] < z_min + 0.001]
        z_min = atoms2.positions[:, 2].min()
        i2 = np.arange(len(atoms2))[atoms2.positions[:, 2] < z_min + 0.001]

        return i1, i2


class Diamond001PointDefect(Diamond001Defect, SurfacePointDefect):
    @classmethod
    def defaultInitialState(cls):
        return BUCKLED

    def name(self):
        if self.__name is DEFAULT:
            name = self.symbol()
            name += SurfacePointDefect.name(self)
            init = self.initialState()
            if init != self.defaultInitialState():
                name = join(name, init.name())
        else:
            name = self.__name

        return name

    def __init__(
            self,
            symbol,
            name=DEFAULT,
            identifiers=tuple(),
            added_atoms=DEFAULT,
            charge=0,
            initial_state=DEFAULT,
            parameters=DEFAULT,
            ):
        self.__name = name
        surface = Diamond001(symbol)
        Diamond001Defect.__init__(self, name)
        SurfacePointDefect.__init__(self,
                        surface,
                        name=name,
                        identifiers=identifiers,
                        added_atoms=added_atoms,
                        charge=charge,
                        initial_state=initial_state,
                        parameters=parameters,
                        )

    def initialStateIdentifiers(self, identifiers):
        db_identifiers = self.dbIdentifiers()
        new_identifiers = [db_identifiers[i] for i in identifiers]

        return new_identifiers

class Diamond001LineDefect(Diamond001Defect, SurfaceLineDefect):
    @classmethod
    def defaultInitialState(cls):
        return ONE_RAISED

    def name(self):
        if self.__name is DEFAULT:
            name = self.symbol()
            name += SurfaceLineDefect.name(self)
            init = self.initialState()
            if init != self.defaultInitialState():
                name = join(name, init.name())
        else:
            name = self.__name

        return name

    def __init__(
            self,
            symbol,
            name=DEFAULT,
            identifiers=tuple(),
            added_atoms=DEFAULT,
            periodicity=tuple(tuple()),
            charge=0,
            initial_state=DEFAULT,
            parameters=DEFAULT,
            ):
        self.__name = name
        surface = Diamond001(symbol)
        if initial_state is DEFAULT:
            initial_state = self.defaultInitialState()

        Diamond001Defect.__init__(self, name)
        SurfaceLineDefect.__init__(self,
                        surface,
                        name=name,
                        identifiers=identifiers,
                        added_atoms=added_atoms,
                        charge=charge,
                        periodicity=periodicity,
                        initial_state=initial_state,
                        parameters=parameters,
                        )

    def initialStateIdentifiers(self, identifiers):
        db_identifiers = self.dbIdentifiers()

        ndb_identifiers = len(db_identifiers)
        identifiers = [divmod(i, ndb_identifiers) for i in identifiers]
        new_identifiers = []
        for div, remain in identifiers:
            identifier = db_identifiers[remain]
            identifier = (identifier[0], tuple(np.array(identifier[1]) + np.array(self.periodicity()[0])*div))
            new_identifiers.append(identifier)

        return new_identifiers

class Diamond001Reconstruction(Diamond001Defect, SurfaceReconstruction):
    @classmethod
    def defaultInitialState(cls):
        return ONE_RAISED

    def name(self):
        if self.__name is DEFAULT:
            name = self.symbol()
            name += SurfaceReconstruction.name(self)
            init = self.initialState()
            if init != self.defaultInitialState():
                name = join(name, init.name())
        else:
            name = self.__name

        return name

    def __init__(
            self,
            symbol,
            name=DEFAULT,
            identifiers=tuple(),
            added_atoms=DEFAULT,
            periodicity=((1, 0), (0, 1)),
            charge=0,
            initial_state=DEFAULT,
            parameters=DEFAULT,
            ):
        self.__name = name
        surface = Diamond001(symbol)
        if initial_state is DEFAULT:
            initial_state = self.defaultInitialState()

        Diamond001Defect.__init__(self, name)
        SurfaceReconstruction.__init__(self,
                        surface,
                        name=name,
                        identifiers=identifiers,
                        added_atoms=added_atoms,
                        charge=charge,
                        periodicity=periodicity,
                        initial_state=initial_state,
                        parameters=parameters,
                        )

    def initialStateIdentifiers(self, identifiers):
        db_identifiers = self.dbIdentifiers()
        new_identifiers = []
        for i, cell in identifiers:
            cell = np.tensordot(cell, self.periodicity(), (0, 0))
            ni = (db_identifiers[i][0], tuple(db_identifiers[i][1] + cell))
            new_identifiers.append(ni)

        return new_identifiers

class Diamond001ReconstructionState(InitialState): pass

BUCKLED_p2X2 = Diamond001ReconstructionState(
        'BUCK',
        (
        InitialStateAtom((0, (0, 0)), (0, 0,  0.4),  0) ,
        InitialStateAtom((1, (0, 0)), (0, 0, -0.4),  0) ,
        InitialStateAtom((0, (1, 0)), (0, 0,  0.4),  0) ,
        InitialStateAtom((1, (1, 0)), (0, 0, -0.4),  0) ,
        InitialStateAtom((0, (0, 1)), (0, 0, -0.4),  0) ,
        InitialStateAtom((1, (0, 1)), (0, 0,  0.4),  0) ,
        InitialStateAtom((0, (1, 1)), (0, 0, -0.4),  0) ,
        InitialStateAtom((1, (1, 1)), (0, 0,  0.4),  0) ,
        ),
        )

BUCKLED_c4X2 = Diamond001ReconstructionState(
        'BUCK',
        (
        InitialStateAtom((0, (0, 0)), (0, 0,  0.4),  0) ,
        InitialStateAtom((1, (0, 0)), (0, 0, -0.4),  0) ,
        InitialStateAtom((0, (1, 0)), (0, 0, -0.4),  0) ,
        InitialStateAtom((1, (1, 0)), (0, 0,  0.4),  0) ,
        InitialStateAtom((0, (0, 1)), (0, 0, -0.4),  0) ,
        InitialStateAtom((1, (0, 1)), (0, 0,  0.4),  0) ,
        InitialStateAtom((0, (1, 1)), (0, 0,  0.4),  0) ,
        InitialStateAtom((1, (1, 1)), (0, 0, -0.4),  0) ,
        ),
        )

class Diamond001Unpassivated(Diamond001Reconstruction):
    @classmethod
    def defaultInitialState(cls):
        return  BUCKLED_c4X2

    def __init__(
            self,
            symbol,
            charge=0,
            initial_state=BUCKLED_c4X2,
            name=DEFAULT,
            parameters=DEFAULT,
            ):
        Diamond001Reconstruction.__init__(
                self,
                symbol=symbol,
                identifiers=[2, 3],
                charge=charge,
                initial_state=initial_state,
                periodicity=[[1, 0], [0, 1]],
                name=name,
                parameters=parameters,
                )

    def plot(self, *args, **kwargs):
        kwargs['size'] = 0.9
        frame = super(Diamond001Unpassivated, self).plot(*args, **kwargs)

        return frame

class IsolatedDB(Diamond001PointDefect):
    @classmethod
    def defaultInitialState(cls):
        return SINGLE_SPIN

    def __init__(
            self,
            symbol,
            charge=0,
            initial_state=SINGLE_SPIN,
            name=DEFAULT,
            parameters=DEFAULT,
            ):
        Diamond001PointDefect.__init__(
                self,
                symbol=symbol,
                identifiers=[2],
                charge=charge,
                initial_state=initial_state,
                name=name,
                parameters=parameters,
                )


class Dimer2DBDefect(Diamond001PointDefect):
    def __init__(
            self,
            symbol,
            initial_state=BUCKLED,
            charge=0,
            parameters=DEFAULT,
            ):

        Diamond001PointDefect.__init__(
                self, symbol=symbol,
                identifiers=[2, 3],
                charge=charge,
                initial_state=initial_state,
                parameters=parameters,
                )


class SameSide2DBDefect(Diamond001PointDefect):
    def __init__(self, symbol, charge=0, initial_state=BUCKLED):
        Diamond001PointDefect.__init__(
                self,
                symbol=symbol,
                identifiers=[3, (3, (0, 1))],
                charge=charge,
                initial_state=initial_state,
                )


class Cross2DBDefect(Diamond001PointDefect):
    def __init__(
            self,
            symbol,
            charge=0,
            initial_state=BUCKLED,
            ):
        Diamond001PointDefect.__init__(
                self,
                symbol,
                identifiers=[3, (2, (0, 1))],
                charge=charge,
                initial_state=initial_state,
                )


class Trough2DBDefect(Diamond001PointDefect):
    """
    Two dangling bond sites on different rows, but right across from each other.
    """
    def __init__(self, symbol, charge=0, initial_state=BUCKLED):
        Diamond001PointDefect.__init__(self,
                symbol=symbol,
                identifiers=[3, (2, (1, 0))],
                charge=charge,
                initial_state=initial_state,
                )


class TroughCross2DBDefect(Diamond001PointDefect):
    """
    Two dangling bond sites on different rows, and different lines.
    The last nearest-neighbour.
    """
    def __init__(self, symbol, charge=0, initial_state=BUCKLED):
        Diamond001PointDefect.__init__(self,
                symbol=symbol,
                identifiers=[3, (2, (1, 1))],
                charge=charge,
                initial_state=initial_state,
                )


class TroughCross2DBDefect2(Diamond001PointDefect):
    """
    Two dangling bond sites on different rows, and different lines.
    Having a full dimer in between.
    """
    def __init__(self, symbol, charge=0):
        Diamond001PointDefect.__init__(self,
                symbol=symbol,
                identifiers=[3, (2, (1, 2))],
                charge=charge,
                )


class StepAcross2DBDefect(Diamond001PointDefect):
    """
    Two DBs, shifted one row compared to each other.
    """
    def __init__(self, symbol, charge=0, initial_state=BUCKLED, parameters=DEFAULT):
        Diamond001PointDefect.__init__(self,
                symbol=symbol,
                identifiers=[3, (3, (1, 0))],
                charge=charge,
                initial_state=initial_state,
                parameters=parameters,
                )


class StepAcrossJump2DBDefect(Diamond001PointDefect):
    """
    """
    def __init__(self, symbol, charge=0):
        Diamond001PointDefect.__init__(self,
                symbol=symbol,
                identifiers=[3, (3, (1, 2))],
                charge=charge,
                )


class Step2Along2DBDefect(Diamond001PointDefect):
    """
    Two DBs on the same dimer row, with one dimer separating them. This system
    was studied by Schofield and was the quatum charge qubit proposal by Livaradu.
    """
    def __init__(self, symbol, charge=0, initial_state=BUCKLED):
        Diamond001PointDefect.__init__(self,
                symbol=symbol,
                identifiers=[3, (3, (0, 2))],
                charge=charge,
                initial_state=initial_state,
                )


class Step3Along2DBDefect(Diamond001PointDefect):
    """
    """
    def __init__(self, symbol, charge=0):
        Diamond001PointDefect.__init__(self,
                symbol=symbol,
                identifiers=[3, (3, (0, 3))],
                charge=charge,
                )


class DimerWire(Diamond001LineDefect):
    def __init__(
            self,
            symbol,
            charge=0,
            initial_state=ONE_RAISED,
            parameters=DEFAULT,
            ):
        Diamond001LineDefect.__init__(self,
                symbol=symbol,
                identifiers=[3, 2],
                periodicity=[[0, 1]],
                charge=charge,
                initial_state=initial_state,
                parameters=parameters,
                )


class DBWire(Diamond001LineDefect):
    def __init__(
            self,
            symbol,
            charge=0,
            initial_state=ONE_RAISED,
            parameters=DEFAULT,
            ):
        Diamond001LineDefect.__init__(
                self,
                symbol=symbol,
                identifiers=[3],
                periodicity=[[0, 1]],
                charge=charge,
                initial_state=initial_state,
                parameters=parameters,
                )


class ZigZagDBWire(Diamond001LineDefect):
    def __init__(
            self,
            symbol,
            charge=0,
            initial_state=ONE_RAISED,
            ):
        Diamond001LineDefect.__init__(
                self,
                symbol=symbol,
                identifiers=(3, (2, (0, 1))),
                periodicity=((0, 2),),
                charge=charge,
                initial_state=initial_state,
                )


class DoubleAlongDimerWire(Diamond001LineDefect):
    def __init__(
            self,
            symbol,
            charge=0,
            initial_state=ONE_RAISED,
            periodicity=((1, 0),),
            ):
        Diamond001LineDefect.__init__(
                self,
                symbol=symbol,
                identifiers=(3, 2, (3, (0,1)), (2, (0, 1))),
                periodicity=((1, 0),),
                charge=charge,
                initial_state=initial_state,
                )


class AlongDimerWire(Diamond001LineDefect):
    def __init__(
            self,
            symbol, charge=0,
            initial_state=ONE_RAISED,
            ):
        Diamond001LineDefect.__init__(
                self,
                symbol=symbol,
                identifiers=(3, 2),
                periodicity=((1, 0),),
                charge=charge,
                initial_state=initial_state,
                )

    def electrodeNames(self):
        return ('Left', 'Right')

    def makeTransport(self,
                coordinates='orth_defect',
                relaxed=True,
                passivate=True,
                constrain=True,
                ):
        vectors = slab_parameters.vectors()
        e_vectors = np.array(vectors)
        e_vectors[0,0] = 1
        tmp_slab_parameters = slab_parameters.copy(vectors=e_vectors)
        e_atoms = self.makeSlab(
                 tmp_slab_parameters,
                 coordinates='orth_defect',
                 relaxed=False,
                 passivate=passivate,
                 constrain=False,
                )
        n_elec = len(e_atoms)
        atoms = self.makeSlab(
                       slab_parameters,
                       coordinates='orth_defect',
                       relaxed=False,
                       passivate=passivate,
                       constrain=False,
                      )
        atoms = orderAtoms(atoms, (0, 2, 1))
        n_buf = 0
        names = self.electrodeNames()
        atoms.setToElectrode(
                name=names[0],
                e_range=(n_buf, n_buf + n_elec),
                cell=e_atoms.get_cell(),
                semi_direction=-1,
                )

        atoms.setToElectrode(
                name=names[1],
                e_range=(-n_elec, None),
                cell=e_atoms.get_cell(),
                semi_direction=1,
                )

        if relaxed:
            r_atoms = self.makeSlab(
                           slab_parameters,
                           coordinates='orth_defect',
                           relaxed=relaxed,
                           passivate=passivate,
                           constrain=False,
                          )
            atoms = makeAtomsRelaxed(atoms, r_atoms)

        return atoms


class MetalOn001Defect(Diamond001Defect):
    def name(self):
        return (self.__metal_symbol + Defect.name(self) + self.__surface_symbol + '001')

    def __init__(self, metal_symbol, positions,
                 surface_symbol, identifiers,
                 added_atoms=Atoms([]),
                 periodicity=tuple(tuple())):
        self.__metal_symbol = metal_symbol
        self.__surface_symbol = surface_symbol
        surface = {'Si': Si001(), 'Ge': Ge001()}[surface_symbol]
        positions = np.array(positions)
        symbols = '%s%d'%(metal_symbol, len(positions))
        added_atoms = Atoms(symbols, positions) + added_atoms
        Defect.__init__(self, surface=surface, added_atoms=added_atoms, identifiers=identifiers, periodicity=periodicity)


class SubB(MetalOn001Defect):
    def __init__(self, metal_symbol, surface_symbol):
        identifiers=[(2, (0, 0)), (3, (0, 0)), (-1, (0, 0))]
        periodicity = [[0, 1]]
        defect = Diamond001Defect(surface_symbol, identifiers=identifiers, periodicity=periodicity)
        pos = defect.removedAtoms().get_positions()[-1]
        positions = [pos]
        MetalOn001Defect.__init__(self,
                                  metal_symbol=metal_symbol,
                                  positions=positions,
                                  surface_symbol=surface_symbol,
                                  identifiers=identifiers,
                                  periodicity=periodicity,
                                  )


class SubDimer(MetalOn001Defect):
    def __init__(self, metal_symbol, surface_symbol):
        identifiers=[1, 2,  3, ]
        periodicity = [[0, 1]]
        defect = Diamond001Defect(surface_symbol, identifiers=identifiers, periodicity=periodicity)
        positions = [defect.removedAtoms().get_positions()[0]]
        MetalOn001Defect.__init__(self,
                                  metal_symbol=metal_symbol,
                                  positions=positions,
                                  surface_symbol=surface_symbol,
                                  identifiers=identifiers,
                                  periodicity=periodicity,
                                  )


class DimerAdd(MetalOn001Defect):
    def __init__(self, metal_symbol, surface_symbol):
        identifiers=[(2, (0, 0)), (3, (0, 0)), ]
        periodicity = [[0, 1]]
        defect = Diamond001Defect(surface_symbol, identifiers=identifiers, periodicity=periodicity)
        positions = [defect.removedAtoms().get_positions().mean(0) + np.array([0,0,1])]
        MetalOn001Defect.__init__(self,
                                  metal_symbol=metal_symbol,
                                  positions=positions,
                                  surface_symbol=surface_symbol,
                                  identifiers=identifiers,
                                  periodicity=periodicity,
                                  )


class DimerAddB(MetalOn001Defect):
    def __init__(self, metal_symbol, surface_symbol):
        identifiers=[(2, (0, 0)), (3, (0, 0)), ]
        periodicity = [[0, 1]]
        defect = Diamond001Defect(surface_symbol, identifiers=identifiers, periodicity=periodicity)
        positions = [defect.removedAtoms().get_positions().mean(0) + np.array([3,0,1])]
        MetalOn001Defect.__init__(self,
                                  metal_symbol=metal_symbol,
                                  positions=positions,
                                  surface_symbol=surface_symbol,
                                  identifiers=identifiers,
                                  periodicity=periodicity,
                                  )


class DimerAdd2(MetalOn001Defect):
    def __init__(self, metal_symbol, surface_symbol):
        identifiers=[(2, (0, 0)), (3, (0, 0)), ]
        periodicity = [[0, 1]]
        defect = Diamond001Defect(surface_symbol, identifiers=identifiers, periodicity=periodicity)
        pos1 = defect.removedAtoms().get_positions().mean(0)
        positions = [pos1 + np.array([3,0,1]), pos1 + np.array([-3,0,1])]
        MetalOn001Defect.__init__(self,
                                  metal_symbol=metal_symbol,
                                  positions=positions,
                                  surface_symbol=surface_symbol,
                                  identifiers=identifiers,
                                  periodicity=periodicity,
                                  )


class BulkLikeSub(MetalOn001Defect):
    def __init__(self, metal_symbol, surface_symbol):
        identifiers=[(1,(0,0)), (2, (0, 0)), (3, (0, 0)), ]
        periodicity = [[0, 1]]
        defect = Diamond001Defect(surface_symbol, identifiers=identifiers,
                                  periodicity=periodicity)
        positions = [defect.removedAtoms().get_positions()[0]]
        ge_position = defect.removedAtoms().get_positions()[1:].mean(0) + np.array([0,0,1])
        added_atoms = Atoms('Ge', [ge_position])
        MetalOn001Defect.__init__(self,
                                  metal_symbol=metal_symbol,
                                  positions=positions,
                                  surface_symbol=surface_symbol,
                                  identifiers=identifiers,
                                  added_atoms=added_atoms,
                                  periodicity=periodicity,
                                  )

class AgBulkLikeSubGe001(SurfaceLineDefect):
    def __init__(self):
        surface = Ge001()
        identifiers=[(1,(0,0)), (2, (0, 0)), (3, (0, 0)), ]
        periodicity = [[0, 1]]
        defect = Defect(surface=surface, identifiers=identifiers, periodicity=periodicity)
        pos1 = defect.removedAtoms().get_positions()[0]
        pos2 = defect.removedAtoms().get_positions()[1:].mean(0) + np.array([0,0,1])
        added_atoms = Atoms('AgGe', [pos1, pos2])
        Defect.__init__(self, surface=Ge001(),
                        identifiers=identifiers,
                        added_atoms=added_atoms,
                        periodicity=periodicity,
                        )


class AgSubGe001(SurfaceLineDefect):
    def __init__(self):
        surface = Ge001()
        identifiers=[(2, (0, 0)), (-1, (0, 0))]
        periodicity = [[0, 1]]
        defect = Defect(surface=surface, identifiers=identifiers, periodicity=periodicity)
        pos = defect.removedAtoms().get_positions()[1]
        added_atoms = Atoms('Ag', [pos])
        SurfaceLineDefect.__init__(self, surface=Ge001(),
                        identifiers=identifiers,
                        added_atoms=added_atoms,
                        periodicity=periodicity,
                        )


class AgSubBGe001(SurfaceLineDefect):
    def __init__(self):
        surface = Ge001()
        identifiers=[(2, (0, 0)), (3, (0, 0)), (-1, (0, 0))]
        periodicity = [[0, 1]]
        defect = SurfaceLineDefect(surface=surface, identifiers=identifiers, periodicity=periodicity)
        pos = defect.removedAtoms().get_positions()[-1]
        added_atoms = Atoms('Ag', [pos])
        SurfaceLineDefect.__init__(self, surface=Ge001(),
                        identifiers=identifiers,
                        added_atoms=added_atoms,
                        periodicity=periodicity,
                        )


class AgOnSingleDBGe001(SurfaceLineDefect):
    def __init__(self):
        surface = Ge001()
        identifiers=[(3, (0, 0))]
        periodicity = [[0, 1]]
        defect = SurfaceLineDefect(
                surface=surface,
                identifiers=identifiers,
                periodicity=periodicity,
                )
        positions = defect.removedAtoms().get_positions()
        added_atoms = Atoms('Ag', [[0, 0, 1.1]])
        SurfaceLineDefect.__init__(
                self, surface=Ge001(),
                identifiers=identifiers,
                added_atoms=added_atoms,
                periodicity=periodicity,
                )


class AgOnGe001(SurfaceLineDefect):
    def __init__(self):
        surface = Ge001
        identifiers=[(3, (0, 0)), (2, (1, 0))]
        periodicity = [[0, 1]]
        defect = SurfaceLineDefect(
                surface=surface,
                identifiers=identifiers,
                periodicity=periodicity,
                )
        positions = defect.removedAtoms().get_positions()
        added_atoms = Atoms('Ag', [[0,0, 1.1]])
        SurfaceLineDefect.__init__(
                self,
                surface=surface,
                identifiers=identifiers,
                added_atoms=added_atoms,
                periodicity=periodicity,
                )


class Ag2OnGe001(SurfaceLineDefect):
    def __init__(self):
        surface = Ge001()
        identifiers=[(3, (0, 0)), (2, (1, 0))]
        periodicity = [[0, 1]]
        defect = SurfaceLineDefect(surface=surface, identifiers=identifiers, periodicity=periodicity)
        positions = defect.removedAtoms().get_positions()
        added_atoms = Atoms('Ag2', [[-1.1,-1.1, 1.1], [1.1, 1.1, 1.1],])
        SurfaceLineDefect.__init__(self, surface=Ge001(),
                        identifiers=identifiers,
                        added_atoms=added_atoms,
                        periodicity=periodicity,
                        )


class Ag3On2Ge001(SurfaceLineDefect):
    def __init__(self):
        surface = Ge001()
        identifiers=[(3, (0, 0)), (2, (1, 0)), (3, (0, 1)), (2, (1, 1))]
        periodicity = [[0, 2]]
        defect = SurfaceLineDefect(surface=surface, identifiers=identifiers, periodicity=periodicity)
        positions = defect.removedAtoms().get_positions()
        added_atoms = Atoms('Ag3', [[-1.3,-1.6, 1.1], [-1.3, 1.6, 1.1],[1.3, 0, 1.1]])
        SurfaceLineDefect.__init__(self, surface=Ge001(),
                        identifiers=identifiers,
                        added_atoms=added_atoms,
                        periodicity=periodicity,
                        )


class Ag3On2GeB(SurfaceLineDefect):
    def __init__(self):
        surface = Ge001()
        identifiers=[(3, (0, 0)), (2, (1, 0)), (3, (0, 1)), (2, (1, 1))]
        periodicity = [[0, 2]]
        defect = SurfaceLineDefect(surface=surface, identifiers=identifiers, periodicity=periodicity)
        pos = defect.removedAtoms('orth_defect').positions.mean(0)
        vector = defect.cell()[0]
        pos = np.array([pos + vector*(i/3.0) for i in range(3)])
        added_atoms = Atoms('Ag3', pos)
        SurfaceLineDefect.__init__(self, surface=Ge001(),
                        identifiers=identifiers,
                        added_atoms=added_atoms,
                        periodicity=periodicity,
                        )


class AgSubDimerGe001(SurfaceLineDefect):
    def __init__(self):
        surface = Ge001()
        identifiers=[1, 2,  3, ]
        periodicity = [[0, 1]]
        defect = SurfaceLineDefect(surface=surface, identifiers=identifiers, periodicity=periodicity)
        pos1 = defect.removedAtoms().get_positions()[0]
        added_atoms = Atoms('Ag', [pos1])
        SurfaceLineDefect.__init__(self, surface=Ge001(),
                        identifiers=identifiers,
                        added_atoms=added_atoms,
                        periodicity=periodicity,
                        )


class AgDimerAddGe001(SurfaceLineDefect):
    def __init__(self):
        surface = Ge001()
        identifiers=[(2, (0, 0)), (3, (0, 0)), ]
        periodicity = [[0, 1]]
        defect = SurfaceLineDefect(surface=surface, identifiers=identifiers, periodicity=periodicity)
        pos1 = defect.removedAtoms().get_positions().mean(0) + np.array([0,0,1])
        added_atoms = Atoms('Ag', [pos1])
        SurfaceLineDefect.__init__(self, surface=Ge001(),
                        identifiers=identifiers,
                        added_atoms=added_atoms,
                        periodicity=periodicity,
                        )


class AgDimerAddBGe001(SurfaceLineDefect):
    def __init__(self):
        surface = Ge001()
        identifiers=[(2, (0, 0)), (3, (0, 0)), ]
        periodicity = [[0, 1]]
        defect = SurfaceLineDefect(surface=surface, identifiers=identifiers, periodicity=periodicity)
        pos1 = defect.removedAtoms().get_positions().mean(0) + np.array([3,0,1])
        added_atoms = Atoms('Ag', [pos1])
        SurfaceLineDefect.__init__(self, surface=Ge001(),
                        identifiers=identifiers,
                        added_atoms=added_atoms,
                        periodicity=periodicity,
                        )


class AgDimerAdd2Ge001(SurfaceLineDefect):
    def __init__(self):
        surface = Ge001()
        identifiers=[(2, (0, 0)), (3, (0, 0)), ]
        periodicity = [[0, 1]]
        defect = SurfaceLineDefect(surface=surface, identifiers=identifiers, periodicity=periodicity)
        pos1 = defect.removedAtoms().get_positions().mean(0)
        pos = [pos1 + np.array([3,0,1]), pos1 + np.array([-3,0,1])]
        added_atoms = Atoms('Ag2', pos)
        SurfaceLineDefect.__init__(self, surface=Ge001(),
                        identifiers=identifiers,
                        added_atoms=added_atoms,
                        periodicity=periodicity,
                        )


class Pad(Diamond001Defect):
    def __init__(self, symbol):
        positions = [
                [ -2.88000100e+00 , 7.14285704e-07 , 0.00000000e+00],
                [ -2.88000100e+00 , 5.75999871e+00 , 2.02000000e+00],
                [ -2.88000100e+00 , 1.92000071e+00 , 2.02000000e+00],
                [ -2.88000100e+00 ,-1.91999929e+00 , 2.02000000e+00],
                [ -2.88000100e+00 ,-5.76000029e+00 , 2.02000000e+00],
                [ -2.88000100e+00 , 3.83999871e+00 , 0.00000000e+00],
                [ -2.88000100e+00 ,-3.83999929e+00 , 0.00000000e+00],
                [ -9.60000332e-01 , 3.83999871e+00 , 2.02000000e+00],
                [ -9.60000332e-01 , 7.14285704e-07 , 2.02000000e+00],
                [ -9.60000332e-01 ,-3.83999929e+00 , 2.02000000e+00],
                [  9.59997668e-01 , 5.75999871e+00 , 2.02000000e+00],
                [  9.59997668e-01 , 1.92000071e+00 , 2.02000000e+00],
                [  9.59997668e-01 ,-1.91999929e+00 , 2.02000000e+00],
                [  9.59997668e-01 ,-5.76000029e+00 , 2.02000000e+00],
                [ -9.60000332e-01 , 5.75999871e+00 , 0.00000000e+00],
                [ -9.60000332e-01 , 1.92000071e+00 , 0.00000000e+00],
                [ -9.60000332e-01 ,-1.91999929e+00 , 0.00000000e+00],
                [  9.59997668e-01 , 3.83999871e+00 , 0.00000000e+00],
                [  9.59997668e-01 , 7.14285704e-07 , 0.00000000e+00],
                [  9.59997668e-01 ,-3.83999929e+00 , 0.00000000e+00],
                [ -9.60000332e-01 ,-5.76000029e+00 , 0.00000000e+00],
                [  2.88000367e+00 , 3.83999871e+00 , 2.02000000e+00],
                [  2.88000367e+00 , 7.14285704e-07 , 2.02000000e+00],
                [  2.88000367e+00 ,-3.83999929e+00 , 2.02000000e+00],
                [  2.88000367e+00 , 5.75999871e+00 , 0.00000000e+00],
                [  2.88000367e+00 , 1.92000071e+00 , 0.00000000e+00],
                [  2.88000367e+00 ,-1.91999929e+00 , 0.00000000e+00],
                [  2.88000367e+00 ,-5.76000029e+00 , 0.00000000e+00],]
        atoms = Atoms('Au28', positions=positions)
        atoms.positions[:, 0] += 1.00
        atoms.positions[:, 1] += 1.9
        atoms.positions[:, 2] += 5.6
        Diamond001Defect.__init__(self, symbol=symbol, periodicity=[[1, 0]], added_atoms=atoms)

    def electrodeNames(self):
        return ('Left', 'Right')

    def makeAtoms(
            self,
            parameters=DEFAULT,
            coordinates='orth_defect',
            constrained=True,
            center=None,
            ):
        basis_changer = self.basisChanger()
        atoms = Diamond001Defect.makeAtoms(
                self,
                parameters=parameters,
                coordinates=coordinates,
                constrained=constrained,
                basis_changer=basis_changer,
                center=center,
                )
        mask = atoms.positions[:, 2] > atoms.positions[:,2].max() - 1e-5
        max_y = atoms[mask].positions[:,1].max()
        min_y = atoms[mask].positions[:,1].min()
        mask2 = atoms.positions[:, 1] < min_y + 1e-1
        mask3 = atoms.positions[:, 1] > max_y - 1e-1
        mask2 = np.logical_or(mask2, mask3)
        mask = np.logical_and(mask, mask2)

        if constrained:
            mask = np.logical_or(mask, atoms.constraints[0].index)
            atoms.set_constraint(FixAtoms(mask=mask))

        return atoms
