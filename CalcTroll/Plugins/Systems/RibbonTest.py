# Written by Mads Engelund, 2017, http://espeem.com
import unittest

from CalcTrollASE.Atoms import Atoms
from CalcTroll.Core.Test.Case import CalcTrollTestCase
from CalcTroll.API import view

from Ribbon import ArmchairRibbon, ZigzagRibbon

class RibbonsTest(CalcTrollTestCase):
    def testArmchairConstruction(self):
        ribbon = ArmchairRibbon(n=7)
        atoms = ribbon.atoms()
        expected = [[ 0.   ,     -2.45951215, 0.        ],
                    [ 0.   ,      0.        , 0.        ],
                    [ 0.   ,      2.45951215, 0.        ],
                    [ 0.   ,     -2.45951215, 1.42      ],
                    [ 0.   ,      0.        , 1.42      ],
                    [ 0.   ,      2.45951215, 1.42      ],
                    [ 0.   ,     -4.63323591, 1.585     ],
                    [ 0.   ,      4.63323591, 1.585     ],
                    [ 0.   ,     -3.68926822, 2.13      ],
                    [ 0.   ,     -1.22975607, 2.13      ],
                    [ 0.   ,      1.22975607, 2.13      ],
                    [ 0.   ,      3.68926822, 2.13      ],
                    [ 0.   ,     -3.68926822, 3.55      ],
                    [ 0.   ,     -1.22975607, 3.55      ],
                    [ 0.   ,      1.22975607, 3.55      ],
                    [ 0.   ,      3.68926822, 3.55      ],
                    [ 0.   ,     -4.63323591, 4.095     ],
                    [ 0.   ,      4.63323591, 4.095     ]]

        e_atoms = Atoms('C6H2C8H2', positions=expected)
        self.assertAtomsEqual(e_atoms, atoms)

    def testZigZagConstruction(self):
        ribbon = ZigzagRibbon(n=7)
        atoms = ribbon.atoms()
        expected = [[ 0.   ,     -7.835  ,    0.        ],
                    [ 0.   ,     -6.745  ,    0.        ],
                    [ 0.   ,     -3.905  ,    0.        ],
                    [ 0.   ,     -2.485  ,    0.        ],
                    [ 0.   ,      0.355  ,    0.        ],
                    [ 0.   ,      1.775  ,    0.        ],
                    [ 0.   ,      4.615  ,    0.        ],
                    [ 0.   ,      6.035  ,    0.        ],
                    [ 0.   ,     -6.035  ,    1.22975607],
                    [ 0.   ,     -4.615  ,    1.22975607],
                    [ 0.   ,     -1.775  ,    1.22975607],
                    [ 0.   ,     -0.355  ,    1.22975607],
                    [ 0.   ,      2.485  ,    1.22975607],
                    [ 0.   ,      3.905  ,    1.22975607],
                    [ 0.   ,      6.745  ,    1.22975607],
                    [ 0.   ,      7.835  ,    1.22975607]]

        e_atoms = Atoms('HC14H', positions=expected)
        self.assertAtomsEqual(e_atoms, atoms)


if __name__=='__main__':
    unittest.main()
