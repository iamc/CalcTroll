# Written by Mads Engelund, 2017, http://espeem.com
import numpy as np
from ase import Atoms, Atom
from ase.visualize import view

from CalcTrollASE.Atoms import Atoms
from CalcTroll.Plugins.Systems.Crystal import Crystal1D
from CalcTroll.Plugins.Systems.Surface import Surface1D
from CalcTroll import API
from CalcTroll.API import DEFAULT

class ArmchairRibbon(Crystal1D):
    def __init__(
            self,
            n=7,
            parameters=DEFAULT,
            ):
        atoms = Atoms([])
        C_C = 1.42
        C_H = 1.09
        main_element = 'C'
        saturate_element = 'H'
        vacuum = 10
        b = np.sqrt(3) * C_C / 4
        arm_unit = Atoms(main_element + '2',
                     pbc=(1, 0, 1),
                     cell=[
                           [2 * vacuum, 0, 0 ],
                           [0, 2 * b, 1.5*C_C],
                           [0, 0, 3 * C_C    ],])

        arm_unit.positions = [[0, 0, -1.5*C_C],
                              [0, 0, -0.5*C_C]]


        core_atoms = arm_unit.repeat((1, n, 1))
        h_positions=np.array([
                [0, -np.sqrt(3) / 2 * C_H, - C_H * 0.5],
                [0, -np.sqrt(3) / 2 * C_H,  C_H * 0.5],
                ])
        atoms = Atoms(
                saturate_element + '2',
                cell= core_atoms.get_cell(),
                )
        atoms.positions = core_atoms[:2].positions + h_positions
        atoms += core_atoms
        last_hydrogens = Atoms(
                saturate_element + '2',
                )
        h_positions[:, 1] *= -1
        last_hydrogens.positions = core_atoms[-2:].positions + h_positions
        atoms += last_hydrogens
        atoms.cell = [b * 2 * n + 2 * vacuum, 2 * vacuum, 3 * C_C]
        atoms.set_pbc([False, False, True])
        atoms.wrap()
        atoms.translate((0, -b*float(n-1),  0))

        Crystal1D.__init__(
                self,
                atoms,
                parameters=parameters,
                )


class ZigzagRibbon(Crystal1D):
    def __init__(
            self,
            n=6,
            parameters=DEFAULT,
            ):
        atoms = Atoms([])
        C_C = 1.42
        C_H = 1.09
        main_element = 'C'
        saturate_element = 'H'
        vacuum = 10
        m = 1
        b = np.sqrt(3) * C_C / 4

        zz_unit = Atoms(main_element + '2',
                       pbc=(0, 1, 1),
                       cell=[2 * vacuum, 3 * C_C / 2.0, b * 4])

        zz_unit.positions = [[0, 0, 0],
                             [0, C_C / 2.0, b * 2]]

        edge_index0 = np.arange(m) * 2 + 1
        edge_index1 = (n - 1) * m * 2 + np.arange(m) * 2

        for i in range(n):
            layer = zz_unit.repeat((1, 1, m))
            layer.positions[:, 1] -= 3 * C_C / 2 * i
            if i % 2 == 1:
                layer.positions[:, 2] += 2 * b
                layer[-1].position[2] -= b * 4 * m
            atoms += layer

        H_atoms0 = Atoms(saturate_element + str(m))
        H_atoms0.positions = atoms[edge_index0].positions
        H_atoms0.positions[:, 1] += C_H
        H_atoms1 = Atoms(saturate_element + str(m))
        H_atoms1.positions = atoms[edge_index1].positions
        H_atoms1.positions[:, 1] -= C_H
        atoms += H_atoms0 + H_atoms1

        atoms.cell = [2 * vacuum, n * 3 * C_C / 2 + 2 * vacuum, m * 4 * b]
        atoms.set_pbc([False, False, True])
        center = atoms.positions.mean(axis=0)
        center[0] = 0
        center[2] = 0
        atoms.translate(-center)

        Crystal1D.__init__(
                self,
                atoms,
                parameters=parameters,
                )
