# Written by Mads Engelund, 2017, http://espeem.com
import unittest
import numpy
from ase.visualize import view
from CalcTroll.Core.Test.Case import CalcTrollTestCase

from PolyaromaticHydroCarbon import *

class PolyaromaticHydroCarbonTest(CalcTrollTestCase):

    def testPAHs(self):
        molecule = PolyaromaticHydroCarbon(
            indices=[(0, 0), (1, 0), (1, 1)],
        )
        self.assertEqual(len(molecule), 22)


if __name__=='__main__':
    unittest.main()
