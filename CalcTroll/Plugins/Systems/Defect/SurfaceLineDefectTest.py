# Written by Mads Engelund, 2017, http://espeem.com
import unittest

from CalcTroll.Core.System import InitialStateAtom, InitialState
from CalcTroll.Core.Test.Dummies import TestRelaxationSystem
from CalcTroll.Core.Test.Case import CalcTrollTestCase
from ase.constraints import FixAtoms

from CalcTrollASE.Atoms import Atoms
from CalcTroll.Plugins.Systems.Surface.Surface3D import SurfaceParameters3D
from SurfaceLineDefect import SurfaceLineDefect
from CalcTroll.API import view

class SurfaceLineDefectTest(CalcTrollTestCase):
    pass


if __name__=='__main__':
    unittest.main()
