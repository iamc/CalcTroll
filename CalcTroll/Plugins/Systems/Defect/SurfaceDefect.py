# Written by Mads Engelund, 2017, http://espeem.com
import numpy as np
from ase.visualize import view
from CalcTroll import API
from CalcTroll.API import DEFAULT

from CalcTrollASE.Atoms import Atoms
from CalcTrollASE.AtomUtilities import orderAtoms
from CalcTrollASE.AtomUtilities import findIndices
from CalcTrollASE.AtomUtilities import removePeriodicallyEqualAtoms
from CalcTrollASE.AtomUtilities import makeAtomsRelaxed
from CalcTrollASE.AtomUtilities import cellCenter
from CalcTroll.Core.Visualizations import flatMatplotlibView
from CalcTroll.Core.Visualizations import addMeasuringStick
from CalcTroll.Plugins.Systems.Surface.Surface3D import convertShorthand, setCellAndPBC
from CalcTroll.Plugins.Systems.Defect import Defect
from CalcTroll.Core.System.BasisChanger import BasisChanger, Transformation

from CalcTroll.Plugins.Systems.Surface.Surface import setCellAndPBC

class SurfaceDefect(Defect):
    def __init__(
            self,
            surface,
            name=DEFAULT,
            identifiers=tuple(),
            added_atoms=DEFAULT,
            periodicity=tuple(tuple()),
            charge=0,
            initial_state=DEFAULT,
            parameters=DEFAULT,
            ):
        Defect.__init__(
                self,
                base_system=surface,
                name=name,
                identifiers=identifiers,
                added_atoms=added_atoms,
                periodicity=periodicity,
                charge=charge,
                initial_state=initial_state,
                parameters=parameters,
                )

    def convertIdentifierShorthand(self, identifiers):
        return convertShorthand(identifiers)

    def name(self):
        name = API.System.name(self)
        if self.charge() != 0:
            name += '_Q%.4f' % self.charge()

        return name

    def surface(self):
        return self.baseSystem()

    def setUpBasisChanger(self):
        surface = self.surface()
        extra_atoms = surface.extraAtoms(coordinates='orth_surface')
        crystal_atoms = surface.crystalAtoms(coordinates='orth_surface')
        added_atoms, a_coordinates = self._addedAtoms()
        identifiers = self.identifiers()
        periodicity = self.periodicity()

        transformations = []
        if len(identifiers) > 0:
            atoms = findAtoms(extra_atoms, crystal_atoms, identifiers, surface.cell('orth_surface'), periodicity)
            point = atoms.positions.mean(0)
            surface_transformation = Transformation(
                    to='orth_surface',
                    fro='orth_defect',
                    rotation_point=[point, [0,0,0]],
                    )
        elif len(added_atoms) > 0:
            point = added_atoms.get_positions().mean(axis=0)
            point[2] = added_atoms.get_positions()[:, 2].max()
            surface_transformation = Transformation(
                    to='orth_surface',
                    fro='orth_defect',
                    rotation_point=[point, [0,0,0]],
                    )
        else:
            surface_transformation = Transformation(
                    to='orth_surface',
                    fro='orth_defect',
                    )
        transformations.append(surface_transformation)
        basis_changer = BasisChanger(transformations=transformations)

        return basis_changer

    def atomsTemplate(self, parameters):
        remove_atoms, coordinates = self._removedAtoms()
        added_atoms, coordinates = self._addedAtoms()

        surface = self.surface()
        atoms = surface.atomsTemplate(
                parameters=parameters,
                )
        atoms = removePeriodicallyEqualAtoms(remove_atoms, atoms)
        atoms += added_atoms

        if len(added_atoms) > 0:
            atoms.setRegion(name=self.name(), picker=slice(-len(added_atoms), None), cell=atoms.get_cell(), pbc=(False, False, False))

        atoms = self.fullyUnrelaxed().change(
                atoms,
                to='orth_defect',
                fro='orth_surface',
                )
        atoms.wrap(center=0, eps=0.01)
        atoms = orderAtoms(atoms)

        cell_center = cellCenter(atoms)
        atoms.set_celldisp(-cell_center)

        return atoms

    def makeSubsystemsInitialized(self, atoms):
        diff_vector = np.zeros(atoms.positions.shape)
        init_moments = np.zeros(len(atoms))

        for sub_system in self.subSystems():
            v, i = sub_system.makeAtomsInitialized(atoms)
            diff_vector += v
            init_moments += i

        return diff_vector, init_moments

    def makeSubsystemsRelaxed(self, atoms, tolerance=1.0):
        unrelaxed = self.fullyUnrelaxed()
        atoms = unrelaxed.change(atoms, fro='orth_defect', to='orth_surface')
        atoms = self.surface().makeSubsystemsRelaxed(
                atoms,
                tolerance=tolerance,
                )
        atoms = self.surface().makeAtomsRelaxed(atoms, tolerance=tolerance)
        atoms = self.change(atoms, fro='orth_surface', to='orth_defect')

        return atoms

    def _findAtoms(self,
            identifiers,
            ):
        surface = self.surface().fullyUnrelaxed()
        extra_atoms = surface.extraAtoms()
        crystal_atoms = surface.crystalAtoms(end_layer=1)
        unit_cell = surface.cell()

        atoms = findAtoms(
                extra_atoms,
                crystal_atoms,
                identifiers,
                unit_cell,
                periodicity=self.periodicity(),
                )

        return atoms, 'orth_surface'

    def findAtoms(self,
            identifiers,
            initialized=True,
            relaxed=True,
            coordinates='orth_defect',
            ):
        atoms, base_coordinates = self._findAtoms(identifiers)
        atoms = self.fullyUnrelaxed().change(
                atoms,
                to='orth_defect',
                fro=base_coordinates,
                )

        atoms = self.applyInitializationConstraintsAndRelaxation(
                atoms=atoms,
                initialized=initialized,
                relaxed=relaxed,
                )
        return self.change(atoms, to=coordinates, fro='orth_defect')

    def _addedAtoms(self):
        surface = self.surface().fullyUnrelaxed()
        extra_atoms = surface.extraAtoms()
        unit_cell = surface.unitCell()
        periodicity = self.periodicity()
        atoms = Atoms([], cell=extra_atoms.get_cell(), pbc=extra_atoms.get_pbc())
        atoms += Defect._addedAtoms(self)
        setCellAndPBC(atoms, periodicity)

        return atoms, 'orth_surface'

    def addedAtoms(
            self,
            initialized=True,
            relaxed=True,
            coordinates='orth_defect',
            ):
        atoms, base_coordinates = self._addedAtoms()
        atoms = self.fullyUnrelaxed().change(
                atoms,
                to='orth_defect',
                fro=base_coordinates,
                )

        atoms = self.applyInitializationConstraintsAndRelaxation(
                atoms=atoms,
                initialized=initialized,
                relaxed=relaxed,
                )
        atoms = self.change(atoms, to=coordinates, fro='orth_defect')

        return atoms

    def cell(self):
        cell = self.surface().cell('orth_surface')
        cell = self.change(cell, to='orth_defect', fro='orth_surface', difference=True)
        cell = np.dot(self.periodicity(), cell[:2])

        return cell

    def rattleAtoms(self, coordinates='orth_defect'):
        atoms = self.removedAtoms(coordinates)
        atoms += self.addedAtoms(coordinates)

        return atoms

    def initialStateIdentifiers(self, identifiers):
        return convertShorthand(identifiers)

    def findLayers(self,
            atoms,
            start_layer,
            end_layer,
            coordinates='orth_defect',
            ):
        atoms = self.change(atoms, to='orth_surface', fro=coordinates)
        return self.surface().findLayers(
                atoms,
                start_layer=start_layer,
                end_layer=end_layer
                )


    def bandStructurePath(self):
        path = [
                ((0.0, 0.0, 0.0), '\\Gamma'),
                ((0.5,   0, 0.0), 'A'),
                ((0.5, 0.5, 0.0), 'AB'),
                ((0.0, 0.5, 0.0), 'B'),
                ((0.0, 0.0, 0.0), '\\Gamma'),
                ]
        return path

    def dimension(self):
        return len(self.periodicity())

    def hasRelaxedSurface(self):
        return self.surface().isRelaxed()

    def findAtomIndices(self, identifiers, atoms, initialized=True, coordinates='orth_defect'):
        find_atoms = self.findAtoms(identifiers, initialized=initialized, coordinates=coordinates)
        indices = findIndices(find_atoms, atoms)

        return indices

    def _removedAtoms(self):
        atoms = self._findAtoms(self.identifiers())

        return atoms

    def removedAtoms(
            self,
            initialized=True,
            relaxed=True,
            coordinates='orth_defect',
            ):
        atoms, base_coordinates = self._removedAtoms()
        atoms = self.fullyUnrelaxed().change(
                atoms,
                to='orth_defect',
                fro=base_coordinates,
                )

        atoms = self.applyInitializationConstraintsAndRelaxation(
                atoms=atoms,
                initialized=initialized,
                relaxed=relaxed,
                )

        atoms = self.change(atoms, to=coordinates, fro='orth_defect')

        return atoms

    def removedAtomsIndices(self, atoms, coordinates='orth_defect'):
        return self.findAtomIndices(
                                    self.identifiers(),
                                    atoms,
                                    coordinates=coordinates,
                                    relaxed=relaxed,
                                    )

    def pbc(self):
        return (True, True, False)

    def jpg_save(
            self,
            filename,
            data=None,
            frame=None,
            border=API.DEFAULT,
            **kwargs):
        from matplotlib import pylab as plt
        if data is None:
            data = self.read()
        if frame is None:
            frame = self.frame(
                data=data,
                border=border,
            )

        pmin, pmax = frame
        d = pmax - pmin
        dx = d[0]
        dy = d[1]
        ratio = dx/dy

        fig = plt.figure(frameon=False, figsize=(4*ratio, 4))
        fig.patch.set_facecolor('black')

        ax = plt.Axes(fig, [0., 0., 1., 1.])
        fig.add_axes(ax)

        self.plot(data=data,
                  ax=ax,
                  frame=frame,
                  **kwargs)

        ax.set_xlim((pmin[0], pmax[0]))
        ax.set_ylim((pmin[1], pmax[1]))
        plt.savefig(filename)
        plt.close('all')

        return frame

    def frame(self, data=None, border=API.DEFAULT):
        if not self.isDone():
            return None

        if data is None:
            atoms = self.atoms()
        else:
            atoms = data

        cell_center = cellCenter(atoms)
        pmin, pmax = -cell_center, cell_center

        if border is API.DEFAULT:
            border = 4

        pmin -= border
        pmax += border

        return np.array([pmin, pmax])

    def plot(self,
             data=None,
             ax=None,
             frame=None,
             border=4,
             color='white',
             size=0.55,
             ):
        if data is None:
            atoms = self.atoms()
        else:
            atoms = data

        if frame is None:
            frame = self.frame(data=data, border=border)

        self.plotUnitCell(data=atoms, ax=ax)

        atoms = atoms.repeat((4, 4, 1))
        move = -cellCenter(atoms)
        atoms.translate(move)

        flatMatplotlibView(atoms, ax, direction='-z', size=size)
        if frame is not None:
            ax.set_xlim((frame[0, 0], frame[1, 0]))
            ax.set_ylim((frame[0, 1], frame[1, 1]))
            addMeasuringStick(ax, color=color, size=5)

        ax.patch.set_facecolor('black')
        ax.set_xticks([])
        ax.set_yticks([])

        return frame

    def plotUnitCell(self, data=None, ax=None):
        if data is None:
            atoms = self.atoms()
        else:
            atoms = data

        atoms = self.atoms()
        cell = atoms.get_cell()[:2,:2]
        points = [
            -cell[0]/2 - cell[1]/2,
            cell[0]/2 - cell[1]/2,
            cell[0]/2 + cell[1]/2,
            -cell[0]/2 + cell[1]/2,
            ]
        x, y = np.transpose(points)
        ax.plot(
            x[0:2], y[0:2],
            x[1:3], y[1:3],
            x[2:4], y[2:4],
            [x[-1], x[0]], [y[-1], y[0]],
            color='blue',
            lw=3,
        )


def makeCenters(surface, identifiers, added_atoms, periodicity):
    extra_atoms = surface.extraAtoms()
    crystal_atoms = surface.crystalAtoms(end_layer=1)
    unit_cell = surface.cell()
    atoms = findAtoms(
            extra_atoms,
            crystal_atoms,
            identifiers,
            unit_cell,
            periodicity,
            )
    atoms += added_atoms

    return atoms.get_positions()

def findAtoms(extra_atoms, crystal_atoms, identifiers, surface_unit_cell, periodicity):
    atoms = Atoms([], cell=extra_atoms.get_cell(), pbc=extra_atoms.get_pbc())

    for identifier in identifiers:
        number, cell = identifier

        if number >= 0:
            atom = extra_atoms.copy()[number]
            depth = 0
        elif number < 0:
            number = -(number + 1)
            depth, crystal_number = divmod(number, len(crystal_atoms))

            atom=crystal_atoms.copy()[crystal_number]
        else:
            raise Exception

        shift = -surface_unit_cell[2]*depth

        cell = (cell[0], cell[1], 0)
        shift += np.dot(cell, surface_unit_cell)

        atom.position += shift
        atoms += atom

    setCellAndPBC(atoms, periodicity)

    return atoms
