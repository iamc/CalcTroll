# Written by Mads Engelund, 2017, http://espeem.com
import numpy as np

from CalcTrollASE.AtomUtilities import removePeriodicallyEqualAtoms
from CalcTrollASE.AtomUtilities import findIndices
from CalcTrollASE.AtomUtilities import findCopies
from CalcTrollASE.AtomUtilities import cellCenter
from CalcTrollASE.AtomUtilities import findPeriodicallyEqualAtoms
from CalcTrollASE.AtomUtilities import makeAtomsRelaxed
from CalcTrollASE.Atoms import Atoms
from CalcTroll.Core.Visualizations import flatMatplotlibView
from CalcTroll.Core.Visualizations import addMeasuringStick
from CalcTroll.Core.System.BasisChanger import BasisChanger, Transformation
from CalcTroll.Plugins.LengthScales import LengthScales
from CalcTroll.Plugins.Systems.Surface.Surface import setCellAndPBC
from CalcTroll.Plugins.Systems.Surface.Surface3D import SurfaceParameters3D, convertShorthand
from CalcTroll.Plugins.Systems.Defect import SurfaceDefect
from CalcTroll.Plugins.Systems.Defect.SurfaceDefect import makeCenters
from CalcTroll import API
from CalcTroll.API import DEFAULT


class SurfacePointDefectParameters(SurfaceParameters3D):
    def __init__(
            self,
            free_layers=DEFAULT,
            bound_layers=2,
            electrode_layers=0,
            vectors=DEFAULT,
            kpts=DEFAULT,
            cell_size=DEFAULT,
            length_scales=LengthScales(),
            passivated=True,
            ):
        pass

    def neededKpts(self, atoms):
        scales = self['length_scales']
        unit_cell = atoms.cell[atoms.pbc]
        kpts = np.ones(len(unit_cell))
        for i, vector in enumerate(unit_cell):
            length=np.max(abs(vector))
            number=int(np.ceil(scales['correlation_length']/length))
            kpts[i] = number

        return kpts

class SurfacePointDefect(SurfaceDefect):
    @classmethod
    def parameterClass(cls):
        return SurfacePointDefectParameters

    def subParameters(self, parameters=DEFAULT):
        if parameters is DEFAULT:
            parameters = self.parameters()

        parameters = parameters.copy(vectors=((1,0), (0,1)), kpts=DEFAULT)

        return (SurfacePointDefectParameters(**parameters), )

    def defaultParameters(self, parameters=DEFAULT, centers=((0,0,0), )):
        surface = self.surface().fullyUnrelaxed()
        added_atoms, coordinates = self._addedAtoms()

        surface_parameters = self.surface().defaultParameters()
        surface_parameters = surface_parameters.toDictionary()
        surface_parameters.pop('kpts')
        surface_parameters.pop('vectors')
        if parameters is DEFAULT:
            parameters = SurfacePointDefectParameters(**surface_parameters)
        else:
            parameter_dict = parameters.toDictionary()
            for key, value in surface_parameters.iteritems():
                if parameter_dict.get(key) is None:
                    parameter_dict[key] = surface_parameters[key]

            parameters = SurfacePointDefectParameters(**parameter_dict)

        vectors = parameters['vectors']
        if vectors is DEFAULT:
            new_centers = makeCenters(
                    surface=surface,
                    identifiers=self.identifiers(),
                    added_atoms=added_atoms,
                    periodicity=self.periodicity(),
                    )
            tmp_centers = centers
            centers = np.zeros((len(tmp_centers)+len(new_centers), 3), float)
            centers[:len(tmp_centers)] = tmp_centers
            centers[len(tmp_centers):] = new_centers

            s_parameters = surface.defaultParameters(
                    parameters=parameters,
                    centers=centers,
                    )
            parameters = parameters.copy(vectors=s_parameters['vectors'])

        kpts = parameters['kpts']
        if kpts is DEFAULT:
            s_parameters = surface.defaultParameters(
                    parameters=parameters,
                    )
            kpts = s_parameters['kpts']
            parameters = parameters.copy(kpts=kpts)

        cell_size = parameters['cell_size']
        if cell_size is DEFAULT:
            parameters = parameters.copy(cell_size=45)

        return parameters

    def __init__(
            self,
            surface,
            name=DEFAULT,
            identifiers=tuple(),
            added_atoms=DEFAULT,
            charge=0,
            initial_state=tuple(),
            parameters=DEFAULT,
            ):
        SurfaceDefect.__init__(
                self,
                surface=surface,
                identifiers=identifiers,
                added_atoms=added_atoms,
                charge=charge,
                initial_state=initial_state,
                parameters=parameters,
                periodicity=tuple(tuple()),
                )

    def neededKpts(self, unit_cell, correlation_length):
        kpts=[]
        unit_cell = self.surface().change(unit_cell, fro='surface', to='orth_surface')
        for vector in unit_cell:
            vector = vector/self.surface().directionLenghtMultipliers()
            length=np.sqrt(np.sum(vector**2))
            number=int(np.ceil(correlation_length/length))
            kpts.append(number)

        return kpts

    def bandStructurePath(self):
        path = [
                ((0.0, 0.0, 0.0), '\\Gamma'),
                ((0.5,   0, 0.0), 'A'),
                ((0.5, 0.5, 0.0), 'AB'),
                ((0.0, 0.5, 0.0), 'B'),
                ((0.0, 0.0, 0.0), '\\Gamma'),
                ]
        return path

    def plotUnitCell(self, data=None, ax=None):
        if data is None:
            atoms = self.atoms()
        else:
            atoms = data

        atoms = self.atoms()
        cell = atoms.get_cell()[:2,:2]
        points = [
            -cell[0]/2 - cell[1]/2,
            cell[0]/2 - cell[1]/2,
            cell[0]/2 + cell[1]/2,
            -cell[0]/2 + cell[1]/2,
            ]
        x, y = np.transpose(points)
        ax.plot(
            x[0:2], y[0:2],
            x[1:3], y[1:3],
            x[2:4], y[2:4],
            [x[-1], x[0]], [y[-1], y[0]],
            color='blue',
            lw=3,
        )

    def plot(self,
             data=None,
             ax=None,
             frame=None,
             border=4,
             size=1,
             color='white',
             ):
        if data is None:
            atoms = self.atoms()
        else:
            atoms = data

        if frame is None:
            frame = self.frame(data=data, border=border)

        self.plotUnitCell(data=atoms, ax=ax)

        atoms = atoms.repeat((4, 4, 1))
        move = -cellCenter(atoms)
        atoms.translate(move)

        size = size*0.55
        flatMatplotlibView(atoms, ax, direction='-z', size=size)
        if frame is not None:
            ax.set_xlim((frame[0, 0], frame[1, 0]))
            ax.set_ylim((frame[0, 1], frame[1, 1]))
            addMeasuringStick(ax, color=color, size=5)

        ax.patch.set_facecolor('black')
        ax.set_xticks([])
        ax.set_yticks([])

        return frame

    def frame(self, data=None, border=API.DEFAULT):
        if not self.isDone():
            return None

        if data is None:
            atoms = self.atoms()
        else:
            atoms = data

        cell_center = cellCenter(atoms)
        pmin, pmax = -cell_center, cell_center

        if border is API.DEFAULT:
            border = 4

        pmin -= border
        pmax += border

        return np.array([pmin, pmax])

    def jpg_save(
            self,
            filename,
            data=None,
            frame=None,
            border=API.DEFAULT,
            **kwargs):
        from matplotlib import pylab as plt
        if data is None:
            data = self.read()
        if frame is None:
            frame = self.frame(
                data=data,
                border=border,
            )
        frame = np.array(frame)

        pmin, pmax = frame
        d = pmax - pmin
        dx = d[0]
        dy = d[1]
        ratio = dx/dy

        fig = plt.figure(frameon=False, figsize=(4*ratio, 4))
        fig.patch.set_facecolor('black')

        ax = plt.Axes(fig, [0., 0., 1., 1.])
        fig.add_axes(ax)

        self.plot(data=data,
                  ax=ax,
                  frame=frame,
                  **kwargs)

        ax.set_xlim((pmin[0], pmax[0]))
        ax.set_ylim((pmin[1], pmax[1]))
        plt.savefig(filename)
        plt.close('all')

        return frame
