# Written by Mads Engelund, 2017, http://espeem.com
def removedAtoms(extra_atoms, crystal_atoms, identifiers, surface_unit_cell, periodicity):
    atoms = Atoms([], cell=extra_atoms.get_cell(), pbc=extra_atoms.get_pbc())

    for identifier in identifiers:
        number, cell = identifier

        if number >= 0:
            atom = extra_atoms.copy()[number]
            depth = 0
        elif number < 0:
            number = -(number + 1)
            depth, crystal_number = divmod(number, len(crystal_atoms))

            atom=crystal_atoms.copy()[crystal_number]
        else:
            raise Exception

        shift = -surface_unit_cell[2]*depth

        cell = (cell[0], cell[1], 0)
        shift += np.dot(cell, surface_unit_cell)

        atom.position += shift
        atoms += atom

    setCellAndPBC(atoms, periodicity)

    return atoms

