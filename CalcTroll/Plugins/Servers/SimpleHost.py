# Written by Mads Engelund, 2017, http://espeem.com
# This plugin runs a calculation on the local machine and
# if asked to submit, will simply run the next in line.
# NB: This is not recommended operation and serves only
# for prototyping purposes. The implementation is
# hacky since it operates without a proper submission
# framework and is forced to make guesses about the
# machine state.
import os
import time
import sys
import subprocess
import getpass
from CalcTroll.API import Host
from CalcTroll.Core.Flags import LOCAL
from CalcTroll.Plugins.Programs.Siesta import Siesta


# List of processes recognized when using 'top'.
WHITELIST = ['siesta', 'transiesta', 'tbtrans']

class SimpleHost(Host):

    def waitToFinish(self, time_step):
        """
        There is no need to wait since tasks are not submitted.
        """
        time.sleep(time_step)

    def getSubmitted(self, identifier=None):
        """ Return all significant processes running."""

        # Run 'top' and get the output.
        cmd = 'top -n 1 -b -u %s' % getpass.getuser()
        out = self.executeCommand(cmd, output=True)
        out = out.split('\n')

        # Skip past the first information lines.
        output = []
        for line in out:
            if len(line) > 0 and line.split()[0] == 'PID':
                output.append(line)
                break

        # Find all processes run with known binaries.
        for line in out:
            if len(line) > 0 and line.split()[-1] in WHITELIST:
                output.append(line)

        return output

    def writeSubmitted(self):
        """ Write running processes to the screen."""
        output = self.getSubmitted(identifier=None)
        if len(output) == 1:
            print "Nothing is submitted..."
            return

        for entry in output:
            print entry

    def tailJob(self, filename):
        """ Run command 'tail -f' on the relevant file """
        if filename is None:
            filename = 'TMP.out'
        cmd = 'tail -f %s' % filename
        self.executeCommand(cmd)

    def catJob(self, filename):
        """ Run command 'cat' on the relevant file """
        if filename is None:
            filename = 'TMP.out'
        cmd = 'cat %s' % filename
        self.executeCommand(cmd)

    def executeCommand(self, cmd, verbose=False, output=False):
        """ Excecute a shell command, either with or without returning
            the output."""
        if output:
            out = subprocess.check_output(cmd, shell=True, stdin=subprocess.PIPE, universal_newlines=True)
        else:
            os.system(cmd)
            out = None

        return out

    def run(self, submission_file):
        """ Run the task describe in the submission file """
        dirname = os.path.dirname(submission_file)
        basename = os.path.basename(submission_file)
        if len(dirname) > 0:
            os.chdir(dirname)
        cmd = 'bash %s' % basename
        print "Running calculation..."
        self.executeCommand(cmd, output=True)

    def fileExtension(self):
        """ Return the file extension of the submission file:
            here bash script"""
        return '.sh'

    def methodScript(self, run):
        """  Return part of the script, describing the how to
             call the executables actually doing the work.
             (siesta, transiesta, tbtrans).
        """
        runfile = os.path.basename(run.runFile())
        tmp_outfile = 'TMP.out'
        outfile = os.path.basename(run.outFile())
        method = run.method()
        program = method.program()

        script = ''
        if isinstance(program, Siesta):
            python_executable = sys.executable
            executable = run.executable()
            pseudo_path = program.pseudoPath()
            script += """
export SIESTA_PP_PATH=%s
export PROGRAM=%s
""" % (pseudo_path, python_executable)
        form = """$PROGRAM %s >& %s"""

        for command in run.makeCommands(form):
            script += command
            script += '\n'

        return script

    def isRunning(self, task):
        """ Assume the task is running if something is running. """
        return len(self.getSubmitted()) > 1

    def isQueued(self, task):
        """ Nothing is queued because no submission server is set up. """
        return False

    def topFolder(self, path=LOCAL):
        """ There is only one type of path - the one to the local
            directory tree."""
        if path is not LOCAL:
            raise ValueError

        return self.localPath()

    def isRemote(self):
        """ This host is not remote."""
        return False

