# Written by Mads Engelund, 2017, http://espeem.com
import os
from os.path import join
import subprocess

from CalcTroll import API
from CalcTroll.Core.Flags import LOCAL, REMOTE, TAIL

class SlurmServer(API.SubmissionServer):

    def cancelJobId(self, job_id):
        cmd = 'scancel %s'%job_id
        self.executeCommand(cmd, verbose=True)

    def isRunning(self, run):
        submitted = self.getSubmitted('id')
        if len(submitted) == 0:
            return False

        for jobid in submitted:
            info = self.getJobInfo(jobid)
            is_queued = info['RunTime'][0] == '-'

            runfile = '.'.join(run.runFile(path=REMOTE).split('.')[:-1])
            command = '.'.join(self.localCommandFile(info).split('.')[:-1])

            if runfile==command and not is_queued:
                return True

        return False

    def isQueued(self, run):
        submitted = self.getSubmitted('id')
        if len(submitted) == 0:
            return False


        for jobid in submitted:
            info = self.getJobInfo(jobid)
            is_queued = info['RunTime'][0] == '-'

            runfile = self.changePathType(run.runFile(LOCAL), fro=LOCAL, to=REMOTE)
            command = '.'.join(self.localCommandFile(info).split('.')[:-1])

            if runfile==command and is_queued:
                return True

        return False

    @classmethod
    def setupScript(cls, run):
        sync_cmd = 'rsync -av ${SLURM_SUBMIT_DIR}/* $TMPDIR'
        for needed_file in run.neededFiles():
            sync_cmd += ' --include=%s'%needed_file
        sync_cmd += ' --exclude=*'

        script = """
# Remove annoying or confusing files.
rm $SLURM_SUBMIT_DIR/INPUT_TMP.*

TMPDIR="/lscratch2/$USER/job_${SLURM_JOB_ID}"

# Generate information files in original folder.
echo NODELIST=`scontrol show hostnames $SLURM_NODELIST`

# Make lscratch
srun --ntasks $SLURM_JOB_NUM_NODES --ntasks-per-node=1 \\
mkdir -p $TMPDIR

# Copy to lscratch
srun --ntasks $SLURM_JOB_NUM_NODES --ntasks-per-node=1 \\
%s

# Switch to working directory.
cd $TMPDIR

"""%sync_cmd

        return script

    @classmethod
    def tearDownScript(cls, run):
        sync_cmd = "rsync -av ./* ${SLURM_SUBMIT_DIR} --exclude '*slurm*'"
        if run.resultFiles() is not None:
            for filename in run.resultFiles():
                sync_cmd += ' --include="%s"'%filename
            sync_cmd += ' --exclude="*"'

        script = """
# Cleanup, sleep, then copy back to original folder
%s
srun --ntasks $SLURM_JOB_NUM_NODES --ntasks-per-node=1 \\
rm -rf $TMPDIR
"""%sync_cmd
        script += """
# Remove files with no more use.
rm $SLURM_SUBMIT_DIR/siesta.TSGF*
# --- END OF JOB ---
"""
        return script

    def run(self, submission_file):
        remote = self.changePathType(
                submission_file,
                fro=LOCAL,
                to=REMOTE)
        remote_dir = os.path.dirname(remote)
        full_remote = "%s@%s:%s" % (self.username(), self.server(), remote_dir)
        self.syncFile(submission_file, to=REMOTE, fro=LOCAL)

        cmd = 'cd %s;  sbatch %s'%(remote_dir, remote)
        self.executeCommand(cmd, output=True)

    def executeCommand(self, cmd, verbose=False, output=False):
        if self.isRemote():
            init_cmd = 'source /etc/bashrc'
            cmd = """%s; %s""" % (init_cmd, cmd)
            host = '%s@%s' % (self.username(), self.server())
            cmd = 'ssh %s "%s"' % (host, cmd)

        if output:
            out = subprocess.check_output(cmd, shell=True, stdin=subprocess.PIPE, universal_newlines=True)
            out = out.split('\n')
            if verbose:
                print ''.join(out)
        else:
            os.system(cmd)
            out = None

        return out

    @classmethod
    def jobNameLimit(cls):
        return 8

    @classmethod
    def fileExtension(cls):
        return '.slurm'

    def getSubmitted(self, identifier='id'):
        user = self.username()
        cmd = "squeue -u %s"%user
        queread = self.executeCommand(cmd, output=True)
        queread = queread[1:-1]
        column = {
                  'id':          0,
                  'partition':   1,
                  'name':        2,
                  'active':      4,
                  'time':        5,
                  None: None,
                  }[identifier]

        submitted=[]
        for line in queread:
            line=line.split()
            line = [element.strip() for element in line if element!='']
            if column is None:
                submitted.append(tuple(line))
            else:
                submitted.append(line[column])


            if line[0][:4]=='----':
                read=True

        if identifier=='active':
            submitted = [element=='R' for element in submitted]

        return submitted

    def writeSubmitted(self):
        cmd = 'squeue -u %s'%self.username()
        if self.isRemote():
            cmd = 'ssh %s@%s %s' % (self.username(), self.server(), cmd)

        os.system(cmd)
