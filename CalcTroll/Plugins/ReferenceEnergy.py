# Written by Mads Engelund, 2017, http://espeem.com
# Defines input energy levels that cannot be defined before the electronic structure has been calculated.
# For instance, a user can specify the HOMO level of a molecule as an input before the calculation of the energy levels
# have been performed.
import numpy as np

from CalcTroll import API
from CalcTroll.Core.Utilities import isSpinPolarized
from CalcTroll.Plugins.Analysis.PDOS import PDOS
from CalcTroll.Plugins.Analysis.BandStructure import BandStructure

TRIVIAL_ENERGY = 2e-2

def convertEnergy(level, PDOS):
    if len(PDOS) <= 1:
        return level

    for i in range(len(PDOS) - 1):
        i1, i2 = PDOS[i].system().referenceEnergyMatchIndices(PDOS[i].parameters())
        reference1 = PDOS[i].reference(indices=i1)
        reference2 = PDOS[i + 1].reference(indices=i2)

        level += reference1 - reference2

    return level


class EnergyLevel(API.Analysis):
    """ Abstract base class of energy specifications that cannot be determined before a calculations has been performed.
    """
    def __init__(self, es=None, offset=0):
        self.__offset = offset
        self.__es = es
        self.__level = None

        if es is not None:
            system = es.system()
            method = es.method()
            parameters = es.parameters()
            pdos_list = []
            if len(system.subSystems()) > 0:
                pdos_list.append(PDOS(system, method, parameters))
            while len(system.subSystems()) > 0:
                parameters = system.subParameters(parameters)[0]
                system = system.subSystems()[0]
                parameters = system.defaultParameters(parameters)
                pdos = PDOS(system, method, parameters)
                pdos_list.append(pdos)

            self.__pdos = pdos_list
            if len(pdos_list) > 1:
                inputs = pdos_list
            else:
                inputs = [es]


            API.Analysis.__init__(
                    self,
                    system=es.system(),
                    method=es.method(),
                    parameters=es.parameters(),
                    inputs=inputs,
                    )

    def level(self):
        if self.__level is None:
            self.__level = self._determine()

        return self.__level

    def energySpectrum(self):
        return self.__es

    def offset(self):
        return self.__offset

    def _determine(self):
        """ Given the electronic eigenvalues, determine the energy level.

        :param es: An ElectronicSpectrum.
        :return A concrete energy (float)
        """
        raise NotImplementedError


class BandGapLevel(EnergyLevel):
    def __init__(self, es=None, offset=0):
        EnergyLevel.__init__(self, es, offset)
        if es is not None:
            pdos = self.PDOS()[-1]
            bs = BandStructure(
                    system=pdos.system(),
                    method=pdos.method(),
                    parameters=pdos.parameters(),
                    )
            self.addInput(bs)
            self.__bs = bs

    def bandStructure(self):
        return self.__bs


class ValenceBandMaximum(BandGapLevel):

    def title(self):
        """
        :return A text representation of the energy level.
        """
        return 'VBM_%.2f' % self.offset()

    def _determine(self):
        """
        Given the electronic eigenvalues, determine the energy level.

        :return: A concrete energy (float)
        """
        """ Given the electronic eigenvalues, determine the energy level. """
        if not self.isDone():
            return None

        level = self.bandStructure().read()[-1]
        level = convertEnergy(level, self.PDOS())

        return level + self.offset()

class ConductionBandMinimum(BandGapLevel):
    """ Input parameter specifying the minimum of the conduction band."""

    def title(self):
        """
        :return A text representation of the energy level.
        """
        return 'CBM_%.2f' % self.offset()

    def _determine(self):
        """
        Given the electronic eigenvalues, determine the energy level.

        :return: A concrete energy (float)
        """
        """ Given the electronic eigenvalues, determine the energy level. """
        if not self.isDone():
            return None

        level = self.bandStructure().read()[0]
        level = convertEnergy(level, self.PDOS())

        return level + self.offset()


class MidGapLevel(EnergyLevel):
    """ Input parameter specifying the energy level in the middle of the gap."""

    def _determine(self):
        """
        Given the electronic eigenvalues, determine the energy level.

        :return: A concrete energy (float)
        """
        """ Given the electronic eigenvalues, determine the energy level. """
        if not self.isDone():
            return None
        es = self.energySpectrum()

        level = (HOMO(es).level() + LUMO(es).level())/2.

        return level + self.offset()


class MO(EnergyLevel):
    def degeneracy(self):
        if not self.isDone():
            return None

        EF, eigs = es.read()
        level = self.determine(es)

        return (abs(eigs - level) < TRIVIAL_ENERGY).sum()


class LUMO(MO):
    """ Input parameter specifying the Lowest Unoccupied Molecular Orbital."""
    def __init__(self, es=None,  plus=0, offset=0.0):
        """
        :param plus: Integer specifying to chose either LUMO(plus=0) or a higher energy level LUMO+X(plus=X)
        :param offset: Adjust the target energy up or down, for instance 0.1 eV above LUMO level.
        """
        assert isinstance(plus, int) and plus>=0
        self.__plus = plus
        MO.__init__(self, es, offset=offset)

    def title(self):
        """
        :return A text representation of the energy level.
        """
        if self.__plus != 0:
            return 'LUMO_+%d' % self.__plus
        else:
            return 'LUMO'

    def _determine(self):
        """ Given the electronic eigenvalues, determine the energy level.

        :param es: An ElectronicSpectrum.
        :return A concrete energy (float)
        """
        """ Given the electronic eigenvalues, determine the energy level. """
        if not self.isDone():
            return None

        es = self.energySpectrum()
        EF, eigs = es.read()
        if not isSpinPolarized(eigs):
            eigs = eigs[:,:, 0]
        eigs = eigs.flatten()
        eigs = sorted(eigs[eigs > EF - TRIVIAL_ENERGY])
        eigs = findUniqueEnergies(eigs, TRIVIAL_ENERGY)
        level = eigs[self.__plus] + self.offset()

        return level


class HOMO(MO):
    """ Input parameter specifying the Highest Occupied Molecular Orbital."""
    def __init__(self, es=None, minus=0, offset=0.0):
        """
        :param minus: Integer specifying to chose either HOMO(minus=0) or a lower energy level HOMO-X(minus=X)
        :param offset: Adjust the target energy up or down, for instance 0.1 eV above HOMO level.
        """
        assert isinstance(minus, int) and minus>=0
        self.__minus = minus
        MO.__init__(self, es=es, offset=offset)

    def title(self):
        """
        :return A text representation of the energy level.
        """
        if self.__minus !=0:
            return 'HOMO_-%d' % self.__minus
        else:
            return 'HOMO'

    def _determine(self):
        """
        Given the electronic eigenvalues, determine the energy level.

        :param es: An ElectronicSpectrum.
        :return: A concrete energy (float)
        """
        """ Given the electronic eigenvalues, determine the energy level. """
        if not self.isDone():
            return None

        es = self.energySpectrum()
        EF, eigs = es.read()
        if not isSpinPolarized(eigs):
            eigs = eigs[:,:, 0]
        eigs = eigs.flatten()
        eigs = sorted(eigs[eigs < EF + TRIVIAL_ENERGY])
        eigs = findUniqueEnergies(eigs, TRIVIAL_ENERGY)
        eigs.reverse()
        level = eigs[self.__minus] + self.offset()

        return level

def findUniqueEnergies(energies, delta):
    """
    Remove duplicate energies due to degeneracy.

    :param energies: Array of energies.
    :param delta: Energy difference that is considered trivial and due.
    :return: A new array with close energies removed.
    """
    i = 1
    while i < len(energies):
        if abs(energies[i] - energies[i - 1]) < delta:
            energies[i - 1] = (energies[i] + energies[i - 1])/2
            energies.pop(i)
        else:
            i += 1

    return energies
