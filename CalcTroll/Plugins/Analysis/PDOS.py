# Written by Mads Engelund, 2017, http://espeem.com
from CalcTroll import API
class PDOS(API.CollatableAnalysis):
    def reference(self, indices):
        EF = self.fermiEnergy()
        values, energies = self.read(target_indices=indices)
        if values.shape[1] == 2:
            values = values.sum(axis=1)
        below_fermi = energies < EF
        energies = energies[below_fermi]
        values = values[below_fermi]
        reference = (energies*values).sum()/values.sum()

        return reference
