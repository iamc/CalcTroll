# Written by Mads Engelund, 2017, http://espeem.com
import os
import numpy as np
from CalcTroll import API
from CalcTroll.API import DEFAULT

class Transmission(API.CollatableAnalysis):
    def __init__(self,
                 system,
                 method=DEFAULT,
                 energies=slice(-2, 1, 0.02),
                 kpts=DEFAULT,
                 ):
        self.__energies = energies
        if method is DEFAULT:
            method = system.method()
        if kpts is DEFAULT:
            kpts = system.parameters().kpts()
        self.__kpts = tuple(kpts)

        API.CollatableAnalysis.__init__(
                self,
                system=system,
                method=method,
                )

    def energies(self):
        return self.__energies

    def kpts(self):
        return self.__kpts

    def _getStatus(self):
        outfile = self.outFile()
        if not os.path.exists(outfile):
            return API.NOT_STARTED

        with open(outfile, 'r') as f:
            done = False
            for line in f:
                done = '>> End of run:' in line
        if done:
            return DONE
        else:
            return NOT_STARTED

    def plot(self, data, ax, **kwargs):
        for labels, line in data.iteritems():
            values, energies = line
            label = '%s-%s' % tuple(labels)
            ax.plot(values, energies, label=label)

        xlim = np.around(values.max() * 1.5, 1)
        ax.set_xlim((0, xlim))
        ax.set_ylim((energies.min(), energies.max()))
        ax.set_xlabel('Transmission(G0)')
        ax.set_ylabel('$E-E_F$')
        if len(data) > 1:
            ax.legend()
