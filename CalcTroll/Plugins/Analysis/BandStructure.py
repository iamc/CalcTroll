# Written by Mads Engelund, 2017, http://espeem.com
import os
import numpy as np
from CalcTroll import API
from CalcTroll.API import DEFAULT


class BandStructure(API.CollatableAnalysis):
    def __init__(
            self,
            system,
            method=DEFAULT,
            parameters=DEFAULT,
            k_path=DEFAULT,
            ):
        API.CollatableAnalysis.__init__(self, system=system, method=method, parameters=parameters)
        self.__k_path=k_path

    def kPath(self):
        if self.__k_path is DEFAULT:
            k_path = self.system().bandStructurePath()
        else:
            k_path = self.__k_path

        return k_path
