# Written by Mads Engelund, 2017, http://espeem.com
# Defines the request for the energy eigenvalues on a coarse k-grid (one sufficient for accurate forces).
from CalcTroll import API

class EnergySpectrum(API.CollatableAnalysis):
    def txt_save(
            self,
            filename,
            neutral_level=API.DEFAULT,
            data=None,
            levels=API.ALL,
            **kwargs):
        N = neutral_level.determine(self)
        lines = []
        for level in levels:
            e = level.determine(self) - N
            d = level.degeneracy(self)
            string = "%s\t%8.2f  %d" % (level.title(), e, d)
            lines.append((e, string))

        lines = sorted(lines)
        lines = [line for e, line in lines]
        lines.reverse()

        with open(filename, 'w') as f:
            for line in lines:
                f.write(line+'\n')
