# Written by Mads Engelund, 2017, http://espeem.com
# The point is that you can import this module and have most functionality needed
# to build a module with only one import statement.

from CalcTroll.Core.Plugin.Types import Parameters
from CalcTroll.Core.System import System, InitialState, InitialStateAtom, Collection
from CalcTroll.Core.Task import HasInputs, Task
from CalcTroll.Core.Analysis import Analysis, CollatableAnalysis,\
    AnalysisCollator
from CalcTroll.Core.Relaxation import Unrelaxed, RelaxedSubSystems, Relaxed, AbstractRelaxed
from CalcTroll.Core.Relaxation.Task import RelaxationTask
from CalcTroll.Core.Program import Program
from CalcTroll.Core.Program import Method, Program
from CalcTroll.Core.Host import Host, SubmissionServer
from CalcTroll.Core.Flags import *
from CalcTroll.Core.InterfaceFunctions import submit, calculate
from CalcTroll.Core.InterfaceFunctions import plot, view, save, read
from CalcTroll.Core.Submission.SubmitType import SubmitType
from CalcTroll.Core.Plugin.Interface import load, loadPlugin
from CalcTroll.Core.Test import Case
from CalcTroll.Core.Explanation import Explanation
from CalcTrollASE.Atoms import Atoms
