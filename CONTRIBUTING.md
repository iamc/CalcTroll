CalcTroll welcomes new developers:)    

Getting Started
===============
- Create a GitLab account.  
- Request access to the repository.  
- Wait for access.

Contributing - if everything goes smoothly
===========================================
- Create or locate an "Issue" within the GitLab interface about the change you want to make.  
- Create a new branch from within Gitlab using the button in the "Issue" window. 
- Checkout this branch on your own machine.  
- Make your changes.  
- Run python Tests.py and ensure there are no errors.
- Push your changes to the remote branch.
- In Gitlab, create a "Merge Request" for it to be merged into the master branch.
- Wait for approval.

Contributing - if things are less smooth
========================================
- If your merge request is rejected, please keep working on the same branch untill the review comments are implemented - then  
- If you wish feedback on code then create a "Merge Request" where the title starts with "WIP:"(Work In Progress). These requests will not be approved, but serves as a way of asking for the code to be reviewed. 
