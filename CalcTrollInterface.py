# Written by Mads Engelund, 2017, http://espeem.com
# This file the scripting interface to CalcTroll. The API and all plugins are loaded when the user write the import
# statement "from CalcTroll import *"
from CalcTroll import API
from CalcTroll.API import calculate, submit, plot, view, save, Atoms
from CalcTroll.API import INTEGRATED, DIFFERENTIAL

# Load all classes that inherit from Plugin and put them in the interface.
class_dict = API.load()
for key, classes in class_dict.iteritems():
    for cls in classes:
        vars()[cls.className()] = cls

# Remove temporary variables used to set up the interface.
del class_dict, key, classes, cls
