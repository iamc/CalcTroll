# Written by Mads Engelund, 2017, http://espeem.com
import numpy as np
import math
from ase.atoms import Atoms
from ase.visualize import view
from ase.constraints import FixAtoms
from numpy.linalg import norm
from numpy import matrix

from CalcTroll.Core.Utilities import convertToBasis, convertFromBasis
from CalcTroll.Core.Utilities import wrapPositions
from CalcTroll.Core.Utilities import findCopies
from CalcTroll.Core.Utilities import vectorLength
from CalcTroll.Core.Utilities import minimalDistance
from CalcTroll.Core.Utilities import periodicDistance
from CalcTroll.Core.Flags import TooHeavy

def atomsTooLarge(atoms, limit=400):
    nv = numberOfValenceElectrons(atoms)
    if nv > limit:
        raise TooHeavy("""The molecule you submitted contained %d valence electrons.
Unfortunately, we so far only support up to %d valence electrons.""" % (nv, limit))

def numberElectrons(atoms):
    return atoms.get_atomic_numbers().sum()

def numberOfValenceElectrons(atoms):
    n = atoms.get_atomic_numbers()
    vn = np.array(map(atomicNumberToValenceNumber, n))

    return vn.sum()

def atomicNumberToValenceNumber(number):
    limits = [2, 10, 18, 36, 54, 86] # The numbers of the noble gasses.
    last_limit = 0
    for limit in limits:
        if number <= limit:
            return number - last_limit
        last_limit = limit

    return number - last_limit

def findMiddleLayer(atoms):
    mid = atoms.positions[:, 2].mean()
    closest_value_index = abs(atoms.positions[:, 2] - mid).argmin()
    mid = atoms.positions[closest_value_index, 2]
    diff = abs(atoms.positions[:, 2] - mid).min() + 1e-2
    take = np.logical_and(atoms.positions[:, 2] > mid - diff,
                             atoms.positions[:, 2] < mid + diff,
                             )

    return np.arange(len(atoms))[take]

def substitutionalDopingOfMiddleLayer(atoms, n_dopants=1, dopant_element='P', centers=None):
    assert isinstance(n_dopants, int)
    assert n_dopants > 0
    atoms = atoms.copy()
    middle_layer = list(findMiddleLayer(atoms))
    symbols = np.array(atoms.get_chemical_symbols())
    make_dopants = []
    for i in range(n_dopants):
        if centers is None:
            middle_layer_index = 0
            centers = []
        else:
            pos = atoms.positions[np.array(middle_layer, int)]
            for center in centers:
                middle_layer_index = vectorLength(minimalDistance(pos - center, atoms.cell), 1).argmax()

        index = middle_layer.pop(middle_layer_index)
        make_dopants.append(index)
        centers.append(atoms[index].position)

    symbols[np.array(make_dopants, int)] = dopant_element
    atoms.set_chemical_symbols(symbols)

    return atoms

def areAtomsEqual(atoms1, atoms2, tolerance=1e-5):
    if len(atoms1) != len(atoms2):
        return False

    if tuple(atoms1.pbc) != tuple(atoms2.pbc):
        return False

    diff = atoms1.cell - atoms2.cell
    diff = np.sqrt((diff**2).sum())/9.0
    if diff > tolerance:
        return False

    diff = atoms1.positions - atoms2.positions
    diff = np.sqrt((diff**2).sum())/len(atoms1)
    if diff > tolerance:
        return False

    return True

def cellCenter(atoms, directions=(True, True, True)):
    return atoms.cell[np.array(directions)].sum(0)/2

def moveOriginToCenter(atoms, directions=(True, True, True)):
    pbc = atoms.get_pbc()
    atoms = atoms.copy()
    center = cellCenter(atoms, directions=directions)
    atoms.translate(center)
    take = np.logical_and(pbc, directions)
    atoms.set_pbc(take)
    atoms = wrap(atoms)
    atoms.set_pbc(pbc)

    return atoms

def moveOriginTo(atoms, position, directions=(True, True, True)):
    atoms.translate(position)
    pbc = atoms.get_pbc()
    take = np.logical_and(pbc, directions)
    atoms.set_pbc(take)
    atoms = wrap(atoms)
    atoms.set_pbc(pbc)

    return atoms

def moveCenterToOrigin(atoms, directions=(True, True, True)):
    pbc = atoms.get_pbc()
    atoms = atoms.copy()
    center = cellCenter(atoms, directions=directions)
    atoms.translate(-center)
    take = np.logical_and(pbc, directions)
    atoms.set_pbc(take)
    atoms = wrap(atoms)
    atoms.set_pbc(pbc)

    return atoms

def atomsInDistanceOfAtomsMask(atoms, fix_atoms, radius):
    tmp_atoms = atoms.copy()
    cell = tmp_atoms.get_cell()
    cell[fix_atoms.get_pbc()] = fix_atoms.get_cell()[fix_atoms.get_pbc()]
    tmp_atoms.set_cell(cell)
    tmp_atoms = wrap(tmp_atoms)
    take = np.ones(len(tmp_atoms), dtype=np.bool)
    for atom in fix_atoms:
        take_this = getBeyondRangeMask(tmp_atoms, atom.position, radius)
        take = np.logical_and(take, take_this)

    return take

def rattleNearBy(atoms, rattle_atoms, radius, rattle):
    atoms = atoms.copy()
    take = atomsInDistanceOfAtomsMask(atoms, rattle_atoms, radius)
    take = np.logical_not(take)
    rattle_atoms = atoms[take]
    rattle_atoms.rattle(stdev=rattle)
    atoms.positions[take] = rattle_atoms.positions

    return atoms

def fixAtomsInDistanceOfAtoms(atoms, fix_atoms, radius):
    take = atomsInDistanceOfAtomsMask(atoms, fix_atoms, radius)
    constraint = FixAtoms(mask=take)
    atoms.set_constraint(constraint)

    return atoms

def fixAtomsInDistanceOfPositions(atoms, positions, radius):
    take = np.ones(len(atoms), dtype=np.bool)
    for position in positions:
        take_this = getBeyondRangeMask(atoms, position, radius)
        take = np.logical_and(take, take_this)

    constraint = FixAtoms(mask=take)
    atoms.set_constraint(constraint)

    return atoms

def fixBelow(atoms, value, axis=2, inplace=True):
    # Fix lowest coordinates.
    constraint = FixAtoms(mask=(atoms.get_positions()[:, axis] < value + 1e-5))
    if inplace:
        atoms.set_constraint(constraint)
        return atoms
    else:
        return constraint

def fixBetween(atoms, ran, axis=2, inplace=True):
    amin, amax = ran
    mask1 = (atoms.get_positions()[:, axis] > amin)
    mask2 = (atoms.get_positions()[:, axis] < amax)
    mask = np.logical_and(mask1, mask2)

    constraint = FixAtoms(mask=mask)
    if inplace:
        atoms.set_constraint(constraint)
        return atoms
    else:
        return constraint

def orderByType(atoms):
    chemical_symbols = np.array(atoms.get_chemical_symbols())
    unique = np.unique(chemical_symbols)
    n_atoms = Atoms(cell=atoms.get_cell(), pbc=atoms.get_pbc())
    for symbol in unique:
        n_atoms += atoms[chemical_symbols==symbol]
    return n_atoms

def expressAtomsInCoordinates(atoms, vectors):
    positions = convertToBasis(atoms.get_positions(), vectors)
    unit_cell = convertToBasis(atoms.get_cell(), vectors)

    new_atoms = atoms.copy()
    new_atoms.set_positions(positions)
    new_atoms.set_cell(unit_cell)

    return new_atoms

def orderAtoms(atoms, order=(2,1,0), tolerance=1e-2):
    atoms = atoms.copy()
    if len(atoms) == 0:
        return atoms

    mask = np.zeros(len(atoms), bool)
    for constraint in atoms.constraints:
        if isinstance(constraint, FixAtoms):
            mask[constraint.index] = True

    pbc = atoms.get_pbc()
    groups = groupAtoms(atoms=atoms, order=order, tolerance=tolerance)
    atoms, indices = groups[0]
    indices = list(indices)
    for atoms_group, indices_group in groups[1:]:
        atoms += atoms_group
        indices.extend(list(indices_group))

    atoms.set_pbc(pbc)

    constraint = FixAtoms(mask=mask[indices])
    atoms.set_constraint(constraint)

    return atoms

def groupAtoms(atoms, order=(2,), tolerance=1e-2):
    atoms = atoms.copy()
    del atoms.constraints
    pbc = atoms.get_pbc()

    atoms.set_pbc([True]*3)
    cell = atoms.get_cell()
    positions, remainder = convertToBasis(atoms.get_positions(), cell)
    atoms.set_positions(positions)
    tolerance1 = convertToBasis(np.array([1,1,1])*tolerance, cell)[0][0]
    tolerance2 = convertToBasis(np.array([-1,1,-1])*tolerance, cell)[0][0]
    tolerance1 = map(abs, tolerance1)
    tolerance2 = map(abs, tolerance2)
    tolerance = np.max((tolerance1, tolerance2), 0)

    indices = np.arange(len(atoms))
    groups = [(atoms, indices)]

    for i in order:
        new_groups = []
        for atoms, indices in groups:
            while len(atoms) > 0:
                positions = atoms.get_positions()
                min_value = np.min(positions[:, i])
                group_mask = abs(positions[:, i] - min_value) < tolerance[i]
                atom_group = atoms[group_mask]
                indices_group = indices[group_mask]
                atoms = atoms[np.logical_not(group_mask)]
                indices = indices[np.logical_not(group_mask)]
                new_groups.append((atom_group, indices_group))

        groups = new_groups

    for atoms, indices in groups:
        positions = convertFromBasis(atoms.get_positions(), cell)
        atoms.set_positions(positions)
        atoms.set_cell(cell)

    return groups

def wrap(atoms, center=(0,0,0)):
    atoms = atoms.copy()
    pbc = atoms.get_pbc()

    # Early escape if no directions are defined as periodic.
    if not pbc.any():
        return atoms

    unit_cell = atoms.get_cell()[pbc]
    center = np.array(center)[pbc]
    positions = wrapPositions(atoms.get_positions(), unit_cell, center)
    atoms.set_positions(positions)

    return atoms

def removeCopies(atoms):
    if len(atoms) == 0:
        return atoms.copy()

    positions = atoms.get_positions()
    take = findCopies(positions, atoms.get_cell())
    copy_free_atoms = atoms[take]

    return copy_free_atoms


def accomodateAtoms(atoms, padding):
    """ Create a cell """
    return cell

def getBeyondRangeMask(atoms, position, radius):
    atoms = atoms.copy()
    atoms.translate(-position)
    middle = np.sum(atoms.get_cell(), axis=0)/2.
    atoms.translate(middle)
    atoms = wrap(atoms)
    atoms.translate(-middle)
    beyond_range = vectorLength(atoms.get_positions(), 1) > radius

    return beyond_range

def getWithinRangeMask(atoms, position, radius):
    beyond_range = getBeyondRangeMask(atoms, position, radius)
    within_range = np.logical_not(beyond_range)

    return within_range

def removePeriodicallyEqualAtoms(atoms, other_atoms):
    if len(atoms) == 0:
        return other_atoms.copy()
    group = findPeriodicallyEqualAtoms(atoms, other_atoms)
    keep = (group == -1)
    keep = np.arange(len(keep))[keep]

    return other_atoms[list(keep)]

def findPeriodicallyEqualAtoms(atoms, other_atoms):
    center = atoms.positions.mean(axis=0)
    center, remain = convertToBasis(center, other_atoms.cell)
    center = (np.array([.5, .5, .5]) - center)[0]
    other_atoms = wrap(other_atoms, center=center)
    pbc = atoms.get_pbc()
    cell = atoms.cell[pbc]

    group = -np.ones(len(other_atoms), dtype=np.int)
    for i, atom in enumerate(atoms):
        diff = other_atoms.get_positions() - atom.position
        pos = minimalDistance(diff, cell)
        mask = vectorLength(pos, 1) < 1e-5
        indices = np.arange(len(other_atoms))[mask]
        group[indices] = i

    return group

class Rattle:
    def __init__(self, rattle):
        self.__rattle = rattle

    def rattle(self, atoms):
        atoms = atoms.copy()
        atoms.rattle(self.__rattle)

        return atoms

class RattleNearBy(Rattle):
    def __init__(self, rattle_atoms, radius, rattle):
        self.__rattle_atoms = rattle_atoms.copy()
        self.__radius = radius
        self.__rattle = rattle

    def rattle(self, atoms):
        assert isinstance(atoms, Atoms)
        atoms = atoms.copy()
        take = atomsInDistanceOfAtomsMask(atoms, self.__rattle_atoms, self.__radius)
        take = np.logical_not(take)
        rattled_atoms = atoms[take]
        rattled_atoms.rattle(stdev=self.__rattle)
        atoms.positions[take] = rattled_atoms.positions

        return atoms

class Constrainer:
    def constrain(atoms):
        raise Exception

class ConstrainNearest:
    def __init__(self, positions):
        self.__positions = positions

    def constrain(self, atoms):
        fix = []
        constraints = list(atoms.constraints)
        for position in self.__positions:
            diff = atoms.positions - position
            i = periodicDistance(diff, atoms.cell).argmin()
            fix.append(i)
        mask = np.zeros(len(atoms), bool)
        mask[np.array(fix)] = True
        constraint = FixAtoms(mask=mask)
        constraints.append(constraint)

        mask = np.zeros(len(atoms), bool)
        for constraint in constraints:
            mask = np.logical_or(mask, constraint.index)

        atoms.set_constraint(FixAtoms(mask=mask))

        return atoms

def makeAtomsRelaxed(atoms, relaxation, tolerance=1.0, cell=None, change=None, correct_drift=False):
    atoms = atoms.copy()
    if relaxation is None:
        return atoms
    if change is None:
        change = np.ones(len(atoms), np.bool)

    if cell is None:
        cell = atoms.get_cell()
        cell[relaxation.pbc] = relaxation.cell[relaxation.pbc]

    drift = np.zeros(3, np.float)
    used = np.zeros(len(relaxation), np.bool)
    changed = []
    if relaxation is not None:
        # Find relaxed atoms closest to originals.
        for i, atom in enumerate(atoms):
            if not change[i]:
                continue
            mask = np.array([r_atom.symbol == atom.symbol for r_atom in relaxation])
            mask = np.logical_and(mask, np.logical_not(used))

            if mask.sum() == 0:
                continue
            diff = relaxation.positions[mask] - atom.position
            diff = minimalDistance(diff, cell[relaxation.pbc])
            index = vectorLength(diff, 1).argmin()
            adjustment = diff[index]
            if vectorLength(adjustment) < tolerance:
                drift += adjustment
                real_index = np.arange(len(relaxation))[mask][index]
                used[real_index] = False
                changed.append(i)
                atom.position += adjustment

    if correct_drift:
        changed = np.array(changed)
        drift /= len(changed)
        atoms.positions[changed] -= drift

    return atoms

def findIndices(find_atoms, atoms, tolerance=1e-5):
    indices = []
    for pos in find_atoms.positions:
        diff = vectorLength(atoms.positions - pos, 1)
        argmin = np.argmin(diff)
        if diff[argmin] > tolerance:
            indices.append(None)
        else:
            indices.append(argmin)

    return indices
