# Written by Mads Engelund, 2017, http://espeem.com
import numpy
from ase.atoms import Atoms
from RegularizeMolecule import *
from CalcTroll.Core.Test.Case import CalcTrollTestCase

import unittest

class RegularizeMoleculeTest(CalcTrollTestCase):
    def testBezene(self):
        atom_group = Atoms("CH", positions=[[2, 0.2, 0], [3, 0.3, 0]])
        atoms = atom_group.copy()
        for i in range(5):
            atoms.rotate(a=np.pi/3., v='z')
            atoms += atom_group.copy()

        atoms = regularizeMolecularDirections(atoms)

        expected = Atoms("HHCCHCCHCCHH", positions=[
            [-1.50748, -2.61103, 0.0     ],
            [1.50748 , -2.61103, 0.0     ],
            [-1.00499, -1.74069, 0.0     ],
            [1.00499 , -1.74069, 0.0     ],
            [-3.01496, 0.0     , 0.0     ],
            [-2.00998, 0.0     , 0.0     ],
            [2.00998 , 0.0     , 0.0     ],
            [3.01496 , 0.0     , 0.0     ],
            [-1.00499, 1.74069 , 0.0     ],
            [1.00499 , 1.74069 , 0.0     ],
            [-1.50748, 2.61103 , 0.0     ],
            [1.50748 , 2.61103 , 0.0     ],
            ])
        self.assertAtomsEqual(atoms, expected)

    def testMethane(self):
        atoms = Atoms("CHHHH", positions=[ [0,       0,        0     ],
            [ 1.0900, 0,        0     ],
            [-0.3633, 1.0277,   0     ],
            [-0.3633, -0.5138,  0.8900],
            [-0.3633, -0.5138, -0.8900],
            ])

        atoms = regularizeMolecularDirections(atoms)

        expected = Atoms("HCHHH", positions=[
            [-0.3633 , -0.51381, -0.89000],
            [0       , 0       ,  0      ],
            [ 1.08999, 0       ,  0.00000],
            [-0.36331, 1.02769 ,  0.00000],
            [-0.36331,-0.51381 ,  0.89000],
            ])
        self.assertAtomsEqual(atoms, expected)

    def testLinear(self):
        atoms = Atoms("HC", positions=[[1.1, 1.2, 1.3], [1.4, 1.5, 2.1]])
        atoms = regularizeMolecularDirections(atoms)

        expected = Atoms("CH", positions=[[-0.07011, 0, 0], [0.83543, 0, 0]])
        self.assertAtomsEqual(atoms, expected)

    def test2DSym(self):
        atoms = Atoms("CCC", positions=[[0, 0, -1], [0, 0, 1], [0, np.sqrt(3), 0]])
        atoms = regularizeMolecularDirections(atoms)

        expected = Atoms("CCC", positions=[
            [-0.57735, -1.0, 0.0],
            [ 1.15470,  0.0, 0.0],
            [-0.57735,  1.0, 0.0],
            ])
        self.assertAtomsEqual(atoms, expected)

    def testOneAtom(self):
        atoms = Atoms("H", positions=[[1.1, 1.2, 1.3]])
        atoms = regularizeMolecularDirections(atoms)

        expected = Atoms("H", positions=[[0, 0, 0]])
        self.assertAtomsEqual(atoms, expected)


if __name__=='__main__':
    unittest.main()
