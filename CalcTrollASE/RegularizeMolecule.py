# Written by Mads Engelund, 2017, http://espeem.com
import numpy as np
import math
from ase.atoms import Atoms
from ase.visualize import view
from ase.constraints import FixAtoms
from numpy.linalg import norm
from numpy import matrix

from CalcTroll.Core.Utilities import convertToBasis, convertFromBasis
from CalcTroll.Core.Utilities import wrapPositions
from CalcTroll.Core.Utilities import findCopies
from CalcTroll.Core.Utilities import vectorLength
from CalcTroll.Core.Utilities import minimalDistance
from CalcTroll.Core.Utilities import periodicDistance
from CalcTroll.Core.Flags import TooHeavy

from AtomUtilities import orderAtoms

SMALL_ANGULAR_M = 0.01

def regularizeLinear(atoms):
    p = atoms.get_positions()
    maxi, mini = p[:, 2].max(), p[:, 2].min()
    if abs(mini) > maxi:
        p[:, 2] = -p[:, 2]
    atoms.set_positions(p)

    return atoms

def findMirrorPlanes(atoms, subspace):
    p = (atoms.get_positions()[:, subspace].T*atoms.get_masses()).T
    vl = vectorLength(p, axis=1)
    pl = p[vl > vl.max() - SMALL_ANGULAR_M]
    axes = []
    for i in range(0, len(pl)):
        ax = (pl[0] + pl[i])/2.
        ax /= vectorLength(ax)
        axes.append(ax)
    axes = np.array(axes)
    argmax = np.argmax(np.tensordot(axes, p, (1, 1)).max(axis=1))

    return axes[argmax]


def regularize2DSym(atoms, subspace):
    subspace = np.array(subspace)
    m = atoms.get_masses()
    p = atoms.get_positions()
    ps = p[:, subspace]
    v1 = findMirrorPlanes(atoms, subspace)
    if v1 is None:
        vl = vectorLength(ps, axis=1) * m
        argmax = np.argmax(vl)
        v1 = p[argmax, subspace]
        v1 /= vectorLength(v1)

    v2 = np.array([v1[1], -v1[0]])
    M = np.array([v1, v2]).T
    ps = np.tensordot(M, ps, (1, 1)).T
    p[:, subspace] = ps
    atoms.set_positions(p)

    return atoms

def regularize3DSym(atoms):
    m = atoms.get_masses()
    p = atoms.get_positions()
    vl = vectorLength(p, axis=1) * m
    argmax = np.argmax(vl)
    v1 = findMirrorPlanes(atoms, [0, 1, 2])
    atoms.rotate(a=np.array([1, 0, 0]), v=v1)
    atoms = regularize2DSym(atoms, [1, 2])

    return atoms

def regularizeMolecularDirections(atoms):
    pos = atoms.get_positions()
    m = atoms.get_masses()
    mp = np.transpose(np.transpose(pos)*m)
    pos -= mp.sum(axis=0)/m.sum()
    mp = np.transpose(np.transpose(pos)*m)
    a = np.matrix(np.tensordot(mp, mp, (0, 0)))
    vals, M = np.linalg.eigh(a)
    argsort = np.argsort(vals.real)
    M = M.real

    vals = vals[argsort[::-1]]
    M = M[:, argsort[::-1]]
    M[:, 0] = np.linalg.det(M)*M[:, 0]
    mp = np.tensordot(mp, M, (1, 0))
    pos = np.transpose(np.transpose(mp)/m)
    atoms.set_positions(pos)
    if (vals < SMALL_ANGULAR_M).all():
        # It is just one atoms -the directions are equivalent.
        pass
    elif vals[1] - vals[2] > SMALL_ANGULAR_M and \
         vals[0] - vals[1] > SMALL_ANGULAR_M:
        # The directions are already unique.
        pass

    elif vals[2] < SMALL_ANGULAR_M and \
         vals[1] < SMALL_ANGULAR_M:
        pass

    elif vals[1] - vals[2] < SMALL_ANGULAR_M and \
         vals[0] - vals[2] < SMALL_ANGULAR_M:
        atoms = regularize3DSym(atoms)
    elif vals[1] - vals[2] < SMALL_ANGULAR_M:
        regularize2DSym(atoms, [1, 2])
    elif vals[0] - vals[1] < SMALL_ANGULAR_M :
        regularize2DSym(atoms, [0, 1])
    else:
        # All cases should be covered, but...
        raise Exception('Unknown case')

    p = atoms.get_positions()
    flips = 0
    free = 0
    for i in range(3):
        maxi, mini = p[:, i].max(), p[:, i].min()
        if maxi < SMALL_ANGULAR_M and abs(mini) < SMALL_ANGULAR_M:
            free += 1
        elif abs(mini) > maxi:
            p[:, i] = -p[:, i]
            flips += 1

    # We cannot invert an even number of axes since this mirrors
    # the molecule.
    if free == 0 and flips%2 == 1:
        p[:, 2] = -p[:, 2]
    atoms.set_positions(p)

    atoms.set_cell(p.max(axis=0) + abs(p.min(axis=0)) + 10)
    atoms = orderAtoms(atoms, tolerance=0.01, order=(0, 1, 2))

    return atoms
