# Written by Mads Engelund, 2017, http://espeem.com
import os
import stat
from os.path import join
import sys
import shutil
import getpass

host_filename = 'CalcTroll/HOSTS.py'
executables = join('CalcTroll', 'Setup', 'UtilityExecutables')
bin_directory = 'bin'
os.chdir(os.path.abspath(os.path.dirname(__file__)))

if not os.path.exists('ExampleScripts'):
    print 'making new examples'
    shutil.copytree(join('CalcTroll', 'Setup', 'OriginalExampleScripts'), 'ExampleScripts')
else:
    print 'ExampleScripts directory found, will not overwrite...'

if not os.path.exists(host_filename):
    print 'making %s example' % host_filename
    template = join('CalcTroll', 'Setup', 'HostExamples.py')
    with open(template, 'r') as f:
        content = f.read()
    uname = os.uname()[1]
    content = content.replace('COMPUTERNAME', uname)
    home = os.environ['HOME']
    content = content.replace('LOCALPATH', home)

    with open(host_filename, 'w') as f:
        f.write(content)
else:
    print '%s found, will not overwrite...' % host_filename

print 'making utility executables...'
if not os.path.exists(bin_directory):
    os.makedirs(bin_directory)

proper_files = [filename[:-3] for filename in  os.listdir(executables) if filename[-3:] == '.py']
proper_files.remove('__init__')

for basename in proper_files:
    dst_path = join(bin_directory, basename)
    with open(dst_path, 'w') as dst_file:
        dst_file.write('#!' + sys.executable +'\n')
        dst_file.write('from CalcTroll.Setup.UtilityExecutables import %s' % basename)

    os.chmod(dst_path, stat.S_IEXEC|stat.S_IREAD|stat.S_IWRITE)

print 'Setup done.'
