Introduction to CalcTroll
=======================================
CalcTroll a series of utilities for setting up series time-demanding long-running calculations on a cluster, specifically focused on electronic structure calculations with the SIESTA code.

The basic idea is to write a very short and consise script to define what type of calculation is desired; for instance, the calculation of an STMImage of a single dangling-bond on the Ge(001):H, could be written:

.. literalinclude:: ../../CalcTroll/Setup/OriginalExampleScripts/stm.py 
    :language: python


The first line imports the interface, i.e all variable names that have a predefined meaning in CalcTroll. 
The second line tells the program that it should calculate the STM image of pair of dangling-bonds.


'STMImage' is a pre-defined type of analysis and 'Dimer2DBDefect' is a pre-defined physical system (two nearest-neighbour dangling-bond defects on the C/Si/Ge(001):H surface). 
Although a fair number of structures and calculations come predefined in the program, in general a user will need to implement these by subclassing some of the basic classes defined in the code. 

This short script is transparent and readable making the intent clear to anyone. When all calculations and physical systems are predefined users' life is very easy since this script is all they need to define.
If the need arises to add new functionality, then an implementer can to a large extent use the physical systems already defined as building blocks to create ever more complex systems.  
